#pragma once
#include <spawnx/spawnx.h>

struct skill_mana_cost
{
	int cost;
};
struct skill_duration
{
	double remaining_time;
	double effect_length;
};
struct skill_strength
{
	float strength;
};
struct skill_effect
{
	void perform_effect(skill_strength strength)
	{

	}
};
struct logical_duration_buff
{
	skill_duration	duration;
	skill_mana_cost mana_cost;
	skill_strength	strength;
	skill_effect	effect;
};

struct mana_burn_effect
{
	//requires all the components of logical_duration_buff
	float burn_effect_multiplier;//per x unit of time, this will scale the total cost.

	double effect_length_so_far = 0.;
	double growth_rate = 1.3;
	float mana_cost(logical_duration_buff buff, double delta_time)
	{
		effect_length_so_far += delta_time;
		auto this_piece_base_cost = buff.mana_cost.cost / buff.duration.effect_length * delta_time; 
		return this_piece_base_cost * pow(effect_length_so_far, growth_rate);
	}

};