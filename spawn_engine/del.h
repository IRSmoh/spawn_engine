#pragma once
void new_mesher()
{
	//for every vertex that can be positioned
	//determine if its part of the 'sharp' features
	//get that features eigen values for a 3x3 matrix
	//if more than 2 of the eigens are non-zero that implies its a sharp feature.

	//assorted rules for when dealing with meshing the features together (which should mean that the above is run on a per vert basis.
	//this only applies if none of the vertices are 'concave envelope vertices'

	//rule 1: if only 1 vertex is a strong feature; "the diagonal of triangulation must link to the sharp edge feature vertex"
	//basically....
	//	x------------O
	//	|			/|
	//	|		/	 |
	//	|	/		 |
	//	x------------x

	//rule 2: if there are two sharp features that do not belong to adjacent cells the diagonal of triangulation must link the sharp features.
	//basically....
	//	x------------O
	//	|			/|
	//	|		/	 |
	//	|	/		 |
	//	O------------x

	//rule 3: if there are three sharp features you must link the two that are non adjacent to each other.
	//basically....
	//	O------------O
	//	|			/|
	//	|		/	 |
	//	|	/		 |
	//	O------------x
}