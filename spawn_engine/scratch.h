#pragma once
//no actual code is supposed to be here; I just like the color scheme so I'm using this instead of notepad++.

//tasks:
//	fix hitching when moving around the world likely only caused by generating heightmaps.
//	add in physics again (shouldn't take long)
//	multithread physics in so much as physics sim runs on a separate thread from main.
//	add in the ability to throw a ball or something and its associated mesh + gpu mesh.
//	get some form of sound loading
//	start actually using the component system vs just having it sit there (rearchitecture everything again..)
//	get networking up
//	actually get it working
//	sync clients properly (aka fix physics when its going to be broken again)
//	make networking trivial to do from the api standpoint and then start making an actual game.
//	---^^^before august^^^---
//	get model loading for common types or: make a converter to a standard type
//	get bone rigging....
//	more graphics engine improvements... ugh

//schools of magic:
//typical:
//	fire
//	cold
//	water
//	lightning
//	earth

//lazy single mixing of above.
//fire + water		-> steam
//fire + cold		-> temperature instability (vortex?)
//fire + lightning	-> plasma?
//fire + earth		-> magma
//fire + air		-> heated zone/pressurizing

//cold + water		-> ice / frost
//cold + lightning	-> ???????????
//cold + earth		-> ?
//cold + air		-> chilled zone/depressurizing

//water + lightning	-> electrified water
//water + earth		-> mud
//water + air		-> fog/cloud/mist

//lightning + earth	-> grounding (sinks other lightning)
//lightning + air	-> ?? storm?

//earth + air		-> dust

//less typical:
//	abyssal		(dark/demonic)
//	radiant		(light)
//	necrotic	(poison/decay)
//	life		(healing) or self mobile (summoning?)
//	arcane		(magic/runes/enchanting)
//	temporal	(durations mostly/periodic effects (basically a timer))
//	space		(location/bounded location)
//	force		(basically the speed of movement/pushing something)
//	vacuum		(void, somewhat invert of above)

//skill support stuff
//	shape
//	copies
//	movement vector
//	movement pattern
//	delay (of attack/cast/effect)
//	imbument (e.g. giving the ability to cast to some other object)
//		conditions for the above^^^^
//		e.g. damage thresholds
//		mob density thresholds
//		


//if projectile:
//modifing... ->
//multiple projectiles
//source location of projectiles
//vector of movement of projectiles
//movement pattern (straight, circle, spiral, etc)

//if aoe:
//smaller
//larger
//increased angle if not 360 already
//moved center
//higher damage closer to center
//higher damage further from center
//concentric circle with hollow center (e.g. disk thingy)
//pattern masking (e.g. chunks removed (spirals for bosses etc)
//multi zone + zone movements (4x circles equidistant from a center all rotating for instance)

//real world locality + later fantasies
//:europe /new world but primarly euro.

//melee weapons:
//	dagger
//	short sword
//	long sword
//	whip
//	lasso (because fuck you)
//	axe (two/one handed)
//	mace
//	flail
//	pikes/lances/halberds/glaive (all the same broad category something stabby/cutty on a long stick)
//	rune based of above ^^^

//ranged weapons:
//	bow
//	crossbow
//	flint lock pistols/rifles
//	clockwork pistol/files (its cool)
//	throwing weapons (e.g. daggers/knives)
//	sling (rock/potions/etc)
//	throwing discs
//	boomerang
//	

//itemization:
//	potential way it could work:
//		there is a pool of low-mid level craftable mods that are "easily" acquirable that guarantee decentish results for being on level
//		there is a different pool of harder to acquire currency that can roll better mods than above; along with mods that are unique to its currency type
//		there is a different pool of mods that mobs can drop that roll differently from the other 2 above pools that are equally as good as the above or better.
//		you could combine the last 2 sets of mods via acquiring 2 items from the same base type and randomly getting half of the mods on each item.

//	rationale:
//		it should make beginning itemization sort of flat allowing a sane curve. while allowing users to either fight enemies or acquire wealth and gamble to acquire better gear.
//		and allow beyond rich players to gamble top tier items from both pools for absurdly disgusting items.

//players:
//	count:
//		fucking large (try realm of the mad god style instances)
//		: I want trains to be a thing aka a large group of people going through shitting on content for xp
//		>thus we have to be able to simulate... 300?+ people (50'000+ entities)(over shoot probably) at around 144fps
//		>models can't be too complicated else the gpu will have a hard time rendering probably
//		>must have a particle limit for other players and a separate one for yourself
//		>its possible that with a sufficiently low particle limit for mobs to take phantom damage because things were culled that would normally have shown something hitten them.

//classes/skill tree:
//	make classes unique to each other *without* limiting builds
//		so it's possible that just speccing into the starting node (e.g. a 1 point investment that's given for free to people that start as a class) gives you that class.
//		it could be done such that you could path over and spec the point gaining the abilities of the class.
//	maybe classes should have unique sub skill trees to them (similar to poe ascendencies)
//		but speccing into them from other class starting points would be a thing
//		this means we should have a bounded upper limit of the number of points you could spec into class starts with (but you still get some immediate benefits from just having the start)

//multiplayer musings:
//	it's an arpg. it doesn't need to be perfect when it comes to syncing all players ever so long as the local simulation *looks* good enough it's good enough
//	so in that vein: what can we move to the server to reduce the chance of hacks.
//	all item drops should be on the server and sent to the client for rendering; theres no real reason why the client should ever be able to tell what the item's doing
//		item modifying could then look fucky with sufficient delay... so that's a possible cause for concern (aka mass spam clicking an item could time out)
//	player movement could be either way, but non client movement should always be interpolated from the past server state (e.g. not this client)
//	but at least currently its conceivable to have the player move in the world, have the server lag the predictions behind and see if the client was ever hit
//	which basically means for the majority of the time the server is unfortunately running a fucking ton of different states of the past game world trying to determine what players would be hit and when

//alright ignoring the above:
//what makes an arpg fun?
// given the tennants of the genre -> fights + loot and since its an rpg rollplaying + exploring
//	so how do you make the quasi 3 separate parts mesh well, and make them fun?




//========================================================
//gui thoughts
//==----------------------------------------------------==
//complaints from current things:
//	no spatial justification e.g. you can't have text that goes along normally, spaced normally
//	and has peridioc alignment points. aka visual studio finally after some time has tabs aligned to the nearest 4 space increment -- thats nice.
//		not everything does that.
//	there is no easy way to align things to the hard side (with room for padding on the right still.)
//	meaning I'd like to have a way of saying that something is part of a column, and everything in a column must start in the same aligned x location.
//
//Textboxes should have several ways of being set.
//	I want text boxes that auto generate scroll bars (reasonable request)
//	I want them to generate scrolls for the y axis, and the x axis if needed to display text.
//	they must also be user resizable by dragging. this should auto adjust the above
//	text should be optionally auto wrapped to fit a textbox - this would disable the x scrolling.
//	also must be recalculated after a resize (of the x dim)
//	text boxes should also optionally be forced to expand to the size of text.
//	text in the above should still use a maximum width that's user provided if text wrapping is enabled.
//	text should also optionally (basically always) have some padding that's auto created around it.
//	text boxes could be invisible, have transparent backgrounds... etc.
//	to faccilitate the immediate above text generation should probably be decoupled from the actual rendering of the box (at least the background).
//	options for displaying all text as a certain character...
//	some kind of tagging ability to easily in raw text specify if something is formatted differently than the rest of the normal text.
//	in addition to the above we could* and should* probably make this 'strong' in that we can grab live things.
//		meaning we could have something like $[armor_display_color] as a token before some text input, in which case the next portion of text uses that format.
//		whats a sane all case escape character? $[/] for everything, $[/<identifier] for remaining?
//		support common bbcode markups e.g. [i] [b] probably not [img] or [code] though, maybe [s] for strike throughs?
//		maybe have a direct way of inline coloring things $[color{r,g,b,a}] that then auto grabs the rgba values interpreted as [0,255].
//	since its a thing that can be enabled... it should be a thing that's disable-able. we don't want player names to suddenly be neon colors.
// per line things.. e.g. center/left aligned/ right aligned
// different font support..
// sizing...
// does this basically read like css + html?


//Text input regions should have most* things from text boxes. in addition..
//	have hints.. e.g. "please enter a name..." that the user cannot select and vanishes upon any user text input
//	configurably copy and paste able e.g. you'd probably want to disable password copying (or not so people can use password managers lets not be assholes here.)

