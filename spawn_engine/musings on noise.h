//things I want to do with noise:
//support plurality of noise types.
//	e.g. perlin, simplex, worley?, fractal(?), maybe others
//	the usual ways of customizing the noises on a per noise style basis
//	ultimately the API is going to be very similar to the sdfs
//	float noise_gen::operator()(const glm::vec[2,3]& world_pos);
#include <glm\glm.hpp>
#include <vector>
#include <algorithm>
struct noise_module
{
	virtual float operator()(const glm::vec2& world_pos)
	{
		return -1.f;
	}
	virtual ~noise_module() {}
};

struct perlin : noise_module {};
struct worley : noise_module {};

struct selector : noise_module
{
	struct noise_and_gradient
	{
		noise_module noise;
		float gradient_point;
	};
	std::vector<noise_and_gradient> noises;
	noise_module selector;
	float transition_threshold;
	bool sorted;
	void add_gradient(noise_module& noise_gen, float gradient_point)
	{
		sorted = false;
		noises.push_back({ noise_gen,gradient_point });
	}
	float operator()(const glm::vec2& world_pos)
	{
		if(!sorted)
			std::sort(noises.begin(), noises.end(), [](auto& left, auto& right) { return left.gradient_point < right.gradient_point; });

		float selector_val = selector(world_pos);

		auto lower = noises.begin();
		auto upper = lower;
		for (auto itr = lower; itr != noises.end(); ++itr)
		{
			if (itr->gradient_point > selector_val)
				break;
			lower = itr;
			upper = lower + 1;
		}

		if (selector_val < upper->gradient_point - transition_threshold || upper == noises.end())
		{
			//can only be in the range of the lower function
			return lower->noise(world_pos);
		}
		if (selector_val > upper->gradient_point - transition_threshold && selector_val < upper->gradient_point)
		{
			float range_point = selector_val - upper->gradient_point - transition_threshold;
		}
		return upper->noise(world_pos);
	}
};
float some_mountain_thing()
{
	//perlin for the plain style area, worley for map between the differences
	perlin low_lands;
	perlin mountains;
	worley noise_mask;

	glm::vec2 lookup{ 90,-10 };
	float mask_val = noise_mask(lookup);
	float threshold_switch = .01f;
	if (mask_val < -threshold_switch)
		return low_lands(lookup);
	else if (mask_val > -threshold_switch && mask_val < threshold_switch)
		return glm::mix(low_lands(lookup), mountains(lookup), mask_val);
	return mountains(lookup);
}