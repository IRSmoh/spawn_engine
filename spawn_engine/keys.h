#pragma once
#include <GLFW\glfw3.h>
#include <array>
#pragma region KEY_DEFS
//These keys are redefined here incase we start using a different API for grabbing keys/make our own.
/* The unknown key */
enum key_types
{
 KEY_UNKNOWN            =GLFW_KEY_UNKNOWN,

	/* Printable keys */
 KEY_SPACE              =GLFW_KEY_SPACE,
 KEY_APOSTROPHE         =GLFW_KEY_APOSTROPHE,  /* ' */
 KEY_COMMA              =GLFW_KEY_COMMA,		 /* , */
 KEY_MINUS              =GLFW_KEY_MINUS,		 /* - */
 KEY_PERIOD             =GLFW_KEY_PERIOD,		 /* . */
 KEY_SLASH              =GLFW_KEY_SLASH,		 /* / */
 KEY_0                  =GLFW_KEY_0,
 KEY_1					=GLFW_KEY_1,
 KEY_2                  =GLFW_KEY_2,
 KEY_3                  =GLFW_KEY_3,
 KEY_4                  =GLFW_KEY_4,
 KEY_5                  =GLFW_KEY_5,
 KEY_6                  =GLFW_KEY_6,
 KEY_7                  =GLFW_KEY_7,
 KEY_8                  =GLFW_KEY_8,
 KEY_9                  =GLFW_KEY_9,
 KEY_SEMICOLON          =GLFW_KEY_SEMICOLON,	 /* ; */
 KEY_EQUAL              =GLFW_KEY_EQUAL,		 /* = */
 KEY_A                  =GLFW_KEY_A,
 KEY_B                  =GLFW_KEY_B,
 KEY_C                  =GLFW_KEY_C,
 KEY_D                  =GLFW_KEY_D,
 KEY_E                  =GLFW_KEY_E,
 KEY_F                  =GLFW_KEY_F,
 KEY_G                  =GLFW_KEY_G,
 KEY_H                  =GLFW_KEY_H,
 KEY_I                  =GLFW_KEY_I,
 KEY_J                  =GLFW_KEY_J,
 KEY_K                  =GLFW_KEY_K,
 KEY_L                  =GLFW_KEY_L,
 KEY_M                  =GLFW_KEY_M,
 KEY_N                  =GLFW_KEY_N,
 KEY_O                  =GLFW_KEY_O,
 KEY_P                  =GLFW_KEY_P,
 KEY_Q                  =GLFW_KEY_Q,
 KEY_R                  =GLFW_KEY_R,
 KEY_S                  =GLFW_KEY_S,
 KEY_T                  =GLFW_KEY_T,
 KEY_U                  =GLFW_KEY_U,
 KEY_V                  =GLFW_KEY_V,
 KEY_W                  =GLFW_KEY_W,
 KEY_X                  =GLFW_KEY_X,
 KEY_Y                  =GLFW_KEY_Y,
 KEY_Z                  =GLFW_KEY_Z,
 KEY_LEFT_BRACKET       =GLFW_KEY_LEFT_BRACKET,	 /* [ */
 KEY_BACKSLASH          =GLFW_KEY_BACKSLASH,		/* \ */
 KEY_RIGHT_BRACKET      =GLFW_KEY_RIGHT_BRACKET,	/* ] */
 KEY_GRAVE_ACCENT       =GLFW_KEY_GRAVE_ACCENT,	 /* ` */
 KEY_WORLD_1            =GLFW_KEY_WORLD_1,			 /* non-US #1 */
 KEY_WORLD_2            =GLFW_KEY_WORLD_2,			/* non-US #2 */
 
/* Function keys */
 KEY_ESCAPE             =GLFW_KEY_ESCAPE, 
 KEY_ENTER              =GLFW_KEY_ENTER,
 KEY_TAB                =GLFW_KEY_TAB,
 KEY_BACKSPACE          =GLFW_KEY_BACKSPACE,
 KEY_INSERT             =GLFW_KEY_INSERT,
 KEY_DELETE             =GLFW_KEY_DELETE,
 KEY_RIGHT              =GLFW_KEY_RIGHT,
 KEY_LEFT               =GLFW_KEY_LEFT,
 KEY_DOWN               =GLFW_KEY_DOWN,
 KEY_UP                 =GLFW_KEY_UP,
 KEY_PAGE_UP            =GLFW_KEY_PAGE_UP,
 KEY_PAGE_DOWN          =GLFW_KEY_PAGE_DOWN,
 KEY_HOME               =GLFW_KEY_HOME,
 KEY_END                =GLFW_KEY_END, 
 KEY_CAPS_LOCK          =GLFW_KEY_CAPS_LOCK,
 KEY_SCROLL_LOCK        =GLFW_KEY_SCROLL_LOCK,
 KEY_NUM_LOCK           =GLFW_KEY_NUM_LOCK,
 KEY_PRINT_SCREEN       =GLFW_KEY_PRINT_SCREEN,
 KEY_PAUSE              =GLFW_KEY_PAUSE,
 KEY_F1                 =GLFW_KEY_F1,
 KEY_F2                 =GLFW_KEY_F2,
 KEY_F3                 =GLFW_KEY_F3,
 KEY_F4                 =GLFW_KEY_F4,
 KEY_F5                 =GLFW_KEY_F5,
 KEY_F6                 =GLFW_KEY_F6,
 KEY_F7                 =GLFW_KEY_F7,
 KEY_F8                 =GLFW_KEY_F8,
 KEY_F9                 =GLFW_KEY_F9,
 KEY_F10                =GLFW_KEY_F10,
 KEY_F11                =GLFW_KEY_F11,
 KEY_F12                =GLFW_KEY_F12,
 KEY_F13                =GLFW_KEY_F13,
 KEY_F14                =GLFW_KEY_F14,
 KEY_F15                =GLFW_KEY_F15,
 KEY_F16                =GLFW_KEY_F16,
 KEY_F17                =GLFW_KEY_F17,
 KEY_F18                =GLFW_KEY_F18,
 KEY_F19                =GLFW_KEY_F19,
 KEY_F20                =GLFW_KEY_F20,
 KEY_F21                =GLFW_KEY_F21,
 KEY_F22                =GLFW_KEY_F22,
 KEY_F23                =GLFW_KEY_F23,
 KEY_F24                =GLFW_KEY_F24,
 KEY_F25                =GLFW_KEY_F25,
 KEY_KP_0               =GLFW_KEY_KP_0,
 KEY_KP_1               =GLFW_KEY_KP_1,
 KEY_KP_2               =GLFW_KEY_KP_2,
 KEY_KP_3               =GLFW_KEY_KP_3,
 KEY_KP_4               =GLFW_KEY_KP_4,
 KEY_KP_5               =GLFW_KEY_KP_5,
 KEY_KP_6               =GLFW_KEY_KP_6,
 KEY_KP_7               =GLFW_KEY_KP_7,
 KEY_KP_8               =GLFW_KEY_KP_8,
 KEY_KP_9               =GLFW_KEY_KP_9,
 KEY_KP_DECIMAL         =GLFW_KEY_KP_DECIMAL, 
 KEY_KP_DIVIDE          =GLFW_KEY_KP_DIVIDE,
 KEY_KP_MULTIPLY        =GLFW_KEY_KP_MULTIPLY,
 KEY_KP_SUBTRACT        =GLFW_KEY_KP_SUBTRACT,
 KEY_KP_ADD             =GLFW_KEY_KP_ADD, 
 KEY_KP_ENTER           =GLFW_KEY_KP_ENTER, 
 KEY_KP_EQUAL           =GLFW_KEY_KP_EQUAL,
 KEY_LEFT_SHIFT         =GLFW_KEY_LEFT_SHIFT,
 KEY_LEFT_CONTROL       =GLFW_KEY_LEFT_CONTROL,
 KEY_LEFT_ALT           =GLFW_KEY_LEFT_ALT,
 KEY_LEFT_SUPER         =GLFW_KEY_LEFT_SUPER, 
 KEY_RIGHT_SHIFT        =GLFW_KEY_RIGHT_SHIFT,
 KEY_RIGHT_CONTROL      =GLFW_KEY_RIGHT_CONTROL,
 KEY_RIGHT_ALT          =GLFW_KEY_RIGHT_ALT,
 KEY_RIGHT_SUPER        =GLFW_KEY_RIGHT_SUPER,
 KEY_MENU               =GLFW_KEY_MENU,
 KEY_LAST               =GLFW_KEY_LAST
};
enum mouse_keys
{
	//mouse keys
 MOUSE_BUTTON_1         =GLFW_MOUSE_BUTTON_1,
 MOUSE_BUTTON_2         =GLFW_MOUSE_BUTTON_2,
 MOUSE_BUTTON_3         =GLFW_MOUSE_BUTTON_3,
 MOUSE_BUTTON_4         =GLFW_MOUSE_BUTTON_4,
 MOUSE_BUTTON_5         =GLFW_MOUSE_BUTTON_5,
 MOUSE_BUTTON_6         =GLFW_MOUSE_BUTTON_6,
 MOUSE_BUTTON_7         =GLFW_MOUSE_BUTTON_7,
 MOUSE_BUTTON_8         =GLFW_MOUSE_BUTTON_8,
 MOUSE_BUTTON_LAST      =GLFW_MOUSE_BUTTON_LAST,
 MOUSE_BUTTON_LEFT      =GLFW_MOUSE_BUTTON_LEFT,
 MOUSE_BUTTON_RIGHT     =GLFW_MOUSE_BUTTON_RIGHT,
 MOUSE_BUTTON_MIDDLE    =GLFW_MOUSE_BUTTON_MIDDLE

};
enum scroll_axis
{
	SCROLL_AXIS_VERTICAL =0,
	SCROLL_AXIS_HORIZONTAL = 1
};
#pragma endregion KEY_DEFS

class Keys
{
public:
	constexpr static int Key_Count = 349;
	constexpr static int Mouse_Count = 8;
	constexpr static int Scroll_Count = 2;	//we can only have 2 inputs, x/y.

	static double old_mouse_x, old_mouse_y;
	static double delta_x, delta_y, old_delta_x, old_delta_y;
	static float width, height;

	static void UpdateKeys(GLFWwindow* window)
	{
		for (int key = 0; key < keys.size(); key++)
		{
			SetKey(key, glfwGetKey(window, key));
		}
		for (int key = 0; key < mouse_keys.size(); key++)
		{
			SetMouseButton(key, glfwGetMouseButton(window, key));
		}
		for (int key = 0; key < scroll_state.size(); key++)
		{
			SetScroll(key);
		}
		MousePoll();
	}

	static void Init(int _width, int _height)
	{
		width = (float)_width;
		height = (float)_height;
		old_mouse_x = width / 2.0f;
		old_mouse_y = height / 2.0f;
	}
	static void SetKey(int key, int action)
	{
		if (key >= 0 && key < keys.size())
		{
			//determine if the key was just pressed this frame
			if (!keys[key].pressed && action == GLFW_PRESS)
			{
				keys[key].just_pressed = true;
				keys[key].toggle = !keys[key].toggle;
			}
			else
				keys[key].just_pressed = false;
			//determine if the key was just released this frame
			if (keys[key].pressed && action == GLFW_RELEASE)
				keys[key].just_released = true;
			else
				keys[key].just_released = false;
			//set the key's current status
			keys[key].pressed = action != 0;
		}
	}
	static bool GetKey(int key)
	{
		if (key >= 0 && key < keys.size())
		{
			return keys[key].pressed;
		}
		return false;
	}
	static bool KeyToggled(int key)
	{
		if (key >= 0 && key < keys.size())
		{
			return keys[key].toggle;
		}
		return false;
	}
	static bool KeyDown(int key)
	{
		if (key >= 0 && key < keys.size())
		{
			return keys[key].just_pressed;
		}
		return false;
	}
	static bool KeyUp(int key)
	{
		if (key >= 0 && key < keys.size())
		{
			return keys[key].just_released;
		}
		return false;
	}
	static void MouseMovementCallB(GLFWwindow* window, double x, double y)
	{
		if (!KeyToggled(KEY_F))
		{
			if (old_mouse_x != x)
			{
				delta_x = (x - old_mouse_x)/width;
				//glfwSetCursorPos(window, width / 2, y);
			}
			else
			{
				delta_x = 0;
			}
			if (old_mouse_y != y)
			{
				delta_y = (y - old_mouse_y)/height;

			}
			else
			{
				delta_y = 0;
			}
			glfwSetCursorPos(window, width / 2, height / 2);
			old_mouse_x = x;
			old_mouse_y = y;
			glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
		}
		else
		{
			glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
		}
	}
	static void MousePoll()
	{
		if (delta_x != 0)
			if (old_delta_x == delta_x)
			{
				delta_x = 0;
			}
		if (delta_y != 0)
			if (old_delta_y == delta_y)
			{
				delta_y = 0;
			}
		old_delta_x = delta_x;
		old_delta_y = delta_y;
	}
	static void KeyboardCallB(GLFWwindow* window, int key, int scancode, int action, int mods)
	{
		if (key == KEY_ESCAPE && action == GLFW_PRESS)
			glfwSetWindowShouldClose(window, GLFW_TRUE);
	}
	static void SetMouseButton(int key, int action)
	{
		if (key >= 0 && key < mouse_keys.size())
		{
			//determine if the key was just pressed this frame
			if (!mouse_keys[key].pressed && action == GLFW_PRESS)
			{
				mouse_keys[key].just_pressed = true;
				mouse_keys[key].toggle = !mouse_keys[key].toggle;
			}
			else
				mouse_keys[key].just_pressed = false;
			//determine if the key was just released this frame
			if (mouse_keys[key].pressed && action == GLFW_RELEASE)
				mouse_keys[key].just_released = true;
			else
				mouse_keys[key].just_released = false;
			//set the key's current status
			mouse_keys[key].pressed = action != 0;
		}
	}
	static bool GetMouseButton(int key)
	{
		if (key >= 0 && key < mouse_keys.size())
		{
			return mouse_keys[key].pressed;
		}
		return false;
	}
	static bool MouseButtonToggled(int key)
	{
		if (key >= 0 && key < mouse_keys.size())
		{
			return mouse_keys[key].toggle;
		}
		return false;
	}
	static bool MouseButtonDown(int key)
	{
		if (key >= 0 && key < mouse_keys.size())
		{
			return mouse_keys[key].just_pressed;
		}
		return false;
	}
	static bool MouseButtonUp(int key)
	{
		if (key >= 0 && key < mouse_keys.size())
		{
			return mouse_keys[key].just_released;
		}
		return false;
	}

	static double ScrollAxis(int axis)
	{
		if (axis >= 0 && axis < scroll_state.size())
		{
			return scroll_state[axis];
		}
		return 0;
	}
	static void SetScrollEvent(GLFWwindow* window, double x, double y)
	{
		scroll_event[SCROLL_AXIS_HORIZONTAL] = x;
		scroll_event[SCROLL_AXIS_VERTICAL] = y;
	}
	static void SetScroll(int key)
	{
		if (key >= 0 && key < scroll_state.size())
		{
			if (scroll_event[key] != scroll_state[key])
			{
				scroll_state[key] = scroll_event[key];
				scroll_event[key] = 0;
			}
			else
			{
				//sometimes we can skip a callback, so if the two scroll states happen to be the same, forcibly set them to zero.
				//this is to fix an issue when you scroll rapidly causing the state to stick.
				scroll_event[key] = 0;
			}
		}
	}

private:
	struct keyBools
	{
		//the button is currently pressed
		unsigned char pressed : 1;
		//this frame the button was pressed
		unsigned char just_pressed : 1;
		//this frame the button was released
		unsigned char just_released : 1;
		//the button was toggled e.g. pressed, then depressed then pressed again for this to toggle from t->f
		unsigned char toggle : 1;
	};
	//these are already initiliazed in the .cpp
	static std::array<keyBools,Key_Count> keys;
	static std::array<keyBools,Mouse_Count> mouse_keys;
	static std::array<double, Scroll_Count> scroll_state;
	//we have to do this due to the scroll event not occuring frequently enough.
	//e.g. the player can scroll back and the value will be held until the next callback, obviously this is bad.
	static std::array<double,Scroll_Count> scroll_event;
};

