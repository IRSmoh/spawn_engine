#pragma once
#include <vector>
#include "height_map.h"
#include "thread_pool.h"
namespace spawn_engine
{
	struct height_map_manager
	{
		using height_map_gen_and_callables = std::unordered_map<glm::ivec3, std::vector<shared_call_on_death>>;
		constexpr static int height_map_dim = 64;
		using height_map_type = height_map<height_map_dim>;

		struct max_maps_per_level
		{
			constexpr static int level_0_max_maps = 6;
			constexpr static int level_1_plus_max_maps = 6;
			//static_assert(is_power_of_two(level_1_plus_max_maps), "height_map_manager::max_maps_per_level::level_1_plus_max_maps must be a power of 2.");
			constexpr static int level_0_max_load = level_0_max_maps * level_0_max_maps*level_0_max_maps;
			constexpr static int level_1_plus_max_load = level_1_plus_max_maps * level_1_plus_max_maps*level_1_plus_max_maps;
		};
		struct hmap_and_dependents
		{
			using dependents_vec_type = tbb::concurrent_vector<shared_call_on_death>;
			std::shared_ptr<height_map_type> hmap;
			std::shared_ptr<dependents_vec_type> dependents;
			hmap_and_dependents(height_map_type&& hmap_) : hmap(std::make_shared<height_map_type>(hmap_)), dependents(std::make_shared<dependents_vec_type>()) {}
		};
		using built_hmap_collection_type = tbb::concurrent_vector<std::shared_ptr<height_map_type>>;
		mutexed_struct<std::vector<std::unordered_map<glm::ivec2, std::shared_ptr<height_map_type>>>, std::shared_mutex> heightmaps;
		std::vector<std::unordered_map<glm::ivec2, hmap_and_dependents>> pending_heightmaps;
		mutexed_struct<built_hmap_collection_type, std::shared_mutex> built_heightmaps;

		std::function<float(const glm::vec2&)> hmap_field_generator;

		glm::vec2 get_bounded_pos(const glm::ivec2& pos, int height_map_scale) const
		{
			return pos;
			constexpr int max_load = max_maps_per_level::level_0_max_maps;
			return (((pos / height_map_dim) >> height_map_scale) % max_load + max_load) % max_load;	//if the level 0 would just be power of 2 it'd be so much easier... but instead its somewhere between 4/6.
		}
		const std::shared_ptr<height_map_type> get_map_in_slot(const glm::ivec2 lookup_pos, int height_map_scale)
		{
			const auto hmaps = heightmaps.get_shared();
			if (height_map_scale >= hmaps->size())
				return nullptr;
			auto candidate = (*hmaps)[height_map_scale].find(lookup_pos);
			if (candidate == (*hmaps)[height_map_scale].end())
				return nullptr;
			return candidate->second;
		}
		const std::shared_ptr<height_map_type> get_map_from_world(const glm::vec3& world_coords, int height_map_scale)
		{
			glm::ivec2 lookup_pos = get_bounded_pos({ world_coords.x,world_coords.z }, height_map_scale);
			auto candidate = get_map_in_slot(lookup_pos, height_map_scale);
			if (!candidate)
				return nullptr;
			if (candidate->world_pos != glm::ivec2{ world_coords.x, world_coords.z })
				return nullptr;
			return candidate;
		}
		hmap_and_dependents* get_pending_map_in_slot(const glm::vec2& lookup_pos, int height_map_scale)
		{
			if (height_map_scale >= pending_heightmaps.size())
				return nullptr;
			auto candidate = pending_heightmaps[height_map_scale].find(lookup_pos);
			if (candidate == pending_heightmaps[height_map_scale].end())
				return nullptr;
			return &candidate->second;
		}
		void finalize_hmaps()
		{
			built_hmap_collection_type built_hmaps_tmp;
			{
				built_heightmaps.get()->swap(built_hmaps_tmp);
			}
			{
				auto hmaps = heightmaps.get();
				for (auto& hmap : built_hmaps_tmp)
				{
					glm::ivec2 lookup_pos = get_bounded_pos(hmap->world_pos, hmap->map_scale);
					for (size_t i = hmaps->size(); i <= hmap->map_scale; ++i)
						hmaps->push_back({});
					(*hmaps)[hmap->map_scale].insert_or_assign(lookup_pos, hmap);
				}
			}
			for (auto& hmap : built_hmaps_tmp)
			{
				//this is currently a race condition with deleting the heightmap below causing all pending chunks to start being generated
				//and having heightmaps actually updated so when a thread attempts to shortly access it it gets a valid shared_ptr.
				pending_heightmaps[hmap->map_scale].erase(hmap->world_pos);
			}
		}
		void generate_height_maps(const height_map_gen_and_callables& height_creation_container, thread_pool& pool)
		{
			for (auto&& creation_data : height_creation_container)
			{
				glm::ivec2 lookup_pos = creation_data.first;
				int height_map_scale = creation_data.first.z;
				//the heightmap has already been built. once this function exists all chunks that would depend on it can be created & started.
				if (get_map_from_world({ lookup_pos.x, 0, lookup_pos.y }, height_map_scale))
					continue;

				//see if we have an existing pending heightmap.
				if (auto candidate_ptr = get_pending_map_in_slot(lookup_pos, height_map_scale))
				{
					for (auto&& dependent : creation_data.second)
						candidate_ptr->dependents->push_back(dependent);
					continue;
				}
				//else we have to create a heightmap and add it to the thread pool.
				for (size_t i = pending_heightmaps.size(); i <= height_map_scale; ++i)
					pending_heightmaps.push_back({});

				auto itr = pending_heightmaps[height_map_scale].insert({ lookup_pos, height_map_type{ lookup_pos, 1 << height_map_scale } });
				auto dependents = itr.first->second.dependents;
				auto hmap_ptr = itr.first->second.hmap;
				for (auto& dependent : creation_data.second)
					dependents->push_back(dependent);

				auto hmap_generator = [dependents, hmap_ptr, &hmap_field_generator = this->hmap_field_generator, &built_heightmaps = built_heightmaps]()
				{
					hmap_ptr->create_map(hmap_field_generator);
					built_heightmaps.get_shared()->push_back(hmap_ptr);
				};
				pool.add_job(hmap_generator);
			}
		}
		void set_height_map_generator(std::function<float(const glm::vec2&)> generator)
		{
			hmap_field_generator = generator;
		}
	};
}