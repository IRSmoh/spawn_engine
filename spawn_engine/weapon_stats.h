#pragma once
#include <vector>
#include <functional>
#include "random_generators.h"
namespace spawn_engine
{
	namespace weapons
	{
		using id_type = int;
		using damage_value_type = int;
		struct damage_value
		{
			id_type id;
			damage_value_type damage;
		};
		struct damage_roll
		{
		private:
			enum damage_underflow_handling
			{
				e_throw,
				e_clamp,
				e_no_handling
			};
		public:

			id_type damage_id;
			damage_value_type min_damage;
			damage_value_type max_damage;

			constexpr static damage_underflow_handling underflow_handling = e_throw;
			//insert a check to make sure you can't have min > max damage.
			damage_roll(id_type id, damage_value_type min, damage_value_type max) : damage_id(id), min_damage(min), max_damage(max)
			{
				if constexpr(underflow_handling == e_throw)
				{
					if (min > max)
						throw std::runtime_error("weapon_stats::damage_roll Attempting to set a min damage greater than the supplied max damage.");
				}
				else if constexpr(underflow_handling == e_clamp)
				{
					if (min > max)
						min_damage = max_damage;
				}
			}

			//sfinae'd to make sure the result_type actually exists and to make sure its an integral rng since assumptions fall apart if its not.

			template<typename rng_type, typename result_type = typename rng_type::result_type, std::enable_if<std::is_integral<result_type>::value && sizeof(result_type) >= sizeof(damage_type), bool>::type = true>
			damage_value roll(rng_type& rng) const
			{
				damage_value_type diff = max_damage - min_damage;
				typename rng_type::result_type rng_roll = rng();
				return { damage_id,min_damage + (rng_roll%diff) };
			}
		};
		struct weapon_stats
		{
			//need to support an unknown amount of flat damage rolls
			//need to support damage conversion
			//need to support buffing things that are not weapon related
			//need to be able to get an easy-to-use roll of the above.
			std::vector<damage_roll> damage_rolls;

			template<typename rng_type>
			std::vector<damage_value> roll(rng_type& rng) const
			{
				std::vector<damage_value> values;
				for (auto& damage_roll : damage_rolls)
				{
					values.push_back(damage_roll.roll(rng));
				}
				return values;
			}
		};
		struct damage_type
		{
			id_type damage_id;					//maybe not needed but placing for now
			std::string damage_display_name;	//aka Physical or Cold or whatever.
			std::function<damage_value_type(float, int, damage_value_type)> reduction_function;
		};
		struct damage_reduction
		{
			id_type id;
			//whatever the resist function is
			float percentage_reduction;
			int flat_effects;	//e.g. if we had 10k armor or something and fed it into our function it'd come out with... something. probably like -900 phys or some shit
			std::function<damage_value_type(float, int, damage_value_type)> reduction_function;
		};
		struct damage_reductions
		{
			//?
			//we could have flat resists
			//massive resists that work with a function to determine final value (for whatever reason if we decide that 500+ res is a thing it should be remapped into a 0-100 range)
			//large flat values similar to poe/other armor/evasion
			//wherever this is supposed to go...
			damage_value_type calculate_final_damage_value(const std::vector<damage_value>& damage_rolls)
			{

			}
		};

	}
}