#pragma once
#include <glm\glm.hpp>
namespace spawn_engine
{
	template<size_t height_map_max_dim>
	class height_map
	{
		friend struct height_map_manager;
	public:
		struct height_map_data
		{
			std::vector<float> map;
			size_t dim;
			height_map_data(size_t _dim) : map(std::vector<float>(_dim*_dim)), dim(_dim) {}
			float& operator[](const glm::ivec2& pos)
			{
				return map[pos.x + pos.y*dim];
			}
			const float& operator[](const glm::ivec2& pos) const
			{
				return map[pos.x + pos.y*dim];
			}
		};
		struct height_map_mip_data
		{
			struct map_extremes
			{
				float min, max;
			};
			std::vector<map_extremes> map;
			size_t dim;
			height_map_mip_data(size_t _dim) : map(std::vector<map_extremes>(_dim*_dim)), dim(_dim) {}
			map_extremes& operator[](const glm::ivec2& pos)
			{
				return map[pos.x + pos.y*dim];
			}
			const map_extremes& operator[](const glm::ivec2&pos) const
			{
				return map[pos.x + pos.y*dim];
			}
		};

		constexpr static size_t height_map_dim = height_map_max_dim;	//our octrees sample [0,64] so we have to have an additional element. oh well. its either that or have them load a new heightmap to check.
	private:
		height_map_data map;
		std::vector<height_map_mip_data> map_mips;
		glm::ivec2 world_pos;
		glm::vec3 world_pos3d;
		int map_scale;
		int map_size;
	public:
		height_map() : height_map({}, 1)
		{

		}
		height_map(glm::ivec2 pos, int map_scale_) : map({ 0 }), world_pos(pos), world_pos3d({ pos.x,0,pos.y }), map_scale(bit_scan(map_scale_)), map_size(1 << map_scale)
		{

		}
		//node_size goes.. 64->1 where 1 is higher granularity (e.g. more precise)
		typename height_map_mip_data::map_extremes get_density_extremes(const glm::ivec2& _pos, int node_size) const
		{
			//64 [4]	->6 as the ind
			//32 <skip to 16> sample 4 spots instead
			//16
			//8

			int32_t ind = bit_scan(node_size);
			ind -= map_scale;

			glm::ivec2 pos = _pos >> (bit_scan(node_size));
			if (ind == 0)
				return { map[pos], map[pos] };
			return map_mips[ind - 1][pos];
		}
		bool contains_crossing_point(const glm::ivec3& center_point_in_world, int node_size) const
		{
			//the heightmaps do not currently support one singular sample at max size; so if you request that size we can just infer you meant to do it at the next step down.
			if (bit_scan(node_size) - map_scale == 6)
				node_size >>= 1;
			float min = std::numeric_limits<float>::max();
			float max = -min;
			float base_height = (float)center_point_in_world.y;
			glm::ivec2 relative_pos = glm::ivec2{ center_point_in_world.x, center_point_in_world.z } -world_pos;
			for (int z = 0; z < 2; ++z)
				for (int x = 0; x < 2; ++x)
				{
					typename height_map_mip_data::map_extremes extremes = get_density_extremes(relative_pos + glm::ivec2{ x,z }*node_size, node_size);

					if (!(extremes.min > (base_height + node_size) || extremes.max < (base_height)))
						return true;

					if (extremes.min < min)
						min = extremes.min;
					if (extremes.max > max)
						max = extremes.max;
				}
			//check if either min/max land inside of our sub region
			//OR if the min + max are both out of their respective bounds (the edge must cross us).
			return (min > base_height && min < (base_height + node_size)) || (max > base_height && max < (base_height + node_size)) || (min < base_height && max >(base_height + node_size));
		}
		float get_density(const glm::ivec2& pos) const
		{
			return map[pos];
		}
		//input a world co-ordinate instead of local co-ordinates
		//this will auto convert to local for you.
		float get_world_density(const glm::ivec2& pos) const
		{
			return get_density((pos - world_pos) >> map_scale);
		}
		//input a world co-ordinate instead of local co-ordinates
		//this will auto convert to local for you.
		float get_world_density(const glm::vec3& pos) const
		{
			return get_density((pos - world_pos3d) / map_size);
		}
		float get_density(const glm::vec3& pos) const
		{
			//determine if our sampling for the heightmap lies on an integer boundary; if it does we don't have to calculate multiple blends.
			glm::ivec2 candidate_pos{ pos.x,pos.z };

			//pure int boundary
			bool x_is_int = pos.x == candidate_pos.x;
			bool z_is_int = pos.z == candidate_pos.y;
			if (x_is_int && z_is_int)
				return get_density(candidate_pos);


			//check to see if we're just scanning along one of the int planes
			if (x_is_int || z_is_int)
			{
				//x has a fractional part....
				if (!x_is_int)
				{
					float pos_x = pos.x - candidate_pos.x;
					return glm::mix(get_density({ pos.x, pos.z }), get_density({ pos.x + 1, pos.z }), pos_x);
				}
				//z has a fractional part....
				else
				{
					float pos_z = pos.z - candidate_pos.y;
					return glm::mix(get_density({ pos.x, pos.z }), get_density({ pos.x, pos.z + 1 }), pos_z);
				}
			}

			//checking somewhere in the middle of the heightmap grid/off integer grid
			float pos_x = pos.x - candidate_pos.x;
			float pos_z = pos.z - candidate_pos.y;

			float zmins = glm::mix(get_density(candidate_pos), get_density({ candidate_pos.x + 1, candidate_pos.y }), pos_x);
			float zmaxs = glm::mix(get_density({ candidate_pos.x, candidate_pos.y + 1 }), get_density({ candidate_pos.x + 1, candidate_pos.y + 1 }), pos_x);
			return glm::mix(zmins, zmaxs, pos_z);
		}
		template<typename generator_type>
		static height_map create_map(int32_t map_scale, const glm::vec2& world_pos, generator_type& generator)
		{
			//a bonus overlap of 2 instead of the default one for the smallest size only***
			//allows us to have sane normals between chunks and prevent normal fields from going crazy (the texture/normals appear to have tight loops otherwise).
			height_map heightmap{ world_pos, map_scale };
			heightmap.create_map(generator);
			return heightmap;
		}
		template<typename generator_type>
		void create_map(generator_type& generator)
		{
			constexpr size_t border_size = 2;
			constexpr size_t height_map_dim = height_map::height_map_dim + border_size;

			height_map_data sub_map{ height_map_dim };
			for (int z = 0; z < (int)height_map_dim; ++z)
			{
				for (int x = 0; x < (int)height_map_dim; ++x)
				{
					sub_map[{x, z}] = generator(glm::ivec2(world_pos.x, world_pos.y) + (glm::ivec2(x, z) << map_scale));
				}
			}
			map = std::move(sub_map);

			add_mipped_heightmaps();
		}
		void add_mipped_heightmaps()
		{
			if (map_mips.size() != 0)
				throw std::runtime_error("Attempting to add mipmaps to a heightmap that already has mipmaps.");

			constexpr size_t border_size = 2;
			constexpr size_t mip_border_size = 1;
			constexpr float absurd_size = std::numeric_limits<float>::max();
			constexpr int mip_down_scale_factor = 2;
			constexpr size_t height_map_dim = height_map::height_map_dim + border_size;
			{
				constexpr size_t mip_size = 2;
				constexpr size_t map_dim = height_map::height_map_dim / mip_size + mip_border_size;
				size_t map_upper_limits = height_map::height_map_dim / mip_size;
				height_map_mip_data sub_map{ map_dim };
				height_map_data& prior_map = map;
				for (int z = 0; z < map_dim; ++z)
				{
					for (int x = 0; x < map_dim; ++x)
					{
						//inner tiny sampler
						float min = absurd_size;
						float max = -absurd_size;
						for (int z_sampler = 0; z_sampler < mip_down_scale_factor; ++z_sampler)
						{
							for (int x_sampler = 0; x_sampler < mip_down_scale_factor; ++x_sampler)
							{
								float candidate_weight;
								if (x + x_sampler > map_upper_limits || z + z_sampler > map_upper_limits)
									candidate_weight = prior_map[{x*mip_down_scale_factor + (x + x_sampler > map_upper_limits ? 0 : x_sampler), z*mip_down_scale_factor + (z + z_sampler > map_upper_limits ? 0 : z_sampler)}];
								else
									candidate_weight = prior_map[{x*mip_down_scale_factor + x_sampler, z*mip_down_scale_factor + z_sampler}];

								if (candidate_weight < min)
									min = candidate_weight;
								if (candidate_weight > max)
									max = candidate_weight;
							}
						}
						sub_map[{x, z}] = { min,max };
					}
				}
				map_mips.emplace_back(std::move(sub_map));
			}
			for (size_t mip_size = 4; mip_size < height_map::height_map_dim; mip_size <<= 1)
			{
				size_t map_dim = height_map::height_map_dim / mip_size + mip_border_size;
				size_t map_upper_limits = height_map::height_map_dim / mip_size - mip_border_size;
				height_map_mip_data sub_map{ map_dim };
				height_map_mip_data& prior_map = map_mips.back();
				for (int z = 0; z < map_dim; ++z)
				{
					for (int x = 0; x < map_dim; ++x)
					{
						//inner tiny sampler
						float min = absurd_size;
						float max = -absurd_size;
						for (int z_sampler = 0; z_sampler < mip_down_scale_factor; ++z_sampler)
						{
							for (int x_sampler = 0; x_sampler < mip_down_scale_factor; ++x_sampler)
							{
								typename height_map_mip_data::map_extremes candidate_weight;
								if (x > map_upper_limits || z > map_upper_limits)
									candidate_weight = prior_map[{x*mip_down_scale_factor + (x > map_upper_limits ? 0 : x_sampler), z*mip_down_scale_factor + (z > map_upper_limits ? 0 : z_sampler)}];
								else
									candidate_weight = prior_map[{x*mip_down_scale_factor + x_sampler, z*mip_down_scale_factor + z_sampler}];

								if (candidate_weight.min < min)
									min = candidate_weight.min;
								if (candidate_weight.max > max)
									max = candidate_weight.max;
							}
						}
						sub_map[{x, z}] = { min,max };
					}
				}
				map_mips.emplace_back(std::move(sub_map));
			}
		}
	};
}