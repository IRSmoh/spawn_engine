#pragma once
#include <functional>
#include <memory>
#include <variant>
#include <glm/glm.hpp>
#include <algorithm>
namespace spawn_engine
{
	struct sdf
	{
		enum op_e
		{
			add_op,
			sub_op,
			intersect_op,
			displace_op,
			blend_op
		};
		using sdf_function_type = std::function<float(const glm::vec3&)>;
		using mixing_op_type = std::function<float(float, float)>;
		using optional_check_type = std::function<bool(const glm::vec3&, float)>;
		using ops_type = std::tuple<sdf, sdf, op_e>;

		std::variant<sdf_function_type, mixing_op_type> func_or_op;
		std::unique_ptr<ops_type> ops;
		//used only for cases like heightmaps where its incredibly difficult to coerce them to be a proper sdf.
		std::unique_ptr<optional_check_type> optional_bound_check;
		bool is_valid : 1;
		bool is_simple : 1;

		template<typename callable>
		sdf(callable&& func) :
			func_or_op(std::forward<callable>(func)),
			is_valid(true),
			is_simple(true)
		{}
		template<typename callable, typename bound_checker>
		sdf(callable&& func, bound_checker&& checker) :
			func_or_op(std::forward<callable>(func)),
			optional_bound_check(std::make_unique<optional_check_type>(std::forward<bound_checker>(checker))),
			is_valid(true),
			is_simple(false)
		{}
		template<typename mixing_fnc>
		sdf(const sdf& left, const sdf& right, op_e mixing_func_op, const mixing_fnc& mix) :
			func_or_op(mix),
			ops(std::make_unique<ops_type>(left, right, mixing_func_op)),
			is_valid(true),
			is_simple(left.is_simple && right.is_simple)
		{}

		sdf(const sdf& right) : func_or_op(right.func_or_op), is_valid(right.is_valid), is_simple(right.is_simple)
		{
			if (right.ops)
				ops = std::make_unique<ops_type>(*right.ops);
			if (right.optional_bound_check)
				optional_bound_check = std::make_unique<optional_check_type>(*right.optional_bound_check);
		}
		sdf(sdf&& right) : 
			func_or_op(std::move(right.func_or_op)),
			ops(std::move(right.ops)),
			optional_bound_check(std::move(right.optional_bound_check)),
			is_valid(right.is_valid),
			is_simple(right.is_simple)
		{}
		sdf& operator=(const sdf& right)
		{
			func_or_op = right.func_or_op;
			is_valid = right.is_valid;
			is_simple = right.is_simple;

			if (right.ops)
				ops = std::make_unique<ops_type>(*right.ops);
			if (right.optional_bound_check)
				optional_bound_check = std::make_unique<optional_check_type>(*right.optional_bound_check);
			return *this;
		}
		sdf& operator=(sdf&& right)
		{
			func_or_op = std::move(right.func_or_op);
			is_valid = right.is_valid;
			is_simple = right.is_simple;

			ops = std::move(right.ops);
			optional_bound_check = std::move(right.optional_bound_check);
			return *this;
		}
		//attempts some basic simplification in the region [origin,origin+region_dim]
		//if all sdfs are removed instead returns +inf for the sub region described above.
		void collapse_field_in_region(const glm::vec3& origin, float region_dim)
		{
			collapse_field_in_region_impl(origin, region_dim);
			if (!is_valid)
			{
				ops = nullptr;
				func_or_op = [](const glm::vec3&) {return std::numeric_limits<float>::max(); };
			}
		}
		static bool contains_edge_crossing(const sdf& sdf_to_check, const glm::vec3& region_origin, float region_dim)
		{
			if (sdf_to_check.is_simple)
			{
				constexpr float sqrt_3 = 1.7320508075688772935274463415059f;
				float bounding_sphere = sqrt_3 * region_dim / 2.f;
				glm::vec3 center = region_origin + region_dim / 2;
				if (abs(sdf_to_check(center)) < bounding_sphere)
					return true;
				return false;
			}
			if (sdf_to_check.ops)
			{
				return contains_edge_crossing(std::get<0>(*sdf_to_check.ops), region_origin, region_dim) || contains_edge_crossing(std::get<1>(*sdf_to_check.ops), region_origin, region_dim);
			}
			if (sdf_to_check.optional_bound_check)
				return  (*sdf_to_check.optional_bound_check)(region_origin, region_dim);

			return true;	//should be unreachable but incase.
		}
		//returns true if the entire bounded region would have no edge crossings.
		void collapse_field_in_region_impl(const glm::vec3& region_origin, float region_dim)
		{
			//reminder: + is empty space - is inside geometry
			constexpr float sqrt_3 = 1.7320508075688772935274463415059f;
			float bounding_sphere = sqrt_3 * region_dim / 2.f;
			glm::vec3 center = region_origin + region_dim / 2;

			auto should_collapse_lam = [&](const sdf& sdf_to_check)
			{
				if (!sdf_to_check.is_simple)
					return false;
				return !contains_edge_crossing(sdf_to_check, region_origin, region_dim);
			};
			auto collapse_sdf = [&](sdf& other)
			{
				other.collapse_field_in_region_impl(region_origin, region_dim);
				if (other.is_valid)
				{
					*this = std::move(other);
					return true;
				}
				is_valid = false;
				return false;
			};
			if (should_collapse_lam(*this))
			{
				//remove everything
				is_valid = false;
				return;
			}
			if (!ops)
				return;

			auto[left, right, op_type] = *ops;
			switch (op_type)
			{
			case add_op:
			{
				//either can be collapsed since they're independent of each other.
				//either can be collapsed since they're independent of each other.
				if (should_collapse_lam(left))
				{
					//we don't want the left side anymore so...
					collapse_sdf(right);
					return;
				}
				else if (should_collapse_lam(right))
				{
					//right side isn't required....
					collapse_sdf(left);
					return;
				}
				break;
			}
			case sub_op:
			{
				//if the object we're working on is entirely out of the zone throw it away.
				//if the 'minus' sections are out of bounds we don't need to check them and can collapse their functions/checks.

				//this case is implicit and doesn't need to be checked.
				//if (should_collapse_lam(left))
				//{
				//	is_valid = false;
				//	return;
				//}
				if (!right.is_simple)
					return;
				float right_val = right(center);
				if (right_val > bounding_sphere)
				{
					//the removed geometry can't affect us; remove the check entirely.
					collapse_sdf(left);
					return;
				}
				//the removing geometry is large enough to fully remove all geometry in our subsection. (how does this get hit but fail the earlier check...?)
				if (right_val < -bounding_sphere)
				{
					is_valid = false;
					return;
				}
				break;
			}
			//case intersect_op:
			//{
			//due to the way the sdf's work e.g. they approximate the shape of the object but don't necessarily cross over for every function...
			//intersect is the overlap area.
			//so if either of the overlaps are too positive we can drop the entire function.
			//interior is handled implicility earlier.
			//this is implicit though.. so this can only be a pointless check.

			//if (left(center) > bounding_sphere || right(center) > bounding_sphere)
			//{
			//drop everything
			//is_valid = false;
			//}
			//break;
			//}
			default:
				//there is no simple way of removing anything from this that would not also pass on the generic check.
				//displacement can't either...
				//blend is plausible? but it depends on the blend factor and nothing stores that easily right now.
				break;
			}
		}

		float operator()(const glm::vec3& input_pos) const
		{
			if (ops)
			{
				return std::get<mixing_op_type>(func_or_op)(std::get<0>(*ops)(input_pos), std::get<1>(*ops)(input_pos));
			}
			return std::get<sdf_function_type>(func_or_op)(input_pos);
		}

		sdf operator+(const sdf& right) const
		{
			return { *this,right, add_op, [](float left, float right)
			{
				return glm::min(left,right);
			} };
		}
		sdf operator-(const sdf& right) const
		{
			return { *this,right, sub_op, [](float left, float right)
			{
				return glm::max(left, -right);
			} };
		}
		sdf operator%(const sdf& right) const
		{
			return { *this,right, intersect_op, [](float left, float right)
			{
				return glm::max(left, right);
			} };
		}
		sdf operator*(float scalar) const
		{
			return [instance = *this, scalar](const glm::vec3& world_pos)
			{
				return instance(world_pos/scalar)*scalar;
			};
		}
		sdf operator+(const glm::vec3& translation) const
		{
			return [instance = *this, trans = translation](const glm::vec3& world_pos)
			{
				return instance(world_pos - trans);
			};
		}
		friend sdf scale(const sdf& left_sdf, float scalar)
		{
			return left_sdf*scalar;
		}
		friend sdf translate(const sdf& left_sdf, const glm::vec3& translation)
		{
			return left_sdf + translation;
		}
		friend sdf rotate(const sdf& left_sdf, const glm::mat4& rotation)
		{
			auto rot = glm::inverse(rotation);
			return[instance = left_sdf, rot](const glm::vec3& world_pos)
			{
				return instance(rot*glm::vec4(world_pos,0.f));
			};
		}
		friend sdf inverse(const sdf& left)
		{
			auto new_lam = [self = left](const glm::vec3& world_pos) { return -self(world_pos); };
			if (left.optional_bound_check)
				return { new_lam, *left.optional_bound_check };
			return new_lam;
		}
		friend sdf displacement(const sdf& left_sdf, const sdf& right_sdf, float left_weight, float right_weight)
		{
			return { left_sdf, right_sdf, sdf::displace_op, [left_weight,right_weight](float left, float right)
			{
				return (left*left_weight + right * right_weight) / (left_weight + right_weight);
			} };
		}
		friend sdf displacement(const sdf& left, const sdf& right)
		{
			return displacement(left, right, 1, 1);
		}

		static float smooth_min_poly(float left, float right, float blend_factor)
		{
			auto mix = [](auto left, auto right, auto distance)
			{
				return left * (1 - distance) + right * distance;
			};
			float h = std::clamp(0.5f + 0.5f*(right - left) / blend_factor, 0.f, 1.f);
			return mix(right, left, h) - blend_factor * h*(1.0f - h);
		};

		friend sdf smooth_union(const sdf& left_sdf, const sdf& right_sdf, float blend_factor = 0.1f)
		{
			return { left_sdf, right_sdf, sdf::blend_op,[blend_factor](float left, float right)
			{
				return smooth_min_poly(left,right,blend_factor);
			} };
		}
		friend sdf smooth_subtraction(const sdf& left_sdf, const sdf& right_sdf, float blend_factor)
		{
			return { left_sdf,right_sdf,sdf::blend_op,[blend_factor](float left, float right)
			{
				float h = std::clamp(0.5f - 0.5f*(right + left) / blend_factor, 0.f, 1.f);
				return glm::mix(right, -left, h) + blend_factor * h*(1.f - h);
			} };
		}
		friend sdf smooth_interesction(const sdf& left_sdf, const sdf& right_sdf, float blend_factor)
		{
			return { left_sdf,right_sdf,sdf::blend_op,[blend_factor](float left, float right)
			{
				float h = std::clamp(0.5f - 0.5f*(right - left) / blend_factor,0.f,1.f);
				return glm::mix(right, left, h) + blend_factor * h*(1.f - h);
			} };
		}

		template<typename callable>
		static sdf extrusion(const callable& sdf2d, float extrude_distance)
		{
			return [sdf2d=sdf2d, extrude_distance](const glm::vec3& world_pos)
			{
				float distance = sdf2d(glm::vec2{ world_pos.x,world_pos.y });
				glm::vec2 w = glm::vec2(distance, glm::abs(world_pos.z) - extrude_distance);
				return glm::min(glm::max(w.x, w.y), 0.f) + glm::length(glm::max(w, glm::vec2{ 0.f }));
			};
		}
		template<typename callable>
		static sdf revolution(const callable& sdf2d, float radius)
		{
			return [sdf2d = sdf2d, radius](const glm::vec3& world_pos)
			{
				glm::vec2 q = glm::vec2(glm::length(glm::vec2{ world_pos.x,world_pos.z }) - radius, world_pos.y);
				return sdf2d(q);
			};
		}
	};
}