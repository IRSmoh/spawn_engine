#pragma once
#include <spawnx/spawnx.h>
#include <vector>
#include <unordered_map>
#include <variant>
#include <glm/glm.hpp>
#include <imgui.h>

//move this somewhere sane but for now I don't care.
ImVec4 rgb_color_to_imvec4(glm::u8vec3 color_code)
{
	glm::vec3 colors = glm::vec3(color_code) / 255.f;
	return { colors.x,colors.y,colors.z,1.f };
}
struct fire_tag
{
	static ImVec4 display_color()
	{
		return rgb_color_to_imvec4({ 207, 16, 20 });
	}
};
struct cold_tag 
{
	static ImVec4 display_color()
	{
		return rgb_color_to_imvec4({ 131, 203, 204 });
	}
};
struct wind_tag
{
	static ImVec4 display_color()
	{
		return rgb_color_to_imvec4({ 225, 225, 223 });
	}
};
struct lightning_tag
{
	static ImVec4 display_color()
	{
		return rgb_color_to_imvec4({ 228, 232, 17 });
	}
};
struct abyss_tag 
{
	static ImVec4 display_color()
	{
		return rgb_color_to_imvec4({ 140, 2, 2 });
	}
};
struct resistance_common
{
	using res_value_type = float;
	using damage_ret_type = float;
	using damage_in_type = float;
	res_value_type res_value;
	damage_ret_type apply_resistance(damage_in_type damage_amount)
	{
		//positive values indicate doing less damage, negative values indicate doing more damage
		return ((damage_in_type)(1) - res_value)*damage_amount;
	}
};
template<typename res_type>
struct resistance : resistance_common 
{
	using tag_type = res_type;
};
struct armor_stats
{
	std::int64_t armor_raiting;
	std::int64_t additional_flat_reduction;
private:
	constexpr static std::int64_t armor_factor = 10;
public:
	static float effective_armor_reduction(std::int64_t base_armor, float hit)
	{
		return base_armor / (base_armor + hit * armor_factor);
	}
	//current idea for armor..
	//get it to mitagate a scaling % of the damage dealt, where huge hits = nearly useless, and small hits exceedingly effective.
	//small hits should be nearly entirely ignored. e.g. getting hit by bird shot w/ a classical knight armor would do basically nothing (unless it hit your eyes but w/e)
	//to meet those ends, have a damage scaling function, and some flat mitigation... this should also give some interesting points to itemize around.

	float mitigate(float hit) const
	{
		return std::clamp(0.f, hit, effective_armor_reduction(armor_raiting, hit) - additional_flat_reduction);
	}
	static ImVec4 display_color()
	{
		return rgb_color_to_imvec4({ 192, 172, 98 });
	}
};
struct evasion_stats
{
	std::int64_t evasion_raiting;
	std::int64_t accuracy_ignore_raiting;
private:
	std::int64_t remaining_evasion_raiting;
	std::int64_t remaining_crit_evasion_raiting;

	constexpr static std::int64_t enemy_accuracy_effectiveness = 4;
	constexpr static std::int64_t enemy_crit_effectiveness = enemy_accuracy_effectiveness * 3;
public:
	//range is 0-1 where 0 is fully deflected and 1 is no deflection
	bool check_if_hit(std::int64_t enemy_accuracy)
	{
		//things beneath a certain accuracy raiting are outright ignored
		//things above that same raiting then take "hits" to the evasion raiting? that then degrades until it goes negative
		//in which case the damage passes through.
		auto remaining_accuracy = enemy_accuracy - accuracy_ignore_raiting;

		remaining_evasion_raiting -= std::clamp(remaining_accuracy, 0ll, std::numeric_limits<std::int64_t>::max()) * enemy_accuracy_effectiveness;

		bool hit_status = remaining_evasion_raiting < 0;
		if (hit_status)
			remaining_evasion_raiting += evasion_raiting;
		return hit_status;
	}
	bool check_if_crit(std::int64_t enemy_accuracy)
	{
		remaining_crit_evasion_raiting -= enemy_accuracy * enemy_crit_effectiveness;
		bool crit_status = remaining_crit_evasion_raiting < 0;
		if (crit_status)
			remaining_crit_evasion_raiting += evasion_raiting;
		return crit_status;
	}
	static ImVec4 display_color()
	{
		return rgb_color_to_imvec4({ 150, 192, 98 });
	}
};

struct player_armor
{
	evasion_stats evasion = {};
	armor_stats armor = {};

	resistance<fire_tag> fire_res = {};
	resistance<cold_tag> cold_res = {};
	resistance<lightning_tag> lightning_res = {};
	resistance<wind_tag> wind_res = {};
	resistance<abyss_tag> abyssal_res = {};

	void gui_display()
	{
		if (evasion.evasion_raiting > 0)
		{
			ImGui::Text("Adds: ");
			ImGui::SameLine();
			ImGui::TextColored(evasion_stats::display_color(), std::to_string(evasion.evasion_raiting).c_str());
			ImGui::SameLine();
			ImGui::Text(" Evasion raiting");
		}
		if (armor.armor_raiting > 0)
		{

			ImGui::Text("Adds: ");
			ImGui::SameLine();
			ImGui::TextColored(armor_stats::display_color(), std::to_string(armor.armor_raiting).c_str());
			ImGui::SameLine();
			ImGui::Text(" Armor raiting");
		}
		auto res_display_lam = [&](auto resistance)
		{
			if (resistance.res_value == 0.f)
				return;
			using res_type = decltype(resistance);
			using tag_type = typename res_type::tag_type;
			std::string res_name = spawnx::type_name_string<tag_type>();
			res_name.resize(res_name.size() - 4); // drop the _tag part.
			auto& first_char = res_name[0];
			first_char = first_char > 'A' && first_char < 'Z' ? first_char : first_char + ('A' - 'a');
			
			res_name += " resistance";

			ImGui::Text("Adds: ");
			ImGui::SameLine();
			ImGui::TextColored(tag_type::display_color(), (std::to_string((int)(resistance.res_value*100))+ "%%").c_str());
			ImGui::SameLine();
			ImGui::Text(res_name.c_str());
		};

		res_display_lam(fire_res);
		res_display_lam(cold_res);
		res_display_lam(lightning_res);
		res_display_lam(wind_res);
		res_display_lam(abyssal_res);
	}
};
struct some_item
{
	glm::ivec2 tile_size;
	spawnx::entity item;
};
struct inventory
{
	int x_dim, y_dim;
	std::vector<int> tiles_id;
	std::unordered_map<glm::ivec2, some_item> items;
	constexpr static int unassigned_id = -1;
	enum class insert_status
	{
		failed,
		success,
		overlap //not used
	};
	inventory(int x_dim_, int y_dim_): x_dim(x_dim_), y_dim(y_dim_)
	{
		tiles_id.resize(x_dim*y_dim,unassigned_id);
	}
	std::variant<some_item, insert_status> place(glm::ivec2 insert_pos, some_item&& item_to_insert)
	{
		glm::ivec2 item_dims = item_to_insert.tile_size;
		bool invalid_pos = insert_pos.x < 0 || insert_pos.y < 0;
		bool out_of_bounds_access = insert_pos.x + item_dims.x > x_dim || insert_pos.y + item_dims.y > y_dim;

		if (invalid_pos || out_of_bounds_access)
			return insert_status::failed;

		int single_overlapped_tile_id = unassigned_id;
		glm::ivec2 first_overlap_pos = {};
		bool can_place = true;
		for (int y = 0; y < item_dims.y; ++y)
		{
			for (int x = 0; x < item_dims.x; ++x)
			{
				auto lookup = insert_pos + glm::ivec2{ x,y };
				int& candidate_tile = get_tile(lookup);
				if (candidate_tile != unassigned_id)
				{
					if (single_overlapped_tile_id == unassigned_id)
					{
						first_overlap_pos = lookup;
						single_overlapped_tile_id = candidate_tile;
					}
					else if (single_overlapped_tile_id != candidate_tile)
					{
						can_place = false;
						break;
					}
				}
			}
		}
		if (can_place)
		{
			auto assign_item = [&]()
			{
				assign_ids(insert_pos, insert_pos + item_dims, lookup_id(insert_pos));
				items.emplace(insert_pos, item_to_insert);
			};
			if (single_overlapped_tile_id != unassigned_id)
			{
				glm::ivec2 items_origin = item_origin_slot(first_overlap_pos);
				auto overlapped_item = std::move(items.extract(items_origin).mapped());
				assign_ids(items_origin, items_origin + overlapped_item.tile_size, unassigned_id);

				assign_item();

				return overlapped_item;
			}
			assign_item();
			return insert_status::success;
		}
		return insert_status::failed;
	}
	constexpr int lookup_id(glm::ivec2 lookup) const
	{
		return lookup.x + lookup.y*x_dim;
	}
	constexpr glm::ivec2 coord_from_id(int id) const
	{
		return { id%x_dim, id / x_dim };
	}
	int& get_tile(glm::ivec2 lookup)
	{
		return tiles_id[lookup.x + lookup.y*x_dim];
	}
	const int& get_tile(glm::ivec2 lookup) const
	{
		return tiles_id[lookup.x + lookup.y*x_dim];
	}
	void assign_ids(glm::ivec2 start, glm::ivec2 end, int id)
	{
		for (int y = start.y; y != end.y; ++y)
		{
			for (int x = start.x; x != end.x; ++x)
			{
				get_tile({ x,y }) = id;
			}
		}
	}
	glm::ivec2 item_origin_slot(glm::ivec2 lookup) const
	{
		return coord_from_id(get_tile(lookup));
	}
	void imgui_display(spawnx::world& world)
	{
		auto colors = fill_colors();
		auto inventory_display_piece_lam = [](ImVec4 color, const std::string& tooltip, auto displable_item)
		{
			ImGui::TextColored(color, "#");
			ImGui::SameLine();
			if (tooltip.empty())
				return;
			if (ImGui::IsItemHovered())
			{
				ImGui::BeginTooltip();
				ImGui::PushTextWrapPos(ImGui::GetFontSize()*35.f);
				
				ImGui::Text(tooltip.c_str());
				if constexpr(std::is_same<decltype(displable_item),player_armor>::value)
					displable_item.gui_display();
				ImGui::PopTextWrapPos();
				ImGui::EndTooltip();
			}
		};
		ImGui::Begin("inventory window");
		for (int y = 0; y < y_dim; ++y)
		{
			for (int x = 0; x < x_dim; ++x)
			{
				int tile_id = get_tile({ x,y });
				if (tile_id == unassigned_id)
				{
					inventory_display_piece_lam({ 1.f,1.f,1.f,1.f },"",nullptr);
					continue;
				}
				auto color = colors[tile_id%colors.size()];
				auto item_coord = coord_from_id(tile_id);
				auto item_dims = items[item_coord].tile_size;
				std::string tooltip = std::string("x:") + std::to_string(item_dims.x) + " y:" + std::to_string(item_dims.y);
				if (auto armor_stats_handle = world.get_component_from_entity<player_armor>(items[item_coord].item))
				{
					auto armor_stats = *armor_stats_handle;
					inventory_display_piece_lam(color, tooltip, armor_stats);
				}
				else
				{
					inventory_display_piece_lam(color, tooltip, nullptr);
				}

				
			}
			ImGui::NewLine();
		}
		static std::array<int, 4> item_insert_data{-1,-1,-1,-1};
		static std::string status_str;
		ImGui::InputInt4("item coords and dims", item_insert_data.data());
		if (ImGui::Button("push item"))
		{
			
			auto ret = place(glm::ivec2{ item_insert_data[0],item_insert_data[1] }, some_item{ {item_insert_data[2],item_insert_data[3]},{} });
			if (auto ptr = std::get_if<some_item>(&ret))
			{
				status_str = "swapped an item";
			}
			else if (auto status = std::get_if<insert_status>(&ret))
			{
				if (*status == insert_status::success)
				{
					status_str = "placed an item";
				}
				else
				{
					status_str = "too many items to place or out of bounds";
				}
			}
			
		}
		ImGui::Text(status_str.c_str());
		ImGui::End();
	}
	std::vector<ImVec4> fill_colors() const
	{
		std::vector<ImVec4> colors;
		colors.reserve(8);
		int range = 3;
		float divisor = (float)range;
		for(int x = 1; x < range; ++x)
			for (int y = 1; y < range; ++y)
				for(int z = 1; z< range; ++z)
					colors.push_back({ x/ divisor,y/ divisor,z/ divisor,1.f });
		return colors;
	}
};