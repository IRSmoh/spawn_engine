#pragma once
#include <glm\glm.hpp>
struct QEF_data
{
	//partial 3x3 matrix since we are dealing with symmetric matrix's for the most part
	float ata_00,	ata_01, ata_02;
	float			ata_11, ata_12;
	float					ata_22;

	glm::vec3 atb;
	float btb;

	float mass_point_x, mass_point_y, mass_point_z;
	glm::vec3 mass_point;
	int num_points;

	void add(const QEF_data& right)
	{
		ata_00 += right.ata_00;
		ata_01 += right.ata_01;
		ata_02 += right.ata_02;
		ata_11 += right.ata_11;
		ata_12 += right.ata_12;
		ata_22 += right.ata_22;

		atb += right.atb;

		btb += right.btb;
		mass_point += right.mass_point;
		num_points += right.num_points;
	}
};
struct QEF_solver
{
private:
	QEF_data data;
	glm::mat3 ata;
	glm::vec3 atb, mass_point, x;

	//represents the amount of error in this qef 
	//used for determining if there would be too much loss of geometry features if this node were to be collapsed
	//only valid if has_solution is set.
	float result;
	bool has_solution;
public:
	QEF_solver() : data({}), ata(), atb(), mass_point(), x(), result(0), has_solution(false) {}

	const glm::vec3 get_mass_point() const { return mass_point; }

	void add(const glm::vec3& pos, const glm::vec3& normal)
	{
		has_solution = false;
		data.ata_00 += normal.x*normal.x;
		data.ata_01 += normal.x*normal.y;
		data.ata_02 += normal.x*normal.z;

		data.ata_11 += normal.y*normal.y;
		data.ata_12 += normal.y*normal.z;

		data.ata_22 += normal.z*normal.z;

		float dot = glm::dot(normal, pos);

		data.atb += dot*normal;

		data.btb += dot*dot;

		data.mass_point += pos;

		++data.num_points;
	}
	void add(const QEF_data& right)
	{
		has_solution = false;
		data.add(right);
	}
	 const QEF_data& get_data()
	{
		return data;
	}
	float get_error()
	{
		if (!has_solution)
			throw std::runtime_error("Must solve QEF before acquiring error.");	//don't try to reach this
		return get_error(x);
	}
	float get_error(const glm::vec3& pos)
	{
		if (!has_solution)
		{
			set_ata();
			set_atb();
		}

		glm::vec3 atax = ata*pos;
		return glm::dot(pos, atax) - 2 * glm::dot(pos, atb) + data.btb;
	}
	float solve(glm::vec3& outx, float svd_tol, int svp_sweeps, float pinv_tol)
	{
		if (has_solution)
		{
			outx = x;
			return result;
		}
		if (data.num_points == 0)
			throw std::runtime_error("Too few data points for QEF.");
		mass_point = data.mass_point*(1.f/data.num_points);

		set_ata();
		set_atb();

		glm::vec3 tmpv = ata*mass_point;
		atb = atb - tmpv;
		x = {};

		float result = solve_symmetric(ata, atb, x, svd_tol, svp_sweeps, pinv_tol);
		set_atb();
		x += mass_point;
		outx = x;
		has_solution = true;
		return result;
	}
private:
	void set_ata()
	{
		//make a symmetric matrix
		ata = make_symmetric(data.ata_00,	data.ata_01,	data.ata_02,
											data.ata_11,	data.ata_12,
															data.ata_22);
	}
	void set_atb()
	{
		atb = data.atb;
	}
	static glm::mat3x3 make_symmetric(float m00,	float m01,	float m02,
													float m11,	float m12,
																float m22)
	{
		return glm::mat3x3(	m00, m01, m02,
							m01, m11, m12,
							m02, m12, m22);
	}
	static glm::mat3x3 make_symmetric(const glm::mat3x3& right)
	{
		return make_symmetric(right[0][0],	right[0][1],	right[0][2],
											right[1][1],	right[1][2],
															right[2][2]);
	}
	static void pseudo_inverse(glm::mat3 &out, const glm::mat3 &d, const glm::mat3 &v,
		const float tol)
	{
		auto pinv = [](float x, float tol)
		{
			return (fabs(x) < tol || fabs(1 / x) < tol) ? 0 : (1 / x);
		};
		const float d0 = pinv(d[0][0], tol), d1 = pinv(d[1][1], tol), d2 = pinv(d[2][2],
			tol);
		out = {	v[0][0] * d0 * v[0][0]		+ v[0][1] * d1 * v[0][1]		+ v[0][2] * d2 * v[0][2],
				v[0][0] * d0 * v[1][0]		+ v[0][1] * d1 * v[1][1]		+ v[0][2] * d2 * v[1][2],
				v[0][0] * d0 * v[2][0]		+ v[0][1] * d1 * v[2][1]		+ v[0][2] * d2 * v[2][2],
				v[1][0] * d0 * v[0][0]		+ v[1][1] * d1 * v[0][1]		+ v[1][2] * d2 * v[0][2],
				v[1][0] * d0 * v[1][0]		+ v[1][1] * d1 * v[1][1]		+ v[1][2] * d2 * v[1][2],
				v[1][0] * d0 * v[2][0]		+ v[1][1] * d1 * v[2][1]		+ v[1][2] * d2 * v[2][2],
				v[2][0] * d0 * v[0][0]		+ v[2][1] * d1 * v[0][1]		+ v[2][2] * d2 * v[0][2],
				v[2][0] * d0 * v[1][0]		+ v[2][1] * d1 * v[1][1]		+ v[2][2] * d2 * v[1][2],
				v[2][0] * d0 * v[2][0]		+ v[2][1] * d1 * v[2][1]		+ v[2][2] * d2 * v[2][2] };
	}
	static float calc_error(const glm::mat3& A, const glm::vec3& x, const glm::vec3& b)
	{
		glm::vec3 vtmp;
		vtmp = A * x;
		vtmp = b - vtmp;
		return glm::dot(vtmp, vtmp);
	}
	static float solve_symmetric(const glm::mat3x3& A, const glm::vec3& b, glm::vec3& x,
		float svd_tol, int svd_sweeps, float pinv_tol)
	{
		glm::mat3x3 mtmp, pinv, V;
		glm::mat3x3 VTAV;

		get_symmetric_svd(A, VTAV, V, svd_tol, svd_sweeps);
		pseudo_inverse(pinv, VTAV, V, pinv_tol);
		x = pinv * b;
		return calc_error(A, x, b);
	}
	static void get_symmetric_svd(const glm::mat3x3& a, glm::mat3x3& vtav_out, glm::mat3x3& v_out,
		float tol,
		int max_sweeps)
	{
		vtav_out = make_symmetric(a);
		v_out = glm::mat3();

		float delta = tol* fnorm(vtav_out);
		delta *= delta;

		for (int i = 0; i < max_sweeps && off(vtav_out)>delta; ++i)
		{
			rotate01(vtav_out, v_out);
			rotate02(vtav_out, v_out);
			rotate12(vtav_out, v_out);
		}
	}
	static float off(const glm::mat3x3& a)
	{
		auto square = [](auto num) {return num*num; };
		return square(a[0][1]) + square(a[0][2]) + square(a[1][0]) + square(a[1][2]) + square(a[2][0]) + square(a[2][1]);
	}
	static float fnorm(const glm::mat3x3& a)
	{
		float result = 0;
		for (int i = 0; i < 3; ++i)
		{
			for (int j = 0; j < 3; ++j)
			{
				result += a[i][j] * a[i][j];
			}
		}
		return sqrt(result);
	}
	static void rotate01(glm::mat3x3& vtav, glm::mat3x3& v)
	{
		if (vtav[0][1] == 0)
			return;
		float c, s;
		rot01(vtav, c, s);
		rot01_post(v, c, s);
	}
	static void rotate02(glm::mat3x3& vtav, glm::mat3x3& v)
	{
		if (vtav[0][2] == 0)
			return;
		float c, s;
		rot02(vtav, c, s);
		rot02_post(v, c, s);
	}
	static void rotate12(glm::mat3x3& vtav, glm::mat3x3& v)
	{
		if (vtav[1][2] == 0)
			return;
		float c, s;
		rot12(vtav, c, s);
		rot12_post(v, c, s);
	}
	static void rot01(glm::mat3x3& m, float& c, float& s)
	{
		calc_symmetric_givens_coefficiencts(m[0][0], m[0][1], m[1][1], c, s);
		float cc = c*c;
		float ss = s*s;
		float mix = 2 * c*s*m[0][1];
		m = make_symmetric(	cc*m[0][0] - mix + ss*m[1][1],		0,							c*m[0][2] - s*m[1][2],
							ss*m[0][0] + mix + cc*m[1][1],		s*m[0][2] + c*m[1][2],		m[2][2]);
	}
	static void rot02(glm::mat3x3& m, float &c, float &s)
	{
		calc_symmetric_givens_coefficiencts(m[0][0], m[0][2], m[2][2], c, s);
		float cc = c*c;
		float ss = s*s;
		float mix = 2 * c*s*m[0][2];
		m = make_symmetric(	cc*m[0][0] - mix + ss*m[2][2],		c*m[0][1] - s*m[1][2],		0,
							m[1][1],							s*m[0][1] + c*m[1][2],		ss*m[0][0] + mix + cc*m[2][2]);
	}
	static void rot12(glm::mat3x3& m, float& c, float& s)
	{
		calc_symmetric_givens_coefficiencts(m[1][1], m[1][2], m[2][2], c, s);
		float cc = c*c;
		float ss = s*s;
		float mix = 2 * c*s*m[1][2];
		m = make_symmetric(	m[0][0],							c*m[0][1] - s *m[0][2],		s*m[0][1] + c*m[0][2],
							cc*m[1][1] - mix + ss*m[2][2],		0,							ss*m[1][1] + mix + cc*m[2][2]);
	}
	static void rot01_post(glm::mat3x3& m, float c, float s)
	{
		m = {	c*m[0][0] - s*m[0][1],		s*m[0][0] + c*m[0][1],		m[0][2],
				c*m[1][0] - s*m[1][1],		s*m[1][0] + c*m[1][1],		m[1][2],
				c*m[2][0] - s*m[2][1],		s*m[2][0] + c*m[2][1],		m[2][2] };
	}
	static void rot02_post(glm::mat3x3& m, float c, float s)
	{
		m = {	c*m[0][0] - s*m[0][2],		m[0][1],		s*m[0][0] + c*m[0][2],
				c*m[1][0] - s*m[1][2],		m[1][1],		s*m[1][0] + c*m[1][2],
				c*m[2][0] - s*m[2][2],		m[2][1],		s*m[2][0] + c*m[2][2] };
	}
	static void rot12_post(glm::mat3x3& m, float c, float s)
	{
		m = {	m[0][0],	c*m[0][1] - s*m[0][2],		s*m[0][1] + c*m[0][2],
				m[1][0],	c*m[1][1] - s*m[1][2],		s*m[1][1] + c*m[1][2],
				m[2][0],	c*m[2][1] - s*m[2][2],		s*m[2][1] + c*m[2][2] };
	}
	static void calc_symmetric_givens_coefficiencts(float a_pp, float a_pq, float a_qq, float& c, float& s)
	{
		if (a_pq == 0)
		{
			c = 1;
			s = 0;
			return;
		}
		float tau = (a_qq - a_pp) / (2 * a_pq);
		float stt = sqrt(1.f + tau*tau);
		float tan = 1.f / ((tau >= 0) ? (tau + stt) : (tau - stt));

		c = 1.f / sqrt(1.f + tan*tan);
		s = tan*c;
	}
};