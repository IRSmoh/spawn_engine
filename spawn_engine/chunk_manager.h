#pragma once
#include <memory>
#include <glm\glm.hpp>
#include <vulkan_renderer\vulkan_renderer.h>
#include <tbb\concurrent_vector.h>

#include "thread_pool.h"
#include "height_map.h"
#include "height_map_manager.h"
#include "octree.h"
#include "density_func.h"
namespace spawn_engine
{
	struct chunk_manager
	{
		struct neighbor_encoding
		{
			enum encoding
			{
				empty = 0,
				equal = 1,
				greater_1 = 2,
				greater_2 = 3,
				smaller = 4
			};
			constexpr static int encoding_size_in_bits = 2;

			int encoding_bits;
			neighbor_encoding() : encoding_bits(0) {}
			void assign(int index, int left_chunk_size, int right_chunk_size)
			{
				int scale = right_chunk_size - left_chunk_size;
				encoding enc;
				if (scale < 0)
					enc = smaller;
				else if (scale == 0)
					enc = equal;
				else if (scale == 1)
					enc = greater_1;
				else
					enc = greater_2;
				if (index > 7)
					throw std::runtime_error("Attempting to assign an invalid index for neighbor_encoding");

				int other_bits = encoding_bits & ~(3 << index * 3);
				encoding_bits = other_bits | (enc << index * 3);
			}
			bool operator==(const neighbor_encoding& right) const
			{
				return encoding_bits == right.encoding_bits;
			}
			bool operator!=(const neighbor_encoding& right) const
			{
				return !(*this == right);
			}
		};
		struct tree_and_pos
		{
			std::shared_ptr<octree_root> root;
			std::shared_ptr<std::shared_mutex> shared_mutex;
			//track how this is managed we're destroying data that's expected to be used and we need to sync things correctly.
			std::shared_ptr<spawnx_vulkan::model> chunk_gpu_model;
			std::shared_ptr<neighbor_encoding> neighbor_encode;
			glm::ivec3 relative_pos;
			tree_and_pos() {}
			tree_and_pos(octree_root&& root_, glm::ivec3 relative_pos_) :
				root(std::make_shared<octree_root>(std::move(root_))),
				shared_mutex(std::make_shared<std::shared_mutex>()),
				chunk_gpu_model(std::make_shared<spawnx_vulkan::model>()),
				neighbor_encode(std::make_shared<neighbor_encoding>()),
				relative_pos(relative_pos_) {}
		};
		struct mesh_data
		{
			std::shared_ptr<spawnx_vulkan::default_cpu_mesh> mesh;
			tree_and_pos chunk_data;
			mesh_data() {}
			mesh_data(std::shared_ptr<spawnx_vulkan::default_cpu_mesh> mesh_, tree_and_pos chunk_data_) : mesh(mesh_), chunk_data(chunk_data_) {}
		};
		struct tree_and_dependents
		{
			tree_and_pos tree;
			tbb::concurrent_vector<shared_call_on_death> dependents;
			std::weak_ptr<tbb::concurrent_vector<shared_call_on_death>> dependents_ptr;	//if dependents is zero; this will point to the thread-local dependents OR be expired.
			tree_and_dependents(const tree_and_pos& tree_) : tree(tree_) {}
		};
		constexpr static int chunk_size = octree_root::default_chunk_size;
		constexpr static int base_chunk_scale = octree_root::default_max_depth;
		//constexpr static int chunk_scale = octree_root::default_min_node_size;

		height_map_manager hmap_manager;

		thread_pool chunk_thread_pool;

		std::vector<std::unordered_map<glm::ivec3, tree_and_pos>> octree_chunks_lod;
		std::vector<std::unordered_map<glm::ivec3, tree_and_dependents>> pending_octrees;
		std::unordered_map<glm::ivec4, std::shared_ptr<mesh_data>> octrees_waiting_finalizing;

		using built_meshes_vec_type = tbb::concurrent_vector<std::shared_ptr<mesh_data>>;
		mutexed_struct<built_meshes_vec_type, std::shared_mutex> built_meshes;

		std::unordered_map<glm::ivec4, int> required_chunks;
		bool moved_this_update;
		glm::ivec3 old_rooted_pos;
		chunk_manager() : moved_this_update(false), old_rooted_pos(glm::ivec3{ 1,1,1 }*std::numeric_limits<int>::max())
		{
			chunk_thread_pool.init();
		}
		const tree_and_pos* get_chunk(const std::pair<glm::ivec3, int>& candidate_pos) const
		{
			if (candidate_pos.second >= octree_chunks_lod.size())
				return nullptr;
			auto itr = octree_chunks_lod[candidate_pos.second].find(candidate_pos.first);
			if (itr == octree_chunks_lod[candidate_pos.second].end())
				return nullptr;
			return &(itr->second);
		}
		const tree_and_dependents* get_pending_chunk(const std::pair<glm::ivec3, int>& candidate_pos) const
		{
			if (candidate_pos.second >= pending_octrees.size())
				return nullptr;
			auto itr = pending_octrees[candidate_pos.second].find(candidate_pos.first);
			if (itr == pending_octrees[candidate_pos.second].end())
				return nullptr;
			return &itr->second;
		}
		const mesh_data* get_waiting_chunk(const std::pair<glm::ivec3, int>& candidate_pos) const
		{
			auto itr = octrees_waiting_finalizing.find(glm::ivec4{ candidate_pos.first, candidate_pos.second });
			if (itr == octrees_waiting_finalizing.end())
				return nullptr;
			return &*itr->second;
		}
		tree_and_pos* get_chunk(const std::pair<glm::ivec3, int>& candidate_pos)
		{
			if (candidate_pos.second >= octree_chunks_lod.size())
				return nullptr;
			auto itr = octree_chunks_lod[candidate_pos.second].find(candidate_pos.first);
			if (itr == octree_chunks_lod[candidate_pos.second].end())
				return nullptr;
			return &(itr->second);
		}
		bool remove_chunk(const std::pair<glm::ivec3, int>& candidate_pos)
		{
			if (candidate_pos.second >= octree_chunks_lod.size())
				return false;
			auto itr = octree_chunks_lod[candidate_pos.second].find(candidate_pos.first);
			if (itr == octree_chunks_lod[candidate_pos.second].end())
				return false;
			octree_chunks_lod[candidate_pos.second].erase(itr);
			return true;
		}

		//predicate must return a bool if it finds a match and take a const glm::ivec3& <lookup_pos>, int <chunk_scale>
		//loop_start takes a glm::ivec3 of inds indicating where it is in the current looping (e.g. x,y,z) and expects a bool back
		//if the bool is true it skips that loop.
		//end_of_small_loop is called after the smaller lookups are performed.
		//forward_lookup is the default way. e.g. we're going +x,+y,+z.
		//if its set to false we're doing reverse lookup e.g. -x,-y,-z
		template<bool forward_lookup, typename candidate_lookup_type, typename loop_start_type, typename end_of_small_loop_type>
		auto get_lookup_lambda(const candidate_lookup_type& candidate_lookup_lam, const loop_start_type& loop_start_lam, const end_of_small_loop_type& end_of_small_loop_lam)
		{
			glm::ivec3 outer_min;
			glm::ivec3 outer_max;
			if constexpr(forward_lookup)
			{
				outer_min = { 0,0,0 };
				outer_max = { 2,2,2 };
			}
			else
			{
				outer_min = { -1,-1,-1 };
				outer_max = { 1,1,1 };
			}
			auto lookup_lambda = [&candidate_lookup_lam, &loop_start_lam, &end_of_small_loop_lam, outer_min, outer_max](const glm::ivec4& chunk)
			{
				int chunk_scale = chunk.w;
				glm::ivec3 chunk_pos = chunk;
				for (int z = outer_min.z; z < outer_max.z; ++z)
					for (int y = outer_min.y; y < outer_max.y; ++y)
						for (int x = outer_min.x; x < outer_max.x; ++x)
						{
							if (loop_start_lam({ x,y,z }))
								continue;

							glm::ivec3 lookup = chunk_pos + (glm::ivec3{ x,y,z }*(chunk_size << chunk_scale));
							//default lookup
							if (candidate_lookup_lam(lookup, chunk_scale))
								continue;

							//larger lookup +1 & +2
							auto larger_lookup_lam = [&]()
							{
								for (int i = 1; i <= 2; ++i)
								{
									int base_size = chunk_scale + base_chunk_scale;
									int mask = ~((1 << (base_size + i)) - 1);
									glm::ivec3 larger_lookup = lookup & mask;
									if (candidate_lookup_lam(larger_lookup, chunk_scale + i))
										return true;
								}
								return false;
							};
							if (larger_lookup_lam())
								continue;
							//smaller lookup -1
							if (chunk_scale == 0)
								continue;
							glm::ivec3 smaller_offsets = glm::ivec3{ x == 1, y == 1, z == 1 };
							glm::ivec3 limits = glm::ivec3{ 2,2,2 } -smaller_offsets;
							for (int zz = 0; zz < limits.z; ++zz)
								for (int yy = 0; yy < limits.y; ++yy)
									for (int xx = 0; xx < limits.x; ++xx)
									{
										glm::ivec3 smaller_offset;
										if constexpr(forward_lookup)
											smaller_offset = glm::ivec3{ xx,yy,zz };
										else
											smaller_offset = glm::ivec3{ xx,yy,zz }+smaller_offsets;

										glm::ivec3 smaller_lookup = lookup + (smaller_offset << (base_chunk_scale + chunk_scale - 1));
										candidate_lookup_lam(smaller_lookup, chunk_scale - 1);
									}
							end_of_small_loop_lam();
						}
			};
			return lookup_lambda;
		}
		neighbor_encoding get_chunk_neighbor_encoding(const glm::ivec3& chunk_root_pos, int chunk_scale)
		{
			int neighbor_index = 0;
			neighbor_encoding encoding;
			auto candidate_lambda = [&](const glm::ivec3& chunk_lookup, int chunk_scale_)
			{
				auto required_candidate = required_chunks.find({ chunk_lookup, chunk_scale_ });
				if (required_candidate != required_chunks.end())
				{
					encoding.assign(neighbor_index, chunk_scale, chunk_scale_);

					return true;
				}
				return false;
			};
			auto start_of_loop_lambda = [&](const glm::ivec3 inds)
			{
				neighbor_index = ((inds.z != 0) << 2) | ((inds.y != 0) << 1) | (inds.x != 0);
				if (inds == glm::ivec3{ 0,0,0 })
					return true;
				return false;
			};
			auto end_of_smaller_loop_lambda = [&]()
			{};
			auto lookup_lambda = get_lookup_lambda<true>(candidate_lambda, start_of_loop_lambda, end_of_smaller_loop_lambda);
			lookup_lambda({ chunk_root_pos,chunk_scale });
			return encoding;
		}
		std::vector<octree_root::seam_dependency> get_chunk_neighbors(const glm::ivec3& chunk_root_pos, int chunk_scale)
		{
			using seam_dependency = octree_root::seam_dependency;
			std::vector<seam_dependency> dependencies;
			seam_dependency dependency{};
			int neighbor_index = 0;
			neighbor_encoding encoding;
			auto candidate_lambda = [&](const glm::ivec3& chunk_lookup, int chunk_scale_)
			{
				auto required_candidate = required_chunks.find({ chunk_lookup, chunk_scale_ });
				if (required_candidate != required_chunks.end())
				{
					if (chunk_scale_ < octree_chunks_lod.size())
					{
						auto chunk_candidate = octree_chunks_lod[chunk_scale_].find(chunk_lookup);
						if (chunk_candidate != octree_chunks_lod[chunk_scale_].end() && !chunk_candidate->second.root->is_dirty)
						{
							dependency.roots.push_back(chunk_candidate->second.root);
							if (!(chunk_scale_ < chunk_scale))
							{
								dependencies.push_back(dependency);
								encoding.assign(neighbor_index, chunk_scale, chunk_scale_);
							}
							return true;
						}
					}
					if (chunk_scale_ < pending_octrees.size())
					{
						auto chunk_candidate = pending_octrees[chunk_scale_].find(chunk_lookup);
						if (chunk_candidate != pending_octrees[chunk_scale_].end())
						{
							dependency.roots.push_back(chunk_candidate->second.tree.root);
							if (!(chunk_scale_ < chunk_scale))
							{
								dependencies.push_back(dependency);
								encoding.assign(neighbor_index, chunk_scale, chunk_scale_);
							}
							return true;
						}
					}
					//throw std::runtime_error("Error: Attempting to find a chunk that exists in chunk_manager::required_chunks but does not exist in ::octree_chunks_lod nor ::pending_octres.");
				}
				return false;
			};
			auto start_of_loop_lambda = [&](const glm::ivec3 inds)
			{
				dependency = {};
				neighbor_index = ((inds.z != 0) << 2) | ((inds.y != 0) << 1) | (inds.x != 0);
				if (inds == glm::ivec3{ 0,0,0 })
					return true;
				return false;
			};
			auto end_of_smaller_loop_lambda = [&]()
			{
				if (!dependency.roots.empty())
				{
					encoding.assign(neighbor_index, chunk_scale, chunk_scale - 1);
				}
				dependencies.push_back(dependency);
			};
			auto lookup_lambda = get_lookup_lambda<true>(candidate_lambda, start_of_loop_lambda, end_of_smaller_loop_lambda);
			lookup_lambda({ chunk_root_pos,chunk_scale });
			//if (encoding != get_chunk_neighbor_encoding(chunk_root_pos, chunk_scale))
			//	throw std::runtime_error("fuggggg");
			return dependencies;
		}

		template<typename candidate_lookup_type>
		void get_tree_list(const candidate_lookup_type& candidate_lookup_lam)
		{
			auto lookup_lambda = get_lookup_lambda<true>(candidate_lookup_lam, []() {}, []() {});
			for (auto& chunk : required_chunks)
			{
				lookup_lambda(chunk.first);
			}
		}
		std::vector<tree_and_dependents*> get_dependents(const glm::ivec3& chunk_root_pos, int chunk_scale)
		{
			std::vector<tree_and_dependents*> dependencies;
			auto candidate_lambda = [&](const glm::ivec3& chunk_lookup, int chunk_scale)
			{
				auto required_candidate = required_chunks.find({ chunk_lookup,chunk_scale });
				if (required_candidate != required_chunks.end())
				{
					if (chunk_scale < pending_octrees.size())
					{
						auto pending_candidate = pending_octrees[chunk_scale].find(chunk_lookup);
						if (pending_candidate != pending_octrees[chunk_scale].end())
						{
							dependencies.push_back(&pending_candidate->second);
							return true;
						}
					}
				}
				return false;
			};
			auto start_loop_lambda = [](const glm::ivec3&) {return false; };
			auto end_loop_lambda = []() {};
			auto lookup_lambda = get_lookup_lambda<true>(candidate_lambda, start_loop_lambda, end_loop_lambda);
			lookup_lambda({ chunk_root_pos,chunk_scale });
			return dependencies;
		}
		template<typename predicate_type>
		void dirty_depending_chunks(const glm::ivec4& chunk_pos, predicate_type& pred)
		{
			glm::ivec3 chunk_root_pos = glm::ivec3(chunk_pos);
			int chunk_scale = chunk_pos.w;
			if (chunk_scale >= octree_chunks_lod.size())
				return;

			auto candidate_lambda = [&](const glm::ivec3& chunk_lookup, int chunk_scale)
			{
				auto required_candidate = required_chunks.find({ chunk_lookup, chunk_scale });
				if (required_candidate != required_chunks.end())
				{
					if (chunk_scale < octree_chunks_lod.size())
					{
						auto candidate_chunk = octree_chunks_lod[chunk_scale].find(chunk_lookup);
						if (candidate_chunk != octree_chunks_lod[chunk_scale].end())
						{
							if (!candidate_chunk->second.root->is_dirty)
							{
								pred(candidate_chunk->second, chunk_pos);
								candidate_chunk->second.root->is_dirty = true;
							}
							return true;
						}
					}
				}
				return false;
			};
			auto loop_start_lam = [](const glm::ivec3&) {return false; };
			auto end_of_loop_lam = []() {};
			auto lookup_lambda = get_lookup_lambda<false>(candidate_lambda, loop_start_lam, end_of_loop_lam);
			lookup_lambda(chunk_pos);
			return;
		}
		int determine_max_lod(int view_distance)
		{
			view_distance >>= base_chunk_scale;
			int power = bit_scan(view_distance);
			return power;
		};
		glm::ivec3 world_pos_grid_aligned(const glm::ivec3& world_pos, int chunk_power)
		{
			int shift_amount = base_chunk_scale + chunk_power;
			return world_pos & ~((1 << (shift_amount)) - 1);
		}
		std::vector<std::pair<glm::ivec3, int>> get_chunks_to_load(glm::ivec3 world_pos, int view_distance)
		{

			int shift_amount;
			glm::ivec3 mask, last_mask;// = (world_pos & (1 << base_chunk_scale)) >> base_chunk_scale;
			bool is_offset;// = mask.x != 0 || mask.y != 0 || mask.z != 0;
			bool last_was_offset = false;
			int max_chunk_lod = determine_max_lod(view_distance);
			std::vector<std::pair<glm::ivec3, int>> coord_and_power;
			glm::ivec3 limits;// = glm::ivec3{ 2,2,2 } +mask;
			for (int chunk_power = 0; chunk_power <= max_chunk_lod; ++chunk_power)
			{
				shift_amount = base_chunk_scale + chunk_power;
				world_pos = world_pos_grid_aligned(world_pos, chunk_power);
				mask = (world_pos & (1 << shift_amount)) >> shift_amount;
				limits = glm::ivec3{ 2,2,2 } +mask;
				is_offset = limits.x != 2 || limits.y != 2 || limits.z != 2;

				for (int z = -limits.z; z < limits.z; ++z)
					for (int y = -limits.y; y < limits.y; ++y)
						for (int x = -limits.x; x < limits.x; ++x)
						{
							if (chunk_power != 0 && (z == 0 || z == -1) && (y == 0 || y == -1) && (x == 0 || x == -1))
								continue;
							if (last_was_offset)
							{
								//currently if both this set is offset and the last set was offset we wind up dropping the highest chunks in mask direction incorrectly
								//e.g.... if both 0 and 1 are +x offset the 1's +x max is dropped.

								//this dark magic is for removing the last shell of base 2 size chunks
								//since we could stradle a larger chunk boundary by being on a half mark for a larger chunk... we can instead choose to not fully use the chunk boundary
								//instead have extra smaller chunks and keep everything to a rigid grid (prevents us from having to make new large octrees any time the player moves)
								//basically doing this should allow us to have fast updates instead of moving chunk bounds causing us to rebuild the entire world over by 1 chunk...
								glm::ivec3 conditions{ x < 0 ? x + 1 : last_mask.x && x < 2 ? 0 : x, y < 0 ? y + 1 : last_mask.y && y < 2 ? 0 : y, z < 0 ? z + 1 : last_mask.z && z < 2 ? 0 : z };
								if (conditions.x == 0 && conditions.y == 0 && conditions.z == 0)
									continue;
							}
							glm::ivec3 chunk_pos = glm::ivec3{ x,y,z } << (base_chunk_scale + chunk_power);
							coord_and_power.push_back({ world_pos + chunk_pos,chunk_power });
						}
				last_was_offset = is_offset;
				last_mask = mask;
			}
			return coord_and_power;
		}
		void generate_chunks()
		{
			auto chunk_exists_lam = [&](const std::pair<glm::ivec3, int>& lookup)
			{
				return get_chunk(lookup) || get_pending_chunk(lookup) || get_waiting_chunk(lookup);
			};
			height_map_manager::height_map_gen_and_callables required_heightmaps;

			for (auto& creation_pos : required_chunks)
			{
				std::pair<glm::ivec3, int> lookup = { creation_pos.first,creation_pos.first.w };
				if (chunk_exists_lam(lookup))
					continue;
				glm::ivec3 hmap_pos = { creation_pos.first.x, creation_pos.first.z, creation_pos.first.w };
				required_heightmaps.insert_or_assign(hmap_pos, std::vector<shared_call_on_death>{});
			}

			//chunk creation logic (easy)
			//hmap manager lives for the entire duration of the program so its safe to take by reference.
			auto root_generator = [&hmap_manager = this->hmap_manager](const tree_and_pos& chunk_to_gen)
			{
				//tree_and_pos curr_root = { octree_root(creation_data.first, 1 << creation_data.second), creation_data.first };
				auto hmap_ptr = hmap_manager.get_map_from_world(chunk_to_gen.root->relative_pos, chunk_to_gen.root->base_node_size);
				if (!hmap_ptr)
					throw std::runtime_error("Heightmap is invalid/expired during expected use.");
				std::unique_lock<std::shared_mutex> lock{ *chunk_to_gen.shared_mutex };
				auto density_fnc = Density_Func(chunk_to_gen.root->relative_pos, (float)(1 << (octree_root::default_max_depth + chunk_to_gen.root->base_node_size)), hmap_ptr);
				chunk_to_gen.root->build_tree(density_fnc, *hmap_ptr, chunk_to_gen.root->relative_pos);
				//chunk_to_gen.root->simplify_octree(100.f);
				chunk_to_gen.root->remove_dead_nodes();
				chunk_to_gen.root->shrink_to_fit(); //vector growth is expensive and a lot of chunks just barely need a resize (they're wasting close to half or more...)
			};

			for (auto& creation_pos : required_chunks)
			{
				for (size_t i = pending_octrees.size(); i <= creation_pos.first.w; ++i)
					pending_octrees.push_back({});

				//check to make sure its: not made, nor is it already scheduled for creation.
				std::pair<glm::ivec3, int> lookup = { creation_pos.first,creation_pos.first.w };
				if (chunk_exists_lam(lookup))
					continue;
				pending_octrees[creation_pos.first.w].insert({ creation_pos.first,tree_and_dependents{ tree_and_pos{ octree_root(creation_pos.first, 1 << creation_pos.first.w),creation_pos.first } } });
			}

			//seam tree logic
			for (auto& chunk_group : pending_octrees)
			{
				for (auto& chunk : chunk_group)
				{
					if (!chunk.second.tree.root->is_dirty && chunk.second.tree.root->is_generated)
						continue;	//chunk is probably in the process of being worked on. maybe we can remove them? idfk.
					auto neighbors = get_chunk_neighbors(chunk.second.tree.relative_pos, chunk.second.tree.root->base_node_size);

					auto tmp = *chunk.second.tree.neighbor_encode;
					*chunk.second.tree.neighbor_encode = get_chunk_neighbor_encoding(chunk.second.tree.relative_pos, chunk.second.tree.root->base_node_size);
					if (tmp == *chunk.second.tree.neighbor_encode && tmp != neighbor_encoding{})
					{
						std::cout << "This shouldn't be happening.\n";
					}
					//this can race vs creation of chunks
					//this is a read only operation though; and both of the other operations are writes.
					//so find some proper way of minimizing the amount of cost we have.
					//at least... I think this is a race condition it seems likely; its not like tbb::concurrent_vector<> actually forces otherthings to be valid.
					auto seam_tree_generator = [this, chunk, neighbors]()
					{
						octree_root::cpu_mesh_data_type mesh_tmp;
						{
							std::shared_lock<std::shared_mutex> lock{ *chunk.second.tree.shared_mutex };
							mesh_tmp = chunk.second.tree.root->generate_seam_tree(neighbors);
						}
						built_meshes.get_shared()->push_back(std::make_shared<mesh_data>(mesh_data{ std::move(mesh_tmp), chunk.second.tree }));
					};
					auto seam_death_call = [seam_tree_generator, this]()
					{
						this->chunk_thread_pool.add_job(seam_tree_generator);
					};
					shared_call_on_death seam_fnc(seam_death_call);
					for (auto& dependency : get_dependents(chunk.second.tree.relative_pos, chunk.second.tree.root->base_node_size))
					{
						dependency->dependents.push_back(seam_fnc);
					}
				}
			}
			for (auto& chunk_group : pending_octrees)
			{
				for (auto& chunk : chunk_group)
				{
					if (!chunk.second.tree.root->is_dirty && chunk.second.tree.root->is_generated)
						continue;	//chunk is probably in the process of being worked on. maybe we can remove them? idfk.
									//this should be modified to check for chunks that have yet to be finished... where ever those are stored.
									//meshing logic (easy)
					auto dependents_ptr = std::make_shared<tbb::concurrent_vector<shared_call_on_death>>(std::move(chunk.second.dependents));
					//for some asinine reason when compiling in visual studio + clang tbb does not move from correctly; instead it just copies the elements.
					//so we have to clear them.
					//why....
					chunk.second.dependents.clear();
					chunk.second.dependents_ptr = dependents_ptr;
					auto mesh_generator = [tree = chunk.second.tree, dependents = std::move(dependents_ptr)]()
					{
						std::unique_lock<std::shared_mutex> lock{ *tree.shared_mutex };
						tree.root->generate_mesh();
					};
					auto mesh_gen_death_call = [mesh_generator, this]()
					{
						this->chunk_thread_pool.add_job(mesh_generator);
					};
					//the chunk must be built first
					if (!chunk.second.tree.root->is_generated)
					{
						chunk.second.tree.root->is_generated = true;
						auto chunk_gen_then_mesh = [root_generator, chunk, death_call = shared_call_on_death(mesh_gen_death_call)]()
						{
							root_generator(chunk.second.tree);
						};
						auto itr = required_heightmaps.find(glm::ivec3{ glm::ivec2{chunk.second.tree.relative_pos.x, chunk.second.tree.relative_pos.z},chunk.second.tree.root->base_node_size });
						if (itr != required_heightmaps.end())
						{
							auto callable_death = [chunk_gen_then_mesh, &chunk_thread_pool = chunk_thread_pool]()
							{
								chunk_thread_pool.add_job(chunk_gen_then_mesh);
							};
							//we're having to build the heightmap this update. *probably*
							itr->second.push_back(shared_call_on_death{ callable_death });
						}
						else
						{
							//this shouldn't be hittable.
							chunk_thread_pool.add_job(chunk_gen_then_mesh);
						}
					}
					//the chunk has already been built; we just need to regen its mesh.
					else if (chunk.second.tree.root->is_dirty)
					{
						chunk.second.tree.root->is_dirty = false;
						chunk_thread_pool.add_job(mesh_generator);
					}
				}
			}
			hmap_manager.generate_height_maps(required_heightmaps, chunk_thread_pool);
		}
		bool aggregate_chunks()
		{
			built_meshes_vec_type built_meshes_tmp;
			{
				built_meshes.get()->swap(built_meshes_tmp);
			}
			bool aggregated_something = false;
			for (auto& mesh : built_meshes_tmp)
			{
				pending_octrees[mesh->chunk_data.root->base_node_size].erase(mesh->chunk_data.relative_pos);
				octrees_waiting_finalizing.insert_or_assign(glm::ivec4{ mesh->chunk_data.relative_pos, mesh->chunk_data.root->base_node_size }, mesh);
				aggregated_something = true;
			}
			return aggregated_something;
		}
		//builds their gpu_mesh
		//uploads the gpu meshes
		//adds the gpu mesh to meshes to render
		void finalize_octrees(spawnx_vulkan::engine& engine)
		{
			static bool uploaded_data = false;
			const spawnx_vulkan::camera& cam = *engine.cameras[0].lock();
			const spawnx_vulkan::camera& shadow_camera = *engine.shadow_camera.lock();
			for (auto& chunk_and_key : octrees_waiting_finalizing)
			{
				auto& chunk = chunk_and_key.second;
				if (chunk->mesh && !chunk->mesh->indices.empty())
				{
					*chunk->chunk_data.chunk_gpu_model = spawnx_vulkan::model{ engine.descr_set_layout,engine.shadow_descriptor_set_layout, engine.vulkan_instance };
					auto& model = *chunk->chunk_data.chunk_gpu_model;
					model.make_default(engine.vulkan_instance);
					model.cpu_local_mesh = chunk->mesh;
					model.position_matrix = glm::translate(glm::mat4(), glm::vec3(chunk->chunk_data.relative_pos));
					model.set_cameras(cam, shadow_camera);
					model.shadow_view = engine.shadow_image.image_view;
					chunk->chunk_data.chunk_gpu_model->upload_data(engine.vulkan_instance);
					engine.add_model(chunk->chunk_data.chunk_gpu_model);
					uploaded_data = true;
				}
				for (size_t i = octree_chunks_lod.size(); i <= chunk->chunk_data.root->base_node_size; ++i)
				{
					octree_chunks_lod.push_back({});
				}
				octree_chunks_lod[chunk->chunk_data.root->base_node_size].insert_or_assign(chunk->chunk_data.relative_pos, chunk->chunk_data);
			}
			octrees_waiting_finalizing.clear();
		}
		void upload_octrees(spawnx_vulkan::engine& engine)
		{
			const spawnx_vulkan::camera& cam = *engine.cameras[0].lock();
			const spawnx_vulkan::camera& shadow_camera = *engine.shadow_camera.lock();
			for (auto& chunk_and_key : octrees_waiting_finalizing)
			{
				auto& chunk = chunk_and_key.second;
				if (chunk->mesh && !chunk->mesh->indices.empty())
				{
					*chunk->chunk_data.chunk_gpu_model = spawnx_vulkan::model{ engine.descr_set_layout,engine.shadow_descriptor_set_layout, engine.vulkan_instance };
					auto& model = *chunk->chunk_data.chunk_gpu_model;
					model.make_default(engine.vulkan_instance);
					model.cpu_local_mesh = chunk->mesh;
					model.position_matrix = glm::translate(glm::mat4(), glm::vec3(chunk->chunk_data.relative_pos));
					model.set_cameras(cam, shadow_camera);
					model.shadow_view = engine.shadow_image.image_view;
					chunk->chunk_data.chunk_gpu_model->upload_data(engine.vulkan_instance);
				}
			}
		}
		//this must come in the same update block, and after upload_octrees()
		void add_engine_models(spawnx_vulkan::engine& engine)
		{
			for (auto& chunk_and_key : octrees_waiting_finalizing)
			{
				if (get_chunk_neighbor_encoding(chunk_and_key.first,chunk_and_key.first.w) != *chunk_and_key.second->chunk_data.neighbor_encode)
					continue;
				auto& chunk = chunk_and_key.second;
				if (chunk->mesh && !chunk->mesh->indices.empty())
				{
					engine.add_model(chunk->chunk_data.chunk_gpu_model);
				}
				for (size_t i = octree_chunks_lod.size(); i <= chunk->chunk_data.root->base_node_size; ++i)
				{
					octree_chunks_lod.push_back({});
				}
				octree_chunks_lod[chunk->chunk_data.root->base_node_size].insert_or_assign(chunk->chunk_data.relative_pos, chunk->chunk_data);
			}
			octrees_waiting_finalizing.clear();
		}
		void invalidate_non_used_chunks(spawnx_vulkan::engine& engine)
		{
			//basically make sure the chunks we don't use are at least never rendered. later on make sure they're never worked on.
			bool did_anything = false;
			auto itr = octrees_waiting_finalizing.begin();
			while (itr != octrees_waiting_finalizing.end())
			{
				auto candidate = required_chunks.find(itr->first);
				if (candidate == required_chunks.end())
				{
					itr = octrees_waiting_finalizing.erase(itr);
					did_anything = true;
				}
				else
					++itr;
			}
			if (did_anything)
			{
				engine.rerecord_commandbuffers = true;
			}
		}
		bool has_pending_work() const
		{
			for (auto pending_group : pending_octrees)
			{
				if (!pending_group.empty())
					return true;
			}
			return false;
		}
		void dirty_chunks()
		{
			//instead of acquiring a list of deltas maybe we should acquire a list of expected values for what our seam trees will be
			//store a group of trees that it depended on, and if the expected vs actual differs then we just update it.
			//along with making sure that the trees when looking for dependencies depend specifically on the trees we expect to be loaded vs some lazy algorithm.
			if (has_pending_work())
				return;
			for (auto& chunk_group : octree_chunks_lod)
			{
				for (auto& chunk : chunk_group)
				{
					if (chunk.second.root->is_dirty)
						continue;
					auto tmp = get_chunk_neighbor_encoding(chunk.first, chunk.second.root->base_node_size);
					if (*chunk.second.neighbor_encode != tmp)
					{
						chunk.second.root->is_dirty = true;
						pending_octrees[chunk.second.root->base_node_size].insert(chunk);
					}
				}
			}
			for (auto& chunk : octrees_waiting_finalizing)
			{
				if (chunk.second->chunk_data.root->is_dirty)
				{
					pending_octrees[chunk.first.w].insert({ chunk.first,chunk.second->chunk_data });
				}
			}
		}
		void update_required_chunks(const glm::ivec3& world_pos, int view_distance)
		{
			//get a list of chunks that we need for our current location.
			//make it known to the rest of the class.
			//if we have not moved don't bother checking for our current chunk's list.
			glm::ivec3 rooted_pos = world_pos_grid_aligned(world_pos, 0);

			std::unordered_map<glm::ivec4, int> required;
			if (rooted_pos != old_rooted_pos)
			{
				moved_this_update = true;
				old_rooted_pos = rooted_pos;
				auto req_chunks = get_chunks_to_load(world_pos, view_distance);
				for (auto& req : req_chunks)
				{
					required.insert({ glm::ivec4{ req.first,req.second },0 });
				}
				required_chunks = std::move(required);
			}
			else
				moved_this_update = false;
		}

		bool remove_unused_chunks()
		{
			bool removed_some_chunks = false;
			if (has_pending_work() || required_chunks.empty())
				return removed_some_chunks;
			for (auto& chunk_group : octree_chunks_lod)
			{
				auto itr = chunk_group.begin();
				while (itr != chunk_group.end())
				{
					auto candidate = required_chunks.find(glm::ivec4{ itr->first, itr->second.root->base_node_size });
					if (candidate == required_chunks.end())
					{
						itr = chunk_group.erase(itr);
						removed_some_chunks = true;
					}
					else
						++itr;
				}
			}
			return removed_some_chunks;
		}
		//retard spelling until it's proven to work.
		void update(const glm::ivec3& world_pos, int view_distance, spawnx_vulkan::engine& engine)
		{
			hmap_manager.finalize_hmaps();
			if (remove_unused_chunks())
				engine.rerecord_commandbuffers = true;

			update_required_chunks(world_pos, view_distance);
			aggregate_chunks();
			invalidate_non_used_chunks(engine);
			finalize_octrees(engine);

			//need to dirty chunks
			dirty_chunks();
			//need to build new chunks
			generate_chunks();
			//remove unused chunks

		}
		void update_2(const glm::ivec3& world_pos, int view_distance, spawnx_vulkan::engine& engine)
		{
			hmap_manager.finalize_hmaps();
			///always:
			//always figure out what the current required chunks are.
			update_required_chunks(world_pos, view_distance);

			///only on movement
			//what is supposed to happen in this update:
			//figure out what chunks if this update could be instant, are supposed to exist (handled above)
			//update any out of date meshes (aka dirty them + reseam those chunks)
			//if a chunk's sdf has somehow changed (based on a range lookup or something)
			//mark that chunk dead.
			//basically queue up any actual work.
			//if (moved_this_update)
			{
				//determine what chunks need to have their seams regenerated this update
				dirty_chunks();
				generate_chunks();
				//determine what chunks need to be created for this update
			}

			///only if new data is meant to be processed e.g. if there are actually finished meshes
			//as the next block of the update; since everything else is queued to be generating meshes
			//aggregate the finalized meshes
			//push? the meshes to the gpu.
			//don't queue them for rendering yet.
			if (aggregate_chunks())
			{
				upload_octrees(engine);
			}

			///only if all generation is done/a break point is reached/forced for pushing the chunks to render.
			//if all the chunks have the correct neighbor profiles great, continue onto the next steps
			//finalize all the octrees e.g. queue them for rendering
			//destroy all unneeded octrees
			if (bool status = all_chunks_are_valid())
			{
				add_engine_models(engine);
				remove_unused_chunks();
			}
			//maybe a periodic update to destroy pending octrees that have no reason to be used/find a way of canceling their job (if the job has not started).
		}
		bool all_chunks_are_valid()
		{
			//this is supposed to perform a bulk check, to make sure that all pending and finished chunks (probably just pending)
			//exist in our required state.
			//optionally we could through out chunks that do not meet our required matching state at all, thus removing chunks that wouldn't need to be rendered.
			//but the above ^^^^
			//would break caching when I finally get around to implementing it.
			bool status = true;
			for (auto& chunk_and_lod : required_chunks)
			{
				glm::ivec3 chunk_coord = chunk_and_lod.first;
				int lod_level = chunk_and_lod.first.w;

				if (lod_level < octree_chunks_lod.size())
				{
					auto itr = octree_chunks_lod[lod_level].find(chunk_coord);
					if(itr != octree_chunks_lod[lod_level].end() && *itr->second.neighbor_encode == get_chunk_neighbor_encoding(chunk_coord, lod_level))
						continue;
				}

				auto candidate_octree = octrees_waiting_finalizing.find(chunk_and_lod.first);
				//if the chunk does not exist anywhere at all its impossible for us to not have holes.
				//additionally its possible that the chunk has the wrong/old seam and thus is also not valid.
				if (candidate_octree == octrees_waiting_finalizing.end())
				{
					status = false;
				}
				else
				{
					auto expected_encoding = get_chunk_neighbor_encoding(chunk_coord, lod_level);
					auto& actual_encoding = *candidate_octree->second->chunk_data.neighbor_encode;
					if (expected_encoding != actual_encoding)
					{
						status = false;
						candidate_octree->second->chunk_data.root->is_dirty = true;
					}
				}
			}
			return status;
		}
		bool check_loaded_neighbors()
		{
			for (auto& required : required_chunks)
			{
				auto itr = octree_chunks_lod[required.first.w].find(required.first);
				if (itr == octree_chunks_lod[required.first.w].end() || *itr->second.neighbor_encode != get_chunk_neighbor_encoding(required.first, required.first.w))
					return false;
			}
			return true;
		}
		void force_chunk_recheck_on_update()
		{
			old_rooted_pos = glm::ivec3(std::numeric_limits<int>::max());
		}
	};
	//what I want to write:
	void func()
	{
		chunk_manager chunks;
		height_map_manager hmap_manager;
		//chunks.update({ 0,0,0 }, 4000, );
	//	sdf_container sdf;
	//	chunk_manager.update(local_pos, sdf, render_engine);
	//	chunk_manager.rebuild_affected_chunks(sdf_edit);

	}

}