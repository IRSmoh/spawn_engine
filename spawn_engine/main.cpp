#define BT_NO_SIMD_OPERATOR_OVERLOADS	//why the fucking shit bullet thinks this is a good idea is beyond me; they break core logic of the laguage for fucks sake
#include <btBulletDynamicsCommon.h>
#include "spawn_engine.h"
#include <vulkan_renderer/im_gui.h>
#include "inventory.h"
#include <spawnx/spawnx.h>
template<typename type>
struct bullet_ptr_wrapper
{
	std::unique_ptr<type> wrapped_object;
	bullet_ptr_wrapper(std::unique_ptr<type> ptr) : wrapped_object(std::move(ptr)) {}
	bullet_ptr_wrapper(type* ptr) : wrapped_object({ ptr }) {}
	bullet_ptr_wrapper(bullet_ptr_wrapper&&) = default;
	bullet_ptr_wrapper& operator=(bullet_ptr_wrapper&&) = default;
	operator type*() const
	{
		return wrapped_object.get();
	}
	operator type&() const
	{
		return *wrapped_object.get();
	}
	template<typename convertible_type>
	operator convertible_type*()
	{
		return wrapped_object.get();
	}
	type* operator->()
	{
		return wrapped_object.get();
	}
};

//requires all lifetime pointers of a rigidbody so we can keep things alive sanely.
class rigid_body_handler
{
	btDynamicsWorld* world;
	std::unique_ptr<btRigidBody> rigid_body;
	//bullet_ptr_wrapper<btCollisionShape> collision_shape;
	//bullet_ptr_wrapper<btDefaultMotionState> motion_state;
public:
	template<typename world_type, std::enable_if_t<std::is_base_of<btDynamicsWorld, world_type>::value, bool> = true>
	rigid_body_handler(const std::unique_ptr<world_type>& _world, std::unique_ptr<btRigidBody>&& _rigid_body) :
		world(_world.get()), rigid_body(std::move(_rigid_body))
	{}
	template<typename world_type, std::enable_if_t<std::is_base_of<btDynamicsWorld, world_type>::value, bool> = true>
	rigid_body_handler(const bullet_ptr_wrapper<world_type>& _world, std::unique_ptr<btRigidBody>&& _rigid_body) :
		world(_world), rigid_body(std::move(_rigid_body))
	{}
	rigid_body_handler(btDynamicsWorld* _world, btRigidBody* _rigid_body, btCollisionShape* _collision_shape, btDefaultMotionState* _motion_state) :
		world(_world), rigid_body(_rigid_body)
	{}
	~rigid_body_handler()
	{
		//we don't have to delete our collision/motion states. they're already handled.
		world->removeRigidBody(rigid_body.get());
	}
	operator btRigidBody*()
	{
		return rigid_body.get();
	}
	btRigidBody* operator->()
	{
		return rigid_body.get();
	}
	const btRigidBody* operator->() const
	{
		return rigid_body.get();
	}
};

struct uniform_buffer_object_matrices
{
	glm::mat4 camera;
	glm::mat4 projection;
};
struct model
{
	spawnx_vulkan::default_cpu_mesh target_mesh;
	vulkan_image target_texture;
	texture_sampler sampler;

	//probably not even needed to be known by the model?
	gpu_data_buffer shader_shared_matrices_buffer;
	gpu_data_buffer model_matrix_buffer;

};
int main()
{

	bullet_ptr_wrapper<btBroadphaseInterface> broadphase{ std::make_unique<btDbvtBroadphase>() };

	bullet_ptr_wrapper<btDefaultCollisionConfiguration> collisionConfiguration{ std::make_unique<btDefaultCollisionConfiguration>() };
	bullet_ptr_wrapper<btCollisionDispatcher> dispatcher{ std::make_unique<btCollisionDispatcher>(collisionConfiguration) };

	bullet_ptr_wrapper<btSequentialImpulseConstraintSolver> solver{ std::make_unique<btSequentialImpulseConstraintSolver>() };

	bullet_ptr_wrapper<btDiscreteDynamicsWorld> dynamicsWorld{ std::make_unique<btDiscreteDynamicsWorld>(dispatcher, broadphase, solver, collisionConfiguration) };

	dynamicsWorld->setGravity(btVector3(0, -10, 0));


	bullet_ptr_wrapper<btCollisionShape> groundShape { std::make_unique<btStaticPlaneShape>(btVector3(0.f, 1.f, 0.f), 1.f) };

	bullet_ptr_wrapper<btCollisionShape> fallShape{ std::make_unique<btSphereShape>(1.f) };


	bullet_ptr_wrapper<btDefaultMotionState> groundMotionState{ std::make_unique<btDefaultMotionState>(btTransform(btQuaternion(0, 0, 0, 1), btVector3(0, -1, 0))) };

	bullet_ptr_wrapper<btDefaultMotionState> fallMotionState{
		std::make_unique<btDefaultMotionState>(btTransform(btQuaternion(0, 0, 0, 1), btVector3(0, 20, 0))) };
	btScalar mass = 1;
	btVector3 fallInertia(0, 0, 0);
	fallShape->calculateLocalInertia(mass, fallInertia);
	btRigidBody::btRigidBodyConstructionInfo fallRigidBodyCI(mass, fallMotionState, fallShape, fallInertia);
	rigid_body_handler fallRigidBody{ dynamicsWorld, std::make_unique<btRigidBody>(fallRigidBodyCI)};
	dynamicsWorld->addRigidBody(fallRigidBody);

	spawnx::world world;

	spawnx_vulkan::engine engine;

	engine.user_screen_resize_fnc = [](int width, int height)
	{
		Keys::Init(width,height);
	};


	auto end = std::chrono::high_resolution_clock::now();
	auto start = std::chrono::high_resolution_clock::now();

	auto print_time = [](auto& start, auto& end, const auto& message)
	{
		end = std::chrono::high_resolution_clock::now();
		std::cout << message << std::to_string(std::chrono::duration_cast<std::chrono::duration<double>>(end - start).count()) << "\n";
		start = end;
	};


	std::vector<vertex> verts;
	std::vector<uint32_t> inds;

	std::unique_ptr<std::pair<decltype(verts), decltype(inds)>> mesh;
	decltype(mesh) mesh_2;
	spawn_engine::height_map_manager hmap_manager;
	auto hmap_generator = spawn_engine::create_fractal_height_map_manager();

	engine.startup(false);
	Keys::Init(engine.vulkan_instance.window_width, engine.vulkan_instance.window_height);
	glfwSetKeyCallback(engine.vulkan_instance.window, Keys::KeyboardCallB);
	glfwSetCursorPosCallback(engine.vulkan_instance.window, Keys::MouseMovementCallB);

	//this invocation is long.
	float view_distance = 5'000.f;

	std::shared_ptr<spawnx_vulkan::camera> cam = std::make_shared<spawnx_vulkan::camera>(spawnx_vulkan::camera::create_camera(glm::perspective(glm::radians(70.f), engine.vulkan_instance.window_width / (float)engine.vulkan_instance.window_height, 0.1f, view_distance), engine.vulkan_instance));
	std::shared_ptr<spawnx_vulkan::camera> shadow_cam = std::make_shared<spawnx_vulkan::camera>(spawnx_vulkan::camera::create_camera(glm::perspective(glm::radians(70.f), 1.f, 0.1f, view_distance*2.f), engine.vulkan_instance));
	
	engine.add_camera(cam);
	engine.add_shadow_camera(shadow_cam);



	spawn_engine::chunk_manager chunk_manager{};
	start = std::chrono::high_resolution_clock::now();
	glm::vec3 load_center = { 64,0,0 };
	int load_distance = 4000;
	int second_load_distance = 400;
	chunk_manager.hmap_manager.set_height_map_generator(hmap_generator);
	chunk_manager.update(load_center, load_distance, engine);

	//bullet_ptr_wrapper<btTriangleIndexVertexArray> index_vert_arrays{ 
	//	new btTriangleIndexVertexArray((int32_t)mesh_inds.size()/3, (int*)mesh_inds.data(), sizeof(int)*3,
	//									(int32_t)mesh_verts.size()/3, &mesh_verts[0].pos.x, sizeof(vertex)) };

	//bullet_ptr_wrapper<btBvhTriangleMeshShape> arbitrary_terrain_mesh{ std::make_unique<btBvhTriangleMeshShape>(index_vert_arrays,true) };


	//btRigidBody::btRigidBodyConstructionInfo
	//	groundRigidBodyCI(0, groundMotionState, arbitrary_terrain_mesh);
	//rigid_body_handler groundRigidBody( dynamicsWorld, std::make_unique<btRigidBody>(groundRigidBodyCI) );
	//dynamicsWorld->addRigidBody(groundRigidBody);

	//start = std::chrono::high_resolution_clock::now();
	//for (int i = 0; i < 300; i++) {
	//	dynamicsWorld->stepSimulation(1 / 60.f, 10);

	//	btTransform trans;
	//	fallRigidBody->getMotionState()->getWorldTransform(trans);

		//std::cout << "sphere height: " << trans.getOrigin().getY() << "\t";
		//print_time(start, end, "sphere update time: ");
	//}
	//btTransform trans;
	//groundRigidBody->getMotionState()->getWorldTransform(trans);

	//std::cout << "asd: " << trans.getOrigin().getY() << "\n";

	glm::mat4 player_matrix = glm::mat4(1);
	glm::mat4 player_rotation = glm::mat4(1);

	shadow_cam->set_world_pos(glm::translate(glm::mat4(), glm::vec3(0.f, 1700.f, 0))*glm::rotate(glm::mat4(), glm::radians(-90.f), glm::vec3(1, 0, 0)));

	float move_speed = 10.f;
	float shift_multiplier = 30.f;
	float actual_speed = 0;
	float rot_speed = 60.f;
	double running_clock = 0.0;
	constexpr double fps_limit = 144;
	constexpr double fps_time = 1 / fps_limit;

	inventory inv(10,20);
	auto entity = world.make_entity();
	player_armor item_desc{};
	item_desc.abyssal_res.res_value = .32f;
	item_desc.fire_res.res_value = .35f;
	world.add_components_to_entity(entity, item_desc);
	inv.place({}, { glm::ivec2{2,4}, entity });
	auto camera_mover = [&]()
	{
		start = end;
		end = std::chrono::high_resolution_clock::now();
		double time = std::chrono::duration_cast<std::chrono::duration<double>>(end - start).count();
		running_clock += time;
		if (time < fps_time)
			std::this_thread::sleep_for(std::chrono::duration<double>(fps_time - time));

		Keys::UpdateKeys(engine.vulkan_instance.window);

		if (Keys::GetKey(KEY_LEFT_SHIFT) || Keys::GetKey(KEY_RIGHT_SHIFT))
		{
			actual_speed = move_speed*shift_multiplier;
		}
		else
		{
			actual_speed = move_speed;
		}
		if (Keys::GetKey(KEY_W))
		{
			auto vec = player_rotation*glm::vec4((glm::vec3(0, 0, 1)*-actual_speed*time), 0);
			player_matrix = glm::translate(player_matrix, glm::vec3(vec));
		}
		else if (Keys::GetKey(KEY_S))
		{
			auto vec = player_rotation*glm::vec4((glm::vec3(0, 0, 1)*actual_speed*time), 0);
			player_matrix = glm::translate(player_matrix, glm::vec3(vec));
		}
		if (Keys::GetKey(KEY_D))
		{
			auto vec = player_rotation*glm::vec4((glm::vec3(1, 0, 0)*actual_speed*time), 0);
			player_matrix = glm::translate(player_matrix, glm::vec3(vec));
		}
		else if (Keys::GetKey(KEY_A))
		{
			auto vec = player_rotation*glm::vec4((glm::vec3(1, 0, 0)*-actual_speed*time), 0);
			player_matrix = glm::translate(player_matrix, glm::vec3(vec));
		}

		player_rotation = glm::rotate(player_rotation, glm::radians(rot_speed*(float)Keys::delta_x), glm::vec3(0.f, 1.f, 0.f));
		player_rotation = glm::rotate(player_rotation, glm::radians(rot_speed*(float)Keys::delta_y), glm::vec3(1.f, 0.f, 0.f));

		cam->set_world_pos(player_matrix*player_rotation);
		//shadow_cam->set_world_pos(player_matrix*player_rotation);
		if (Keys::KeyDown(KEY_F1))
		{
			//at the time of writing this pipeline 0/1 are the same pipeline just toggled for wireframe/fill mode.
			//get a proper setup for this eventually.
			std::swap(engine.pipelines[0].pipeline, engine.pipelines[1].pipeline);
			engine.recreate_swapchain();
		}
		if(!Keys::KeyToggled(KEY_F2))
			load_center = glm::vec3(player_matrix[3]);
		if (Keys::KeyDown(KEY_F3))
		{
			std::swap(load_distance, second_load_distance);
			chunk_manager.force_chunk_recheck_on_update();
		}
		chunk_manager.update(load_center, load_distance, engine);

		if (Keys::KeyDown(KEY_F5))
		{
			if (!chunk_manager.check_loaded_neighbors())
				std::cout << "???\n";
		}

		inv.imgui_display(world);
	};


	engine.descriptor_set_creation();
	engine.main_loop(camera_mover);
}