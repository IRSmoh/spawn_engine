#pragma once
#include <vector>
#include <thread>
#include <mutex>
#include <shared_mutex>
#include <atomic>
#include <future>
#include <tbb/concurrent_vector.h>
#include <tbb/concurrent_queue.h>
struct thread_pool
{
	size_t num_threads;
	using job_type = std::function<void()>;
	std::vector<std::thread> threads;

	tbb::concurrent_queue<job_type> job_queue;

	std::mutex condition_mutex;	 //literally worthless save for conditional's requirements so we can pause our threads...
	std::condition_variable work_available_cv;
	std::atomic_bool terminate_threads = false;
	~thread_pool()
	{
		terminate_threads = true;
		work_available_cv.notify_all();
		for (auto& thread : threads)
			thread.join();
	}
	void init(unsigned int requested_threads = std::thread::hardware_concurrency())
	{
		num_threads = requested_threads;
		threads.reserve(num_threads);
		for (size_t i = 0; i < num_threads; ++i)
		{
			threads.push_back(std::thread(thread_main_fnc, std::ref(*this)));
		}
	}
	void add_job(std::function<void()>&& job)
	{
		job_queue.push(std::move(job));
		work_available_cv.notify_one();
	}
	void add_job(const std::function<void()>& job)
	{
		job_queue.push(job);
		work_available_cv.notify_one();
	}
	void add_jobs(std::vector<std::function<void()>>&& jobs)
	{
		for (auto&& job : jobs)
		{
			job_queue.push(std::move(job));
			work_available_cv.notify_all();	//whats the actual cost of calling this a lot?
		}
	}
private:
	static void thread_main_fnc(thread_pool& owning_pool)
	{
		while (true)
		{
			if (owning_pool.terminate_threads)
				return;
			{
				//the job should be in a different scope from everything else so expected lifetime semantics can be maintained.
				//e.g.... normally objects that are passed in would not be destructed until the next call to ::add_job(s) and even still maybe;
				//its entirely possible that a thread holds data that should be destroyed but it can't be because the thread owning it never wakes.
				//this prevents that.
				//fucking threading man it's a headache.
				job_type job;
				if (owning_pool.job_queue.try_pop(job))
					job();
			}
			if (owning_pool.job_queue.empty())
			{
				//this exist just so if theres no work to do we pause the thread.... If there is a better way that doesn't need a lock do tell.
				std::unique_lock<std::mutex> lock(owning_pool.condition_mutex);
				owning_pool.work_available_cv.wait(lock);
			}
		}
	}
};
struct work_finish_signal
{
	std::atomic_uint64_t work_count;
	std::promise<bool> thread_promise;
	std::future<bool> work_completed_future;
	work_finish_signal(uint64_t work_count_) : work_count(work_count_)
	{
		work_completed_future = thread_promise.get_future();
	}
	void decrement_and_announce()
	{
		if (--work_count == 0)

			thread_promise.set_value(true);
	}
};
struct calls_on_death
{
	std::function<void()> func_to_call;
	calls_on_death(std::function<void()>&& fnc) : func_to_call(std::move(fnc)) {}
	calls_on_death(calls_on_death&& right)
	{
		func_to_call = std::move(right.func_to_call);
		right.func_to_call = []() {};
	}
	calls_on_death& operator=(calls_on_death&& right)
	{
		func_to_call = std::move(right.func_to_call);
		right.func_to_call = []() {};
		return *this;
	}
	~calls_on_death()
	{
		func_to_call();
	}
};
struct shared_call_on_death
{
	std::shared_ptr<calls_on_death> thing_to_call;
	shared_call_on_death(std::function<void()>&& fnc) : thing_to_call(std::make_shared<calls_on_death>(calls_on_death{ std::move(fnc) }))
	{}
};
template<typename type, typename mutex_type>
struct mutexed_struct
{
	template<template<typename> typename lock_type>
	struct lock_and_data
	{
		lock_type<mutex_type> lock;
		type* data_ref;
		lock_and_data(mutex_type& m, type* _data) : lock(m), data_ref(_data)
		{}
		type* operator->()
		{
			return data_ref;
		}
		type* operator->() const
		{
			return data_ref;
		}
		type& operator*()
		{
			return *data_ref;
		}
		type& operator*() const
		{
			return *data_ref;
		}
	};

	mutex_type owned_mutex;
	type mutex_protected_data;
	mutexed_struct() : mutexed_struct(type{}) {}
	mutexed_struct(type&& data) : owned_mutex(), mutex_protected_data(std::forward<type>(data)) {}
	lock_and_data<std::unique_lock> get()
	{
		return { owned_mutex, &mutex_protected_data };
	}
	template<typename mtx_type = mutex_type, std::enable_if_t<std::is_same<std::shared_mutex, mtx_type>::value, bool> = true>
	lock_and_data<std::shared_lock> get_shared()
	{
		return { owned_mutex, &mutex_protected_data };
	}
};