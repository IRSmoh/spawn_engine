#pragma once
#include <memory>
#include <vector>
#include <array>
#include <vulkan_renderer\vulkan_renderer.h>
#include <glm\glm.hpp>
#include "height_map.h"
#include "QEF.h"
#include "misc_helper_functions.h"

namespace spawn_engine
{
	struct octree_draw_info
	{
		int index;
		int corners;
		glm::vec3 pos;
		glm::vec3 avg_normal;
		std::unique_ptr<QEF_data> qef;

		octree_draw_info() : index(-1), corners(0), qef(std::make_unique<QEF_data>()) {}
		octree_draw_info(const octree_draw_info& right) : index(right.index), corners(right.corners), pos(right.pos), avg_normal(right.avg_normal)
		{
			if (right.qef)
				qef = std::make_unique<QEF_data>(*right.qef);
			else
				qef = nullptr;
		}
		octree_draw_info(octree_draw_info&& right) noexcept : index(right.index), corners(right.corners), pos(right.pos), avg_normal(right.avg_normal), qef(std::move(right.qef))
		{}

		octree_draw_info& operator=(const octree_draw_info& right)
		{
			index = right.index;
			corners = right.corners;
			pos = right.pos;
			avg_normal = right.avg_normal;
			qef = std::make_unique<QEF_data>(*right.qef);
			return *this;
		}
		octree_draw_info& operator=(octree_draw_info&& right) noexcept
		{
			index = right.index;
			corners = right.corners;
			pos = right.pos;
			avg_normal = right.avg_normal;
			qef = std::move(right.qef);
			return *this;
		}
	};
	struct octree_node
	{
		enum node_type : char
		{
			node_none,
			node_internal,
			node_pseudo,
			node_leaf
		};
		std::unique_ptr<octree_draw_info> draw_info;
		int32_t next_node_block;


		glm::tvec3<char> internal_min;
		char internal_size:5;
		node_type type:3;

		static constexpr uint32_t invalid_node = -1;
		static constexpr char invalid_size = -1;

		octree_node() : draw_info(nullptr), next_node_block(invalid_node), internal_min({ 0,0,0 }), internal_size(invalid_size), type(node_internal) {}
		octree_node(node_type _type) : draw_info(nullptr), next_node_block(invalid_node), internal_min({ 0,0,0 }), internal_size(invalid_size), type(_type)
		{}
		octree_node(octree_node&& right) noexcept : next_node_block(right.next_node_block), internal_min(right.internal_min),  draw_info(std::move(right.draw_info)), internal_size(right.internal_size), type(right.type)
		{}
		octree_node& operator=(octree_node&& right) noexcept
		{
			type = right.type;
			internal_min = right.internal_min;
			internal_size = right.internal_size;
			next_node_block = right.next_node_block;
			draw_info = std::move(right.draw_info);
			return *this;
		}
		octree_node(const octree_node& right) : next_node_block(right.next_node_block), internal_min(right.internal_min), internal_size(right.internal_size), type(right.type)
		{
			if (right.draw_info)
				draw_info = std::make_unique<octree_draw_info>(*right.draw_info);
		}
		octree_node& operator=(const octree_node& right)
		{
			type = right.type;
			internal_min = right.internal_min;
			internal_size = right.internal_size;
			next_node_block = right.next_node_block;
			if (right.draw_info)
				draw_info = std::make_unique<octree_draw_info>(*right.draw_info);
			return *this;
		}
		void set_size(int32_t size)
		{
			internal_size = bit_scan(size);
		}
		int32_t size() const
		{
			if (internal_size == invalid_size)
				return 0;
			return 1 << internal_size;
		}
		void set_min(glm::ivec3 min)
		{
			internal_min = min >> (int32_t)internal_size;
		}
		glm::ivec3 min() const
		{
			return glm::ivec3( internal_min ) << (int32_t)internal_size;
		}
		bool is_leaf() const
		{
			return next_node_block == invalid_node;
		}
		bool is_valid() const
		{
			return !(is_leaf() && type == octree_node::node_internal);
		}

		static octree_node root_equiv_node()
		{
			octree_node node{};
			node.next_node_block = 0;
			return node;
		}
	};
	struct octree_root
	{
		enum material
		{
			material_air,
			material_solid
		};
		struct data_block
		{
			using block_type = std::array<octree_node, 8>;
			uint32_t owner_arr_ind : 3;
			uint32_t owner_offset : 29;
			block_type node_block;

			data_block() :
				owner_arr_ind(0),
				owner_offset(0),
				node_block({})
			{}
			bool is_dead()
			{
				return owner_offset == (536870911);
			}
		};
		std::vector<data_block> nodes;
		constexpr static int32_t default_max_depth = 6;
		constexpr static int32_t default_chunk_size = 1 << default_max_depth;
		constexpr static int32_t default_min_node_size = 1;
		constexpr static float QEF_ERROR = 1e-6f;
		constexpr static int QEF_SWEEPS = 1;

		glm::ivec3 relative_pos;
		int32_t max_depth;
		int32_t base_node_size;
		int bounding_size;
		bool is_cleaned_tree : 1;
		bool is_dirty : 1;
		bool is_generated : 1;

		using cpu_mesh_data_type = std::shared_ptr<spawnx_vulkan::cpu_mesh<spawnx_vulkan::position,spawnx_vulkan::normal,spawnx_vulkan::uvs>>;
		cpu_mesh_data_type gpu_mesh_data;
		spawnx_vulkan::cpu_mesh_offsets start_of_seam_tree;

		//create a sane insert
		octree_root() : octree_root({}, default_max_depth, default_min_node_size)
		{}
		octree_root(const glm::ivec3& relative_pos_, int32_t base_node_size_) : octree_root(relative_pos_, default_max_depth, base_node_size_)
		{}
		octree_root(const glm::ivec3& relative_pos_, int32_t _max_depth, int32_t base_node_size_) :
			nodes(),
			relative_pos(relative_pos_),
			max_depth(_max_depth),
			base_node_size(bit_scan(base_node_size_)),
			bounding_size(1 << (max_depth + base_node_size)),
			is_cleaned_tree(false),
			is_dirty(false),
			is_generated(false)
		{
			nodes.reserve(4024);
			allocate_nodes();
		}
		void add_node_(const octree_node& placement_node, const glm::ivec3& insert_pos)
		{

		}
		void add_node(const octree_node& placement_node, const glm::ivec3& lookup_pos, int32_t curr_depth = 0, int32_t curr_node_offset = 0, int32_t node_sub_index = 0, glm::ivec3 relative_pos = {})
		{
			//currently this is too willing to create new nodes; figure out why; too lazy right now.
			//probably do do with assuming we have to create new nodes even if we're at the lowest level anyway. idfk
			float offset = (float)(1 << (curr_depth + 1));
			float node_size = bounding_size / offset;
			glm::ivec3 diffs = lookup_pos - relative_pos;
			int32_t curr_sub_index = ((diffs.x >= node_size) << 2) | ((diffs.y >= node_size) << 1) | ((diffs.z >= node_size) << 0);
			glm::vec3 node_local_root{
				relative_pos.x + ((curr_sub_index & 0x4) >> 2)*node_size,
				relative_pos.y + ((curr_sub_index & 0x2) >> 1)*node_size,
				relative_pos.z + ((curr_sub_index & 0x1))*node_size
			};
			curr_sub_index = curr_depth == max_depth ? node_sub_index : curr_sub_index;
			octree_node& candidate_node = get_node(curr_node_offset, curr_sub_index);
			if (curr_depth == max_depth || placement_node.size() == candidate_node.size())
			{
				candidate_node = placement_node;
				return;
			}
			if (!candidate_node.is_valid())
			{
				int32_t new_ind = allocate_nodes();
				for (int i = 0; i < 8; ++i)
				{
					auto& new_node = get_node({ new_ind, i });
					new_node.set_size((int)(node_size / 2));
				}
				auto& old_node = get_node({ curr_node_offset, curr_sub_index });
				old_node.next_node_block = new_ind;
				auto& node_block = nodes[new_ind];
				node_block.owner_arr_ind = curr_sub_index;
				node_block.owner_offset = curr_node_offset;
			}
			int32_t next_block = get_node(curr_node_offset, curr_sub_index).next_node_block;
			if (next_block != -1)
				add_node(placement_node, lookup_pos, curr_depth + 1, next_block, curr_sub_index, node_local_root);
		}
		std::pair<const octree_node&, int> get_(const glm::ivec3& lookup_pos) const
		{
			auto morton_code = morton_encode_3d(lookup_pos);
			int curr_node_offset = 0;
			int max_depth = bit_scan(bounding_size);
			for (int curr_depth = max_depth-1; curr_depth >= 0; --curr_depth)
			{
				const auto& curr_node = get_node(curr_node_offset, relative_offset(morton_code, curr_depth));
				if (curr_node.is_leaf())
					return { curr_node, bounding_size >> curr_depth };	//verify the total paths that nodes can be assigned to see if .size() can ever be invalid in this case.
				curr_node_offset = curr_node.next_node_block;
			}
			//should be unreachable
			throw std::runtime_error("shouldn't be reached ever.");
			return { get_node(0,0),0 };
		}
		std::pair<const octree_node&,int> get(const glm::ivec3& lookup_pos, int32_t curr_depth = 0, const int32_t curr_node_offset = 0, const glm::ivec3& relative_pos = glm::ivec3{}) const
		{
			//determine what sub chunk we're supposed to be in.
			int node_size = bounding_size >> (curr_depth + 1);
			glm::ivec3 diffs = lookup_pos - relative_pos;
			int32_t curr_sub_index = ((diffs.x >= node_size) << 2) | ((diffs.y >= node_size) << 1) | ((diffs.z >= node_size) << 0);
			glm::ivec3 next_relative_pos = {
				relative_pos.x + (diffs.x >= node_size)*node_size,
				relative_pos.y + (diffs.y >= node_size)*node_size,
				relative_pos.z + (diffs.z >= node_size)*node_size
			};
			volatile auto morton_offset = relative_offset(lookup_pos, bit_scan(node_size));
			if (morton_offset != curr_sub_index)
				throw std::runtime_error("zzzzzzzzzzzzzzzzzzzzzzzzz");
			const octree_node& node = get_node(curr_node_offset, curr_sub_index);

			if (node.is_leaf())
			{
				return { node,node_size };
			}
			return get(lookup_pos, curr_depth + 1, node.next_node_block, next_relative_pos);
		}
		void simplify_octree(float threshold)
		{
			if (is_cleaned_tree)
				return;
			octree_node root_equiv = octree_node::root_equiv_node();
			simplify_octree_impl(&root_equiv, threshold);
			//simplify_octree_impl(octree_node::root_equiv_node(), threshold);
		}
		octree_node* simplify_octree_impl(octree_node* node, float threshold)
		{
			if (!node || (node->next_node_block == -1 && node->draw_info == nullptr))
				return nullptr;
			if (node->type != octree_node::node_internal)
			{
				return node;
			}

			auto get_nullable_node = [&](std::pair<uint32_t, uint32_t> inds) -> octree_node*
			{
				if (inds.first > nodes.size())
					return nullptr;
				return &get_node(inds);
			};
			QEF_solver qef;
			int signs[8] = { -1, -1, -1, -1, -1, -1, -1, -1 };
			int midsign = -1;
			int edge_count = 0;
			bool is_collapsible = true;

			int failed_count = 0;	//sometimes we have literal garbage while the algo typically expects sane trees with at least some data.

			for (int i = 0; i < 8; ++i)
			{
				octree_node* child_candidate = simplify_octree_impl(get_nullable_node({ node->next_node_block, i }), threshold);
				//get_node({ node->next_node_block,i }) = child_candidate ? *child_candidate : octree_node{};

				if (child_candidate)
				{
					get_node({ node->next_node_block,i }) = *child_candidate;
					octree_node* child = child_candidate;
					int upper_bound = 1 << (max_depth + base_node_size);
					//make sure we're not on the border of a chunk; removing nodes near where a seam would be causes geometry loss.
					glm::ivec3 child_min = child->min();
					if (child_min.x <= 0 || child_min.y <= 0 || child_min.z <= 0 ||
						child_min.x + child->size() >= upper_bound || child_min.y + child->size() >= upper_bound || child_min.z + child->size() >= upper_bound)
						is_collapsible = false;

					if (child->type == octree_node::node_internal)
						is_collapsible = false;
					else
					{
						qef.add(*child->draw_info->qef);
						midsign = (child->draw_info->corners >> (7 - i)) & 1;
						signs[i] = (child->draw_info->corners >> i) & 1;

						++edge_count;
					}
				}
				else
					failed_count++;
			}
			if (!is_collapsible)
				return node;	//cannot collapse due to one or more children nodes being an internal node.
			if (failed_count == 8)
				return node;
			glm::vec3 qef_position;
			qef.solve(qef_position, QEF_ERROR, QEF_SWEEPS, QEF_ERROR);
			float error = qef.get_error();

			if (error > threshold)
			{
				//the amount of induced error would cause us to lose too much feature detail.
				return node;
			}
			glm::vec3 node_size = node->min();
			if (qef_position.x < node_size.x || qef_position.x >(node_size.x + node->size()) ||
				qef_position.y < node_size.y || qef_position.y >(node_size.y + node->size()) ||
				qef_position.z < node_size.z || qef_position.z >(node_size.z + node->size()))
			{
				qef_position = qef.get_mass_point();
			}

			std::unique_ptr<octree_draw_info> draw_info = std::make_unique<octree_draw_info>();

			for (int i = 0; i < 8; ++i)
			{
				if (signs[i] == -1)
					draw_info->corners |= (midsign << i);
				else
					draw_info->corners |= (signs[i] << i);
			}

			draw_info->avg_normal = {};
			for (int i = 0; i < 8; ++i)
			{
				octree_node* child = get_nullable_node({ node->next_node_block,i });
				if (child)
				{
					if (child->type == octree_node::node_pseudo || child->type == octree_node::node_leaf)
					{
						draw_info->avg_normal += child->draw_info->avg_normal;
					}
				}
			}

			draw_info->avg_normal = glm::normalize(draw_info->avg_normal);
			draw_info->pos = qef_position;
			*draw_info->qef = qef.get_data();

			for (int i = 0; i < 8; ++i)
			{
				octree_node* child = get_nullable_node({ node->next_node_block,i });
				if (child)
				{
					mark_dead(*child);
				}
			}
			node->type = octree_node::node_pseudo;
			node->draw_info = std::move(draw_info);
			return node;
		}
		void mark_dead(octree_node& node)
		{
			if (node.next_node_block == -1)
				return;
			nodes[node.next_node_block].owner_offset = octree_node::invalid_node;
			nodes[node.next_node_block].owner_arr_ind = octree_node::invalid_node;
			node.next_node_block = octree_node::invalid_node;
		}
		//linear (searches from begin/end)
		void remove_dead_nodes()
		{
			//if its linear collapse its easy; just remove the right most elements
			//if its patching holes and shifting data around.....
			//all moved data must re-edit their owner refs and specify their new location (aka fucking slow)

			//new data is always added at the end
			//aka its impossible (if tracing down the dependency list)
			//to wind up at a node whose parent comes after the node. (nothing is inserted in gaps)
			//that is not to say; that nodes wouldn't need to be moved multiple times (that needs to be fixed somehow)

			auto find_dead_node = [](auto begin, auto end)
			{
				while (begin != end)
				{
					if (begin->is_dead())
						return begin;
					++begin;
				}
				return end;
			};
			auto find_valid_node = [](auto begin, auto end)
			{
				while (begin != end)
				{
					if (!begin->is_dead())
						return begin;
					++begin;
				}
				return end;
			};
			auto reconnect_node = [&](auto node_to_move, auto block_relocation_itr)
			{
				int32_t new_header_offset = (int32_t)(block_relocation_itr - nodes.begin());
				*block_relocation_itr = std::move(*node_to_move);

				//ex of problem:
				//[0,X,X,0,X,X]
				//we want to move the 0 over and remove as many x's as possible.
				//all the 0's dependencies will still exist at their original location while this is executed.
				//and everything "left" of this will be unchanged (e.g. if this node depends on another node it can be assured their parent will not be moved)

				//things that need to be changed:
				//everything that depends on this blocks location and whatever it owns.
				//aka all .next_node_block's must be updated to point to the now closer location.
				//all .owner must be updated; not .offset since that can't/won't ever change once set.

				octree_node& owning_node = get_node(block_relocation_itr->owner_offset, block_relocation_itr->owner_arr_ind );

				owning_node.next_node_block = new_header_offset;

				for (auto& node : block_relocation_itr->node_block)
				{
					if (!node.is_leaf())	//does something depend on us?
					{
						nodes[node.next_node_block].owner_offset = new_header_offset;
					}
				}
			};

			using itr_type = typename decltype(nodes)::iterator;
			itr_type start_of_hole_itr = find_dead_node(nodes.begin(), nodes.end());
			itr_type start_of_actual_data_itr{ start_of_hole_itr };	//will be reset frequently anyway..
			itr_type end_of_data_itr = start_of_actual_data_itr;

			while (start_of_actual_data_itr != nodes.end())
			{
				start_of_actual_data_itr = find_valid_node(end_of_data_itr, nodes.end());
				end_of_data_itr = find_dead_node(start_of_actual_data_itr, nodes.end());

				while (start_of_actual_data_itr != end_of_data_itr)
				{
					reconnect_node(start_of_actual_data_itr++, start_of_hole_itr++);
				}
			}
			nodes.erase(start_of_hole_itr, nodes.end());
		}
		bool valid_pos(const glm::vec3& relative_pos)
		{
			auto lam = [&](auto& sub_pos)
			{
				return sub_pos < bounding_size && sub_pos >= 0;
			};
			return lam(relative_pos.x) && lam(relative_pos.y) && lam(relative_pos.z);
		}
		int32_t allocate_nodes()
		{
			//instead of allocating individually; keep track of where our last node is.
			//allocate in large fixed block amounts; or allocate as many as .capacity in one go.
			//then give us a shrink_to_fit that throws away all those other nodes.
			///what does that mean for deletion performance of 'assumed dead nodes'?

			//^^^^ doesn't appear to work at easiest glance; turns out making octree_nodes is the expensive as all hell part.. should figure out how to make them cheaper.
			int32_t ind = (int32_t)nodes.size();
			nodes.emplace_back(data_block{});
			return ind;
		}
		void shrink_to_fit()
		{
			nodes.shrink_to_fit();
		}
		//density functor must capture all but the world pos
		//along with world_pos being its sole parameter.
		//and return a float indicating the relative density (negative inside volume, positive outside)
		//world_pos is the approximation of a point on the surface. how could we generate the normal? -> what would be a valid vector for our cross product
		// -> we have our delta vectors from awhile back; we could generate a normal vector from that couldn't we?
		template<typename generator_type>
		static glm::vec3 calculate_surface_normal(const glm::vec3& world_pos, const generator_type& density_generator)
		{
			constexpr float H = .001f;	// our step for determining rate of change for our derivative.
										//dx/dy/dz are the derivatives. 
			constexpr glm::vec3 offset_x = { H,0,0 };
			constexpr glm::vec3 offset_y = { 0,H,0 };
			constexpr glm::vec3 offset_z = { 0,0,H };
			float center = density_generator(world_pos);
			float dx = density_generator(world_pos + offset_x) - center;
			float dy = density_generator(world_pos + offset_y) - center;
			float dz = density_generator(world_pos + offset_z) - center;

			return glm::normalize(glm::vec3(dx, dy, dz));
		}
		static auto approximate_zero_crossing_position(const glm::vec3& pos_0, const glm::vec3& pos_1, float density_0, float density_1)
		{
			//calculate the linear approximation of the zero point.
			// to do so; figure out the density at our given positions
			//calculate their relative weighting when it comes to the total delta
			//figure out how much of a % we need to move along our delta (mind you; the closer to zero a pos is the more weight its supposed to have.)

			//one or the other density must be negative.
			float density_weights[2] = { abs(density_0), abs(density_1) };
			auto calc_crossing_point = [](const glm::vec3& pos_0, const glm::vec3& pos_1, float weight_0, float weight_1)
			{
				float inv_total = 1.f / ((weight_0 + weight_1) / weight_0);
				return pos_0 + (pos_1 - pos_0)*inv_total;
			};
			return calc_crossing_point(pos_0, pos_1, density_weights[0], density_weights[1]);

		}
		template<typename generator_type, typename height_map_type>
		void build_tree(generator_type& generator, const height_map_type& hmap, const glm::vec3& world_pos, const glm::vec3& relative_pos = {}, const int32_t& curr_depth = 0, const int32_t& curr_node_offset = 0, const int32_t& curr_sub_node_index = 0)
		{
			//so; we want to build the entire tree in as few leaf nodes as possible.
			//so we can check per level we're at if theres any change to the density function on our corners
			//if there is (and we're not the min level)
			//then go down a level and keep checking
			//if we're at the min level build our data
			//else (from no density change)
			//continue to the next node and check further.
			//offer an optional ability to force checking to a specified level of nodes (this should disable checking the density function until that node depth)
			//this would prevent skipping small features if the user knows they're going to have small features (or suspects they would).

			auto get_corners = [&](const glm::vec3& relative_pos, float node_size)
			{
				struct corners_and_densities
				{
					int32_t corners;
					float densities[8];
				};
				corners_and_densities data{};
				for (int32_t i = 0; i < 8; ++i)
				{
					glm::ivec3 corner_pos = world_pos + relative_pos + glm::vec3(CHILD_MIN_OFFSETS[i]) * node_size;
					data.densities[i] = generator(corner_pos);
					auto material = data.densities[i] < 0.f;
					data.corners |= (material << i);
				}
				return data;
			};
			auto check_sub_nodes = [&](const glm::vec3& relative_pos, float node_size)
			{
				//how do we _know_ there is no chance for a node to exist in this subset?
				//with reasonable clarity e.g. there's literally no way for the node to ever exist inside this.
				return sdf::contains_edge_crossing(generator, world_pos + relative_pos, node_size);

				constexpr bool CHECK_NODES = true;
				constexpr float sqrt_3 = 1.7320508075688772935274463415059f;	//since we're dealing with cubes this would be the longest distance possible.

				//flip a coin, clang optimizes this better than fabs() for whatever reason. Visual Studio.... not so much. they literally flip back and forth between which uses andps
				/*auto abs_fnc = [](float val)
				{
					//IEEE754 floating point format (single and double) demands that the highest bit is the sign bit.
					//therefore we can instead just set the bit to zero and ignore all branching required for an abs.
					//this basically compiles down to 1 and asm instruction vs 7ish asm instructions for the usual val > 0.f ? val : -val;
					//for reference...	https://godbolt.org/g/P6t7kN (unless the site dies)

					constexpr std::size_t size = sizeof(val) * 8;
					constexpr int mask = ~(1 << (size - 1));
					int* float_bits = reinterpret_cast<int*>(&val);
					*float_bits &= mask;
					return val;
				};*/
				auto abs_fnc = [](float val)
				{
					return val > 0.f ? val : -val;
				};

				if (hmap.contains_crossing_point(world_pos + relative_pos, (int)node_size))
				{
					return CHECK_NODES;
				}

				glm::vec3 pos = world_pos + relative_pos + (glm::vec3(0.5f, 0.5f, 0.5f)*node_size);
				float center_point_density = abs_fnc(generator(pos));
				float center_point_min_radial_dist = sqrt_3 * node_size*.5f;

				return (center_point_density <= center_point_min_radial_dist);
			};
			if (curr_depth == max_depth)
			{
				auto corner_data = get_corners(relative_pos, (float)(1 << base_node_size));
				if (corner_data.corners == 0x0 || corner_data.corners == 0xff)
					return;
				octree_node& node = get_node({ curr_node_offset,curr_sub_node_index });

				node.set_size(1 << base_node_size);
				node.set_min(relative_pos);
				construct_leaf(node, generator, corner_data, (float)node.size(), world_pos, relative_pos);
				return;
			}

			for (int32_t curr_sub_index = 0; curr_sub_index < 8; ++curr_sub_index)
			{
				int32_t offset = 1ull << (curr_depth + 1);
				float node_size = (float)bounding_size / (float)offset;

				glm::vec3 node_local_root{
					relative_pos.x + ((curr_sub_index & 0x4) >> 2)*node_size,
					relative_pos.y + ((curr_sub_index & 0x2) >> 1)*node_size,
					relative_pos.z + ((curr_sub_index & 0x1))*node_size
				};
				//check if we're even supposed to do anything
				if (check_sub_nodes(node_local_root, node_size))
				{
					int32_t ind = 0;

					if (!get_node({ curr_node_offset,curr_sub_index }).is_valid() && curr_depth < (max_depth - 1))
						//allocate our nodes to work on
					{
						int32_t next_ind = allocate_nodes();
						get_node({ curr_node_offset,curr_sub_index }).next_node_block = next_ind;
					}
					ind = get_node({ curr_node_offset,curr_sub_index }).next_node_block;
					octree_node& node = get_node({ curr_node_offset, curr_sub_index });
					bool use_ind = false;
					if (ind < nodes.size())
					{
						nodes[ind].owner_offset = curr_node_offset;
						nodes[ind].owner_arr_ind = curr_sub_index;
						use_ind = true;
					}
					node.set_size((int32_t)node_size);
					node.set_min(node_local_root);
					node.type = octree_node::node_internal;

					//continue checking if we're supposed to have data here.
					//note: by even getting here we *must* have at least one surface belonging to a subnode.
					build_tree(generator, hmap, world_pos, node_local_root, curr_depth + 1, use_ind ? ind : curr_node_offset, curr_sub_index);
				}
			}
		}
		template<typename generator_type, typename corner_data_type>
		void construct_leaf(octree_node& node, generator_type& generator, const corner_data_type& corner_data, float node_size, const glm::vec3& world_pos, const glm::vec3& relative_pos)
		{
			//actual node work
			//the rest of the function out of this scope winds up boiling down to this (with some checks for being able to discard early nodes)

			constexpr int32_t max_crossings = 6;	//literally the face of a cube

			int32_t edge_count = 0;
			glm::vec3 average_normal;
			QEF_solver qef{};

			for (int32_t ind = 0; ind < edgev_map.size() && edge_count < max_crossings; ++ind)
			{
				int c1 = edgev_map[ind][0];
				int c2 = edgev_map[ind][1];
				int m1 = (corner_data.corners >> c1) & 1;
				int m2 = (corner_data.corners >> c2) & 1;

				if ((m1 == material_air && m2 == material_air) ||
					(m1 == material_solid && m2 == material_solid))
				{
					//no zero crossing
					continue;
				}
				//{ 0, 0, 0 },	0
				//{ 0, 0, 1 },	1
				//{ 0, 1, 0 },	2
				//{ 0, 1, 1 },	3
				//{ 1, 0, 0 },	4
				//{ 1, 0, 1 },	5
				//{ 1, 1, 0 },	6
				//{ 1, 1, 1 }	7
				//} };
				glm::vec3 p1 = glm::vec3(world_pos + relative_pos + (glm::vec3(CHILD_MIN_OFFSETS[c1] << base_node_size)));
				glm::vec3 p2 = glm::vec3(world_pos + relative_pos + (glm::vec3(CHILD_MIN_OFFSETS[c2] << base_node_size)));

				glm::vec3 pos = approximate_zero_crossing_position(p1, p2, corner_data.densities[c1], corner_data.densities[c2]);
				glm::vec3 normal = calculate_surface_normal(pos, generator);


				pos -= world_pos;

				qef.add(pos, normal);
				average_normal += normal;
				++edge_count;
			}

			glm::vec3 qef_position;
			qef.solve(qef_position, QEF_ERROR, QEF_SWEEPS, QEF_ERROR);

			//PERF: is there a way to batch allocate this?
			std::unique_ptr<octree_draw_info> draw_info = std::make_unique<octree_draw_info>();
			draw_info->pos = qef_position;
			*draw_info->qef = qef.get_data();
			draw_info->avg_normal = glm::normalize(average_normal);
			draw_info->corners = (int32_t)corner_data.corners;

			glm::vec3 min = glm::vec3(node.min());
			glm::vec3 max = glm::vec3(node.min() + decltype(node.min())(node.size()));

			//draw_info->pos = min;// +(max - min) / 2;
			if (draw_info->pos.x < min.x || draw_info->pos.x > max.x ||
				draw_info->pos.y < min.y || draw_info->pos.y > max.y ||
				draw_info->pos.z < min.z || draw_info->pos.z > max.z)
			{
				draw_info->pos = qef.get_mass_point();
			}

			node.draw_info = std::move(draw_info);
			node.type = octree_node::node_leaf;
		}
		void generate_vertex_index(int32_t curr_index, int32_t curr_offset)
		{
			octree_node& node = get_node({ curr_index, curr_offset });
			if (!node.is_valid())
				return;
			if (node.type == octree_node::node_internal)
			{
				for (int32_t curr_sub_index = 0; curr_sub_index < 8; ++curr_sub_index)
				{
					generate_vertex_index(node.next_node_block, curr_sub_index);
				}
			}
			else if (node.type != octree_node::node_internal)
			{
				octree_draw_info* draw_info = node.draw_info.get();
				if (!draw_info)
					throw std::runtime_error("No draw info for this quadtree node");	//?? how the fuck would you reach this

				//turns out skipping doesnt work since chunks can be redrawn at a later point in which case the seams/this chunk's draw->index would be hilariously wrong.
				//if (draw_info->index == -1)
				draw_info->index = (int)gpu_mesh_data->get<spawnx_vulkan::position>().size();
				gpu_mesh_data->get<spawnx_vulkan::position>().push_back(draw_info->pos);
				gpu_mesh_data->get<spawnx_vulkan::normal>().push_back(draw_info->avg_normal);
				gpu_mesh_data->get<spawnx_vulkan::uvs>().push_back({ 0,0 });
			}
		}
		void contour_process_edge(const std::array<std::pair<uint32_t, uint32_t>, 4>& candidate_nodes, int dir)
		{
			int min_size = 1000000;		//some large number
			int min_index = 0;
			std::array<uint32_t, 4> indices_candidates{ (uint32_t)-1, (uint32_t)-1, (uint32_t)-1, (uint32_t)-1 };
			bool flip = false;
			std::array<bool, 4> sign_change{ false,false,false,false };

			for (int32_t i = 0; i < 4; ++i)
			{
				int edge = process_edge_mask[dir][i];
				int c1 = edgev_map[edge][0];
				int c2 = edgev_map[edge][1];

				octree_node& node = get_node(candidate_nodes[i]);

				int m1 = (node.draw_info->corners >> c1) & 1;
				int m2 = (node.draw_info->corners >> c2) & 1;

				if (node.size() < min_size)
				{
					min_size = node.size();
					min_index = (int)i;
					flip = m1 != material_air;
				}

				indices_candidates[i] = node.draw_info->index;

				sign_change[i] = (m1 != m2);
			}
			if (sign_change[min_index])
			{
				if (!flip)
				{
					gpu_mesh_data->indices.push_back(indices_candidates[0]);
					gpu_mesh_data->indices.push_back(indices_candidates[1]);
					gpu_mesh_data->indices.push_back(indices_candidates[3]);

					gpu_mesh_data->indices.push_back(indices_candidates[0]);
					gpu_mesh_data->indices.push_back(indices_candidates[3]);
					gpu_mesh_data->indices.push_back(indices_candidates[2]);
				}
				else
				{
					gpu_mesh_data->indices.push_back(indices_candidates[0]);
					gpu_mesh_data->indices.push_back(indices_candidates[3]);
					gpu_mesh_data->indices.push_back(indices_candidates[1]);

					gpu_mesh_data->indices.push_back(indices_candidates[0]);
					gpu_mesh_data->indices.push_back(indices_candidates[2]);
					gpu_mesh_data->indices.push_back(indices_candidates[3]);
				}
			}
		}
		void contour_edge_proc(const std::array<std::pair<uint32_t, uint32_t>, 4>& candidate_nodes, int dir)
		{
			for (const auto& candidate : candidate_nodes)
			{
				if (candidate.first == -1)
					return;
				octree_node& node = get_node(candidate);
				if (!node.is_valid())
					return;
			}
			{
				std::array<octree_node*, 4> tmp_debug_nodes;
				int32_t mask = 0;
				int32_t ind = 0;
				for (const auto& candidate : candidate_nodes)
				{
					octree_node& node = get_node(candidate);
					tmp_debug_nodes[ind] = &node;
					mask |= (node.type != octree_node::node_internal) << ind;
					++ind;
				}
				if (mask == 0b1111)
				{
					contour_process_edge(candidate_nodes, dir);
					return;
				}
			}
			for (int32_t i = 0; i < 2; ++i)
			{
				std::array<std::pair<uint32_t, uint32_t>, 4> edge_nodes;

				std::array<uint32_t, 4> c = { edge_proc_edge_mask[dir][i][0],
					edge_proc_edge_mask[dir][i][1],
					edge_proc_edge_mask[dir][i][2],
					edge_proc_edge_mask[dir][i][3] };

				for (int32_t j = 0; j < 4; ++j)
				{
					octree_node& node = get_node(candidate_nodes[j]);
					if (node.type == octree_node::node_leaf || node.type == octree_node::node_pseudo)
					{
						edge_nodes[j] = candidate_nodes[j];
					}
					else
					{
						edge_nodes[j] = { node.next_node_block, c[j] };
					}
				}
				contour_edge_proc(edge_nodes, edge_proc_edge_mask[dir][i][4]);
			}
		}
		void contour_face_proc(const std::array<std::pair<uint32_t, uint32_t>, 2>& nodes_candidates, int32_t dir)
		{
			for (const auto& candidate : nodes_candidates)
			{
				if (candidate.first == -1)
					return;
				octree_node& node = get_node(candidate);
				if (!node.is_valid())
					return;
			}

			octree_node* node_ptrs[2] = { &get_node(nodes_candidates[0]), &get_node(nodes_candidates[1]) };

			if (node_ptrs[0]->type == octree_node::node_internal || node_ptrs[1]->type == octree_node::node_internal)
			{
				for (int32_t i = 0; i < 4; ++i)
				{
					std::array<std::pair<uint32_t, uint32_t>, 2> face_nodes{};
					std::array<uint32_t, 2> c = { face_proc_face_mask[dir][i][0],
						face_proc_face_mask[dir][i][1] };

					for (int32_t j = 0; j < 2; ++j)
					{
						if (node_ptrs[j]->type != octree_node::node_internal)
						{
							face_nodes[j] = nodes_candidates[j];
						}
						else
						{
							face_nodes[j] = { node_ptrs[j]->next_node_block, c[j] };
						}
					}
					contour_face_proc(face_nodes, face_proc_face_mask[dir][i][2]);
				}

				constexpr std::array<std::array<uint32_t, 4>, 2> orders{ { { { 0,0,1,1 } },{ { 0,1,0,1 } } } };

				for (int32_t i = 0; i < 4; ++i)
				{
					std::array<std::pair<uint32_t, uint32_t>, 4> edge_nodes;

					std::array<uint32_t, 4> c = { face_proc_edge_mask[dir][i][1],
						face_proc_edge_mask[dir][i][2],
						face_proc_edge_mask[dir][i][3],
						face_proc_edge_mask[dir][i][4] };

					const std::array<uint32_t, 4>& order = orders[face_proc_edge_mask[dir][i][0]];
					for (int32_t j = 0; j < 4; ++j)
					{
						if (node_ptrs[order[j]]->type == octree_node::node_leaf ||
							node_ptrs[order[j]]->type == octree_node::node_pseudo)
						{
							edge_nodes[j] = nodes_candidates[order[j]];
						}
						else
						{
							edge_nodes[j] = { node_ptrs[order[j]]->next_node_block, c[j] };
						}
					}
					contour_edge_proc(edge_nodes, face_proc_edge_mask[dir][i][5]);	//so many magics thrown around....
				}
			}
		}
		void contour_cell_proc(octree_node& node)
		{
			//this is a check to make sure we're actually supposed to draw this node.
			//since we can have dead end leafs; while normal 'leafs' still have data.
			if (!node.is_valid())
				return;
			if (node.type == octree_node::node_internal)
			{
				for (uint32_t i = 0; i < 8; ++i)
				{
					contour_cell_proc(get_node( node.next_node_block, i ));
				}
				for (int32_t i = 0; i < cell_proc_face_mask.size(); ++i)
				{
					std::array<std::pair<uint32_t, uint32_t>, 2> face_nodes;
					std::array<uint32_t, 2> c = { cell_proc_face_mask[i][0],cell_proc_face_mask[i][1] };

					face_nodes[0] = { node.next_node_block, c[0] };
					face_nodes[1] = { node.next_node_block, c[1] };
					contour_face_proc(face_nodes, cell_proc_face_mask[i][2]);
				}
				for (int32_t i = 0; i < 6; ++i)
				{
					std::array<std::pair<uint32_t, uint32_t>, 4> edge_nodes;
					std::array<uint32_t, 4> c = { cell_proc_edge_mask[i][0],
						cell_proc_edge_mask[i][1],
						cell_proc_edge_mask[i][2],
						cell_proc_edge_mask[i][3] };

					for (int32_t j = 0; j < c.size(); ++j)
					{
						edge_nodes[j] = { node.next_node_block, c[j] };
					}
					contour_edge_proc(edge_nodes, cell_proc_edge_mask[i][4]);
				}
			}
		}
		struct seam_dependency
		{
			std::vector<std::shared_ptr<octree_root>> roots;
			struct min_max_step
			{
				glm::ivec3 min, max, offset;
				int step;
			};
			min_max_step tree_and_inds_relative(const octree_root& base, const octree_root& dependency) const
			{
				//in assorted order.... the required scan chunks are:
				//[1,1,1] -> [0,	0,			0]
				//[1,0,1] -> [0,	(0,max),	0]
				//basically anywhere that we have a zero or half offset we must scan from 0->edge inclusively.
				//if we have a half offset anywhere (e.g. 2 roots) we must scan the 0->half for the low, and half->max for the high.
				glm::ivec3 offset = dependency.relative_pos - base.relative_pos;

				//this needs to be switched to be fed into us. any time we'd have to offset our neighbors we fail to place the same sized chunks in the correct local spot causing problems.
				//base_must_be_offset = base.base_node_size < dependency.base_node_size;
				auto determine_min = [&](int offset)
				{
					if (dependency.base_node_size > base.base_node_size && offset < 0)
					{
						//we already have the value... just return it
						return -offset;
					}
					//since the min is for the relative chunk; its possible we have a chunk larger than ourselves requesting some data
					//e.g. a chunk that's 128 accessing some 64's, by default in this case you will always want the full chunk/beginning of the chunk.
					return 0;
				};
				auto determine_max = [&](int offset)
				{
					//if we're dealing with a larger chunk
					if (dependency.base_node_size > base.base_node_size)
					{
						//the dependency is a bit negative
						//so we have to go up to the full size of the larger chunk, since our smaller is supposed to be half anyway.
						if (offset < 0)
						{
							//if offset in the case of a chunk that is half our size is going to be a half increment of the larger chunk.
							//therefore based on this the max would be something like 256 for a chunk that's smaller neighbor is 128
							//on the other hand that same 256 chunk could have a chunk that is 64 in size next to it (and then it can occupy 3 other sub sections)
							//this solves the above issues generically
							return -offset + (1 << (octree_root::default_max_depth + base.base_node_size));
						}
						if (offset > 0)
							return 1;
						//otherwise we can only go up to the size of the smaller chunk so we're not sampling into the wrong area.
						//+1 is to force sampling of the last block in the case of our dependency being the larger chunk.
						return (1 << (octree_root::default_max_depth + base.base_node_size)) + 1;
					}
					//if we're dealing with a smaller chunk....
					if (dependency.base_node_size < base.base_node_size)
					{
						//if we're entirely offset into the next chunk that means we're looking for just the sliver bordering us
						if (offset == (1 << (octree_root::default_max_depth + base.base_node_size)))
							return 1;
						//otherwise this is the case of 0/ 1/2 base size; where we are supposed to fully scan a simi-large area.
						return 1 << (octree_root::default_max_depth + dependency.base_node_size);
					}

					if (offset == (1 << (octree_root::default_max_depth + base.base_node_size)))
						return 1;
					return 1 << (octree_root::default_max_depth + dependency.base_node_size);
				};
				glm::ivec3 min = { determine_min(offset.x),determine_min(offset.y),determine_min(offset.z) };
				glm::ivec3 max = { determine_max(offset.x),determine_max(offset.y),determine_max(offset.z) };
				return { min, max, offset, (1 << dependency.base_node_size) };
			}
			template<typename lambda_type>
			static auto create_linear_access_lambda(lambda_type& seam_tree_adder)
			{
				return [&](const octree_root& chunk, int min, int max, int mask, const glm::ivec3& accessor_offset, const glm::ivec3& bounds_offset)
				{
					glm::ivec3 accessor_mask = {};
					accessor_mask[bit_scan(mask)] = 1;
					for (int i = min; i < max;)
					{
						i += seam_tree_adder(chunk, accessor_offset + accessor_mask * i, bounds_offset);
					}
				};
			}
			template<typename lambda_type>
			static auto create_planar_access_lambda(lambda_type& seam_tree_adder)
			{
				return [&](const octree_root& chunk, const glm::ivec2& max, int mask, const glm::ivec3& accessor_offset, const glm::ivec3& bounds_offset)
				{
					auto morton_min = morton_encode_2d({});
					//we can have something like 65 passed in; where we have to check that strip(s).
					//but for speed we need the largest sub region to be a pure morton order.
					auto morton_max = morton_encode_2d((max & ~1) - 1);

					auto accessor_offsets = [](const int32_t& mask, const glm::ivec2& coords) -> glm::ivec3
					{
						if (mask == 0b011)
							return { coords.x,coords.y,0 };

						else if (mask == 0b101)
							return { coords.x,0,coords.y };

						else // 0b110
							return { 0,coords.x,coords.y };
					};
					auto linear_lam = create_linear_access_lambda(seam_tree_adder);
					if (max.y & 1)
					{
						//we have a bit of y at some power of 2 to go through; which means we're incrementing through x.
						auto relative_offset = accessor_offsets(mask, { 0,max.y - 1 });	//this isn't quite right
						//x is always the lowest bit, but by default linear_lam scans from highest so we have to set just that one bit.
						linear_lam(chunk, 0, (max.x & ~1), 1<<bit_scan_forward(mask), relative_offset + accessor_offset, bounds_offset);
					}
					if (max.x & 1)
					{
						//we have a bit of x at some power of 2 to go through; ...
						auto relative_offset = accessor_offsets(mask, { max.x - 1,0 });
						linear_lam(chunk, 0, (max.y & ~1), mask, relative_offset + accessor_offset, bounds_offset);
					}

					for (auto curr_morton = morton_min; curr_morton <= morton_max;)
					{
						auto morton_coord = accessor_offsets(mask, morton_decode_2d(curr_morton));
						auto tmp_accessor_offsets = accessor_offset + morton_coord;
						auto increment = seam_tree_adder(chunk, tmp_accessor_offsets, bounds_offset);
						curr_morton += increment * increment;
					}
				};
			}
			//template<typename lambda_type>
			void add_valid_nodes_to_tree(const octree_root& base, const std::function<int(const octree_root&, const glm::ivec3&, const glm::ivec3&)>& seam_tree_adder) const
			{
				
				auto linear_access = create_linear_access_lambda(seam_tree_adder);
				auto planar_access = create_planar_access_lambda(seam_tree_adder);
				for (auto& chunk_ptr : roots)
				{
					if constexpr (false)
					{
						if (chunk_ptr)
						{
							min_max_step bounds = tree_and_inds_relative(base, *chunk_ptr);
							for (int z = bounds.min.z; z < bounds.max.z; z += bounds.step)
								for (int y = bounds.min.y; y < bounds.max.y; y += bounds.step)
									for (int x = bounds.min.x; x < bounds.max.x; x += bounds.step)
										seam_tree_adder(*chunk_ptr, { x,y,z }, bounds.offset);
						}
					}
					//as a morton encoding...
					else
					{
						//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
						//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
						//rewrite this it literally doesnt work.
						//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
						//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
						//also remember: morton orders need to be -1 for their last element under******************************
						//the normal accessor. and must check the end piece.
						//e.g. if you want to bound the region at (128,128)
						//you will access far; far more than you intend. the last valid element is (127,127) which if you check that one also
						//will fill check the whole region of [0,0]->(128,128) (128's exclusive, everything else inclusive)
						// DO NOT DO PARTIAL FILLS YOU ARE AN IDIOT AND DONT GET THEM CORRECT ALMOST EVER
						//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
						//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
						if (!chunk_ptr)
							return;
						min_max_step bounds = tree_and_inds_relative(base, *chunk_ptr);

						glm::ivec3 diffs = bounds.max - bounds.min;
						int mask = (diffs.x != 1) | ((diffs.y != 1) << 1) | ((diffs.z != 1)<<2);
						if (mask == 0b000)	//point access, e.g. the last tiny corner
						{
							//by default this works
							seam_tree_adder(*chunk_ptr, { 0,0,0 }, bounds.offset);
						}
						else if (mask == 0b100 || mask == 0b010 || mask == 0b001) //linear access
						{
							//this does excessive out of range checks
							int accessor_bit = bit_scan(mask);
							glm::ivec3 offset_mins = bounds.min;
							offset_mins[accessor_bit] = 0;
							//no clue what the local offset should sanely be, check how the above works? does it even need it? idk
							linear_access(*chunk_ptr, bounds.min[accessor_bit], bounds.max[accessor_bit], mask, offset_mins, bounds.offset);
						}
						else //planar access
						{
							//no stitching issues on  the same size chunk, different size chunks sometimes skip stuff though
							//this checks 3x the area and 2/3 of it is out of bounds
							int y_offset = bit_scan(mask);
							int x_offset = bit_scan(mask ^ (1 << y_offset));

							//we only need to check the nodes that could potentiall be bordering us.
							glm::ivec2 accessor_max = { diffs[x_offset], diffs[y_offset] };

							planar_access(*chunk_ptr, accessor_max, mask, bounds.min, bounds.offset);
						}
					}
				}
			}
		};
		octree_root create_seam_tree(const octree_root& base, const std::vector<seam_dependency>& neighbors) const
		{
			int min_node_size = base.base_node_size;
			//figure out what the hell our min node size actually is.
			for (const auto& dependency : neighbors)
			{
				if (dependency.roots.size() != 0 && min_node_size > dependency.roots[0]->base_node_size)
					min_node_size = dependency.roots[0]->base_node_size;
			}

			//+1 because we're controlling at least 8 chunks and need to be able to store everything in their correct location
			//+2 because we're going to have chunks that are a factor larger than us/smaller than us and must be able to store everything in their correct offsets.
			octree_root seam_tree{ {},default_max_depth + 2, 1 << min_node_size };
			auto access_and_add_lam = [&](const octree_root& fetch_root, const glm::ivec3& lookup_pos, const glm::ivec3& offset)
			{
				auto node_ = fetch_root.get(lookup_pos);
				octree_node node = node_.first;
				//should track down how we can have a valid node on the edge of a chunk that does not have draw info and is considered is_valid()
				if (node.is_valid() && node.draw_info)
				{
					node.draw_info->pos += offset;
					seam_tree.add_node(node, lookup_pos + offset);
				}
				return node_.second;
			};
			for (const auto& dependency : neighbors)
			{
				//this is the main part of the loop; everything else is just debugging code because its a pain in the ass to actually debug this
				dependency.add_valid_nodes_to_tree(base, access_and_add_lam);
			}
			//-------------------
			//some verification check to make sure the two hash tables have at least 1 access to the same elements
			//otherwise throw an error.
			//-------------------

			auto planar_access = seam_dependency::create_planar_access_lambda(access_and_add_lam);

			size_t upper_bounds = (size_t)base.bounding_size;
			int offset = 1 << base.base_node_size;
			glm::ivec3 base_offset = glm::ivec3{ 0, 0, 0 };
			//top of our chunk
			if constexpr(false)
			{
				for (int z = 0; z < upper_bounds; z += (1 << base.base_node_size))
					for (int x = 0; x < upper_bounds; x += (1 << base.base_node_size))
					{
						access_and_add_lam(base, { x,upper_bounds - offset,z }, base_offset);
					}
				//right side
				for (int y = 0; y < upper_bounds - offset; y += (1 << base.base_node_size))
					for (int x = 0; x < upper_bounds; x += (1 << base.base_node_size))
					{
						access_and_add_lam(base, { x,y, upper_bounds - offset }, base_offset);
					}
				//front side
				for (int z = 0; z < upper_bounds - offset; z += (1 << base.base_node_size))
					for (int y = 0; y < upper_bounds - 1; y += (1 << base.base_node_size))
					{
						access_and_add_lam(base, { upper_bounds - offset,y,z }, base_offset);
					}
			}
			else
			{
				//auto increment = seam_tree_adder(chunk, accessor_offset + accessor_offsets(mask, curr_morton), bounds_offset);

				//our implied bit representation is reverse of how I want to read it... e.g. 0bzyx is how it is vs I want to read as 0bxyz
				//top of the chunk
				planar_access(base, { upper_bounds, upper_bounds }, 0b101, { 0,upper_bounds - offset,0 }, base_offset);
				//right side..
				planar_access(base, { upper_bounds, upper_bounds }, 0b011, { 0,0,upper_bounds - offset }, base_offset);
				//front side...
				planar_access(base, { upper_bounds, upper_bounds }, 0b110, { upper_bounds - offset,0,0 }, base_offset);
			}
			return seam_tree;
		}
		//neighbors are of the form:
		//{ 0, 0, 1 },
		//{ 0, 1, 0 },
		//{ 0, 1, 1 },
		//{ 1, 0, 0 },
		//{ 1, 0, 1 },
		//{ 1, 1, 0 },
		//{ 1, 1, 1 }
		void generate_mesh(cpu_mesh_data_type continued_data = nullptr)
		{
			if (nodes.empty())
				return;
			if (continued_data == nullptr)
			{
				//really this should be a separate flag for checking if we've generated this mesh.
				//but due to laziness and how easily set up the thread pool is we can basically skip the cost of creating a pointless job.
				if(start_of_seam_tree.index_offset != 0)
					return;
				gpu_mesh_data = std::make_shared<spawnx_vulkan::default_cpu_mesh>(spawnx_vulkan::default_cpu_mesh{});
			}
			else
				gpu_mesh_data = continued_data;

			for (int32_t ind = 0; ind < 8; ++ind)
				generate_vertex_index(0, ind);
			auto fake_root = octree_node::root_equiv_node();
			contour_cell_proc(fake_root);

			if (continued_data == nullptr)
			{
				//if we're passed in a nullptr for continued it means we're generating the octrees own mesh, not a seamtree.
				//so given that we need to store the size of our associated mesh for faster mesh rebuilding.
				//its an attempt at making mesh rebuilding faster when moving and seams must be recaclulated.
				start_of_seam_tree = gpu_mesh_data->get_curr_offsets();
			}
			//above is the mesh generation for the typical chunk
			//if we don't do anything else there will be seams between all chunks.
			//so we create a temporary chunk that takes the appropriate edges from this chunk + its 7 'positive' neighbors.
			//>they can be optionally null incase the chunks have not been created yet; in which case this chunk will have to be remeshed when the other chunk is finally created.
		}
		cpu_mesh_data_type generate_seam_tree(const std::vector<seam_dependency>& neighbors)
		{
			//as a performance boost, we can just destroy the seamtree every time we generate/regenerate a seam.
			//this way we can keep the chunk's own mesh and not have to recalculate everything.
			if (gpu_mesh_data)
			{
				gpu_mesh_data->resize_mesh(start_of_seam_tree);
			}

			octree_root seam_octree = create_seam_tree(*this, neighbors);
			if (seam_octree.nodes.size() > 1)
			{
				seam_octree.generate_mesh(gpu_mesh_data);
				return seam_octree.gpu_mesh_data;
			}
			return gpu_mesh_data;
		}

	private:

		octree_node & get_node(const uint32_t& first, const uint32_t& second)
		{
			return nodes[first].node_block[second];
		}
		const octree_node& get_node(const uint32_t& first, const uint32_t& second) const
		{
			return nodes[first].node_block[second];
		}
		octree_node& get_node(std::pair<uint32_t, uint32_t> index)
		{
			return nodes[index.first].node_block[index.second];
		}
		const octree_node& get_node(std::pair<uint32_t, uint32_t> index) const
		{
			return nodes[index.first].node_block[index.second];
		}
		static constexpr std::array<glm::ivec3, 8> CHILD_MIN_OFFSETS{ {
			{ 0, 0, 0 },
			{ 0, 0, 1 },
			{ 0, 1, 0 },
			{ 0, 1, 1 },
			{ 1, 0, 0 },
			{ 1, 0, 1 },
			{ 1, 1, 0 },
			{ 1, 1, 1 }
			} };
		static constexpr std::array<std::array<uint32_t, 2>, 12> edgev_map{ {
			{ { 0,4 } },{ { 1,5 } },{ { 2,6 } },{ { 3,7 } },	// x-axis 
			{ { 0,2 } },{ { 1,3 } },{ { 4,6 } },{ { 5,7 } },	// y-axis
			{ { 0,1 } },{ { 2,3 } },{ { 4,5 } },{ { 6,7 } }		// z-axis
			} };

		static constexpr std::array<uint32_t, 3> edge_mask = { { 5, 3, 6 } };

		static constexpr std::array<std::array<uint32_t, 3>, 8> vert_map{ {
			{ { 0,0,0 } },
			{ { 0,0,1 } },
			{ { 0,1,0 } },
			{ { 0,1,1 } },
			{ { 1,0,0 } },
			{ { 1,0,1 } },
			{ { 1,1,0 } },
			{ { 1,1,1 } }
			} };

		static constexpr std::array<std::array<uint32_t, 4>, 6> face_map{ { { { 4, 8, 5, 9 } },{ { 6, 10, 7, 11 } },{ { 0, 8, 1, 10 } },{ { 2, 9, 3, 11 } },{ { 0, 4, 2, 6 } },{ { 1, 5, 3, 7 } } } };
		static constexpr std::array<std::array<uint32_t, 3>, 12> cell_proc_face_mask{ { { { 0,4,0 } },{ { 1,5,0 } },{ { 2,6,0 } },{ { 3,7,0 } },{ { 0,2,1 } },{ { 4,6,1 } },{ { 1,3,1 } },{ { 5,7,1 } },{ { 0,1,2 } },{ { 2,3,2 } },{ { 4,5,2 } },{ { 6,7,2 } } } };
		static constexpr std::array<std::array<uint32_t, 5>, 6> cell_proc_edge_mask{ { { { 0,1,2,3,0 } },{ { 4,5,6,7,0 } },{ { 0,4,1,5,1 } },{ { 2,6,3,7,1 } },{ { 0,2,4,6,2 } },{ { 1,3,5,7,2 } } } };

		static constexpr std::array<std::array<std::array<uint32_t, 3>, 4>, 3> face_proc_face_mask{ {
			{ { { { 4,0,0 } },{ { 5,1,0 } },{ { 6,2,0 } },{ { 7,3,0 } } } },
			{ { { { 2,0,1 } },{ { 6,4,1 } },{ { 3,1,1 } },{ { 7,5,1 } } } },
			{ { { { 1,0,2 } },{ { 3,2,2 } },{ { 5,4,2 } },{ { 7,6,2 } } } }
			} };

		static constexpr std::array<std::array<std::array<uint32_t, 6>, 4>, 3> face_proc_edge_mask{ {
			{ { { { 1,4,0,5,1,1 } },{ { 1,6,2,7,3,1 } },{ { 0,4,6,0,2,2 } },{ { 0,5,7,1,3,2 } } } },
			{ { { { 0,2,3,0,1,0 } },{ { 0,6,7,4,5,0 } },{ { 1,2,0,6,4,2 } },{ { 1,3,1,7,5,2 } } } },
			{ { { { 1,1,0,3,2,0 } },{ { 1,5,4,7,6,0 } },{ { 0,1,5,0,4,1 } },{ { 0,3,7,2,6,1 } } } }
			} };

		static constexpr std::array<std::array<std::array<uint32_t, 5>, 2>, 3> edge_proc_edge_mask{ {
			{ { { { 3,2,1,0,0 } },{ { 7,6,5,4,0 } } } },
			{ { { { 5,1,4,0,1 } },{ { 7,3,6,2,1 } } } },
			{ { { { 6,4,2,0,2 } },{ { 7,5,3,1,2 } } } },
			} };

		static constexpr std::array<std::array<uint32_t, 4>, 3> process_edge_mask{ { { { 3,2,1,0 } },{ { 7,5,6,4 } },{ { 11,10,9,8 } } } };
	};
}