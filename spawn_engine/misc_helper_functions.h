#pragma once
#include <type_traits>
#include <intrin.h>
#include <glm\glm.hpp>
namespace spawn_engine
{
	template<typename type, std::enable_if_t<std::is_integral<type>::value, bool> = true>
	constexpr bool is_power_of_two(type& n)
	{
		return (n != 0) && ((n &(n - 1)) == 0);
	}
	template<uint64_t num_dims>
	constexpr uint64_t get_morton_mask(uint64_t minimal_bit_mask)
	{
		constexpr uint64_t max_shifts = sizeof(uint64_t) * 8 / num_dims;
		uint64_t mask = minimal_bit_mask;
		for (uint64_t i = 0; i < max_shifts; ++i)
		{
			mask = (mask << num_dims) | minimal_bit_mask;
		}
		return mask;
	}
	uint64_t morton_encode_3d(const glm::u32vec3& pos)
	{
		constexpr auto x_mask = get_morton_mask<3>(0b100);
		constexpr auto y_mask = get_morton_mask<3>(0b010);
		constexpr auto z_mask = get_morton_mask<3>(0b001);

		return _pdep_u64(pos.x, x_mask) | _pdep_u64(pos.y, y_mask) | _pdep_u64(pos.z, z_mask);
	}
	glm::u32vec3 morton_decode_3d(const uint64_t& mask)
	{
		constexpr auto x_mask = get_morton_mask<3>(0b100);
		constexpr auto y_mask = get_morton_mask<3>(0b010);
		constexpr auto z_mask = get_morton_mask<3>(0b001);

		return { _pext_u64(mask, x_mask),
			_pext_u64(mask, y_mask),
			_pext_u64(mask, z_mask) };
	}
	uint64_t morton_encode_2d(const glm::u32vec2& pos)
	{
		constexpr auto x_mask = get_morton_mask<2>(0b01);
		constexpr auto y_mask = get_morton_mask<2>(0b10);

		return _pdep_u64(pos.x, x_mask) | _pdep_u64(pos.y, y_mask);
	}
	glm::u32vec2 morton_decode_2d(const uint64_t& mask)
	{
		constexpr auto x_mask = get_morton_mask<2>(0b01);
		constexpr auto y_mask = get_morton_mask<2>(0b10);

		return { _pext_u64(mask, x_mask),
			_pext_u64(mask, y_mask) };
	}
	int relative_offset(uint64_t morton_code, int node_bit_size)
	{
		constexpr int mask = 0b111;
		return (morton_code >> (3 *node_bit_size)) & mask;
	}
	int relative_offset(const glm::ivec3& lookup, int node_bit_size)
	{
		return relative_offset(morton_encode_3d(lookup), node_bit_size);
	}

}