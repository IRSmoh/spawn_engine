#pragma once
//this has to be included first due to some fuckery with zip_iterator only.
#include <spawnx\spawnx.h>

#include <vector>
#include <array>
#include <memory>
#include <iostream>
#include <thread>
#include <future>
#include <unordered_map>
#ifdef _WIN32
#include <intrin.h>
template<typename type , std::enable_if_t<std::is_integral<type>::value && sizeof(type) < 8,bool> = true>
type bit_scan(type mask)
{
	unsigned long ind;
	uint8_t ret = _BitScanReverse(&ind, (unsigned long)mask);
	if (ret)
		return ind;
	return 0;
}
template<typename type, std::enable_if_t<std::is_integral<type>::value && sizeof(type) == 8, bool> = true>
type bit_scan(type mask)
{
	unsigned long ind;
	uint8_t ret = _BitScanReverse64(&ind, mask);
	if (ret)
		return ind;
	return 0;
}
template<typename type, std::enable_if_t<std::is_integral<type>::value && sizeof(type) < 8, bool> = true>
type bit_scan_forward(type mask)
{
	unsigned long ind;
	uint8_t ret = _BitScanForward(&ind, (unsigned long)mask);
	if (ret)
		return ind;
	return 0;
}
template<typename type, std::enable_if_t<std::is_integral<type>::value && sizeof(type) == 8, bool> = true>
type bit_scan_forward(type mask)
{
	unsigned long ind;
	uint8_t ret = _BitScanForward64(&ind, mask);
	if (ret)
		return ind;
	return 0;
}
#else
template<typename type, std::enable_if_t<std::is_integral<type>::value, bool> = true>
type bit_scan(type mask)
{
	return __builtin_clz(mask);
}
#endif

#include <vulkan_renderer\vulkan_renderer.h>

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>


#include "misc_helper_functions.h"
#include "thread_pool.h"
#include "height_map.h"
#include "height_map_manager.h"
#include "QEF.h"
#include "octree.h"
#include "chunk_manager.h"


#include "keys.h"

#include "density_func.h"