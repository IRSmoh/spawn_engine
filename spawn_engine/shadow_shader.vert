#version 450

layout(location = 0) in vec3 inPosition;

out gl_PerVertex {
    vec4 gl_Position;
};

layout(binding = 0) uniform UniformBufferObject
{
	mat4 model;
} matrices;
layout (binding = 1) uniform camera_data
{
	mat4 view;
	mat4 proj;
} camera;

void main() 
{
    gl_Position = camera.proj* camera.view * matrices.model * vec4(inPosition, 1.0);
}