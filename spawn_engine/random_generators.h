#pragma once
#include <random>
#include <array>
namespace spawn_engine
{
	struct xoroshiro128
	{
		using result_type = uint64_t;
		std::array<uint64_t, 2> s;


		xoroshiro128() : xoroshiro128(12345431, 214455631) {}
		template<typename seq_type, std::enable_if_t<std::is_integral<typename seq_type::result_type>::value && sizeof(typename seq_type::result_type) >= 2, bool> = true>
		xoroshiro128(seq_type& device) //: xoroshiro128((device() << 32) | device(), (device() << 32) | device())
		{
			using result_type = typename seq_type::result_type;
			if constexpr(sizeof(result_type) == 8)//int64 and variants
			{
				s = { device(),device() };
			}
			else if constexpr(sizeof(result_type) == 4)//int32
			{
				s = { ((uint64_t)(device()) << 32) | (uint64_t)(device()), ((uint64_t)(device()) << 32) | (uint64_t)(device()) };
			}
			else if constexpr(sizeof(result_type) == 2)//int16t
			{
				s = { ((uint64_t)(device()) << 48) | ((uint64_t)(device()) << 32) | ((uint64_t)(device()) << 16) | (uint64_t)(device()),
					((uint64_t)(device()) << 48) | ((uint64_t)(device()) << 32) | ((uint64_t)(device()) << 16) | (uint64_t)(device()) };
			}
		}
		xoroshiro128(uint64_t left, uint64_t right) : s({ left,right }) {}


		static uint64_t rotl(const uint64_t x, int k) {
			return (x << k) | (x >> (64 - k));
		}

		result_type operator()()
		{
			const uint64_t s0 = s[0];
			uint64_t s1 = s[1];
			const result_type result = s0 + s1;

			s1 ^= s0;
			s[0] = rotl(s0, 55) ^ s1 ^ (s1 << 14); // a, b
			s[1] = rotl(s1, 36); // c

			return result;
		}
		void jump()
		{
			constexpr std::array<uint64_t, 2> JUMP = { 0xbeac0467eba5facb, 0xd86b048b86aa9922 };

			uint64_t s0 = 0;
			uint64_t s1 = 0;
			for (int i = 0; i < JUMP.size(); i++)
				for (int b = 0; b < 64; b++) {
					if (JUMP[i] & UINT64_C(1) << b) {
						s0 ^= s[0];
						s1 ^= s[1];
					}
					(*this)();
				}

			s[0] = s0;
			s[1] = s1;
		}
		static constexpr result_type min()
		{
			return 0;
		}
		static constexpr result_type max()
		{
			return std::numeric_limits<result_type>::max();
		}
	};
}