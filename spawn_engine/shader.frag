#version 450

layout(location = 0) in vec3 frag_normal;
layout(location = 1) in vec2 fragTexCoord;
layout(location = 2) in vec4 light_space_pos;
layout(location = 3) in vec4 light_direction;

layout(binding = 3) uniform sampler2D texSampler;
layout(binding = 4) uniform sampler2D shadow_sampler;

layout(location = 0) out vec4 outColor;

float compute_shadow_factor(vec4 light_space_pos_, vec4 light_direction_, sampler2D shadow_map, uint shadow_map_size, uint pcf_size)
{
	const float max_shadow_darkness = 0.4f;
	const float max_brightness = 1.0f;
	vec3 light_space_ndc = light_space_pos_.xyz/ light_space_pos_.w;
	//if the light is entirely outside the projection this light can't affect it meaning its shadowed.
	if(	abs(light_space_ndc.x) >= 1.0f ||
		abs(light_space_ndc.y) >= 1.0f ||
		abs(light_space_ndc.z) >= 1.0f)
		return max_shadow_darkness;
	float dot_val = dot(frag_normal, light_direction_.xyz);

	vec2 shadow_map_coord = light_space_ndc.xy*.5f + .5f;
	float bias = 0.005f/light_space_pos.w;//*tan(acos(dot_val));
	//bias = clamp(bias,0,0.1/light_space_pos.w);
	// Check if the sample is in the light or in the shadow
	if ((light_space_ndc.z-bias) > texture(shadow_map, shadow_map_coord).x)
      return max_shadow_darkness; // In the shadow

   // In the light
   return max_brightness;
}
void main()
{
	vec4 color = texture(texSampler, vec2(frag_normal.x+1.f,frag_normal.y+1.f)/2);
	outColor = color*vec4(compute_shadow_factor(light_space_pos,light_direction,shadow_sampler,1024,3));
	//outColor = texture(texSampler, vec2(frag_normal.x+1.f,frag_normal.y+1.f)/2);
}