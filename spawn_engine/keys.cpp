#include "keys.h"
std::array<Keys::keyBools, Keys::Key_Count> Keys::keys = { 0 };
std::array<Keys::keyBools, Keys::Mouse_Count> Keys::mouse_keys = { 0 };
std::array<double, Keys::Scroll_Count> Keys::scroll_state = { 0 };
std::array<double, Keys::Scroll_Count> Keys::scroll_event = { 0 };

double Keys::old_mouse_x = 0;
double Keys::old_mouse_y = 0;
double Keys::delta_x = 0;
double Keys::delta_y = 0;
double Keys::old_delta_x = 0;
double Keys::old_delta_y = 0;
float Keys::width = 0;
float Keys::height = 0;