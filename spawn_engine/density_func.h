#pragma once
#include <glm/ext.hpp>
#include "height_map.h"
#include "sdf.h"
namespace spawn_engine
{
	sdf make_sphere( float radius)
	{
		return [radius](const glm::vec3& input_pos)
		{
			return glm::length(input_pos) - radius;
		};
	}

	sdf make_torus(const glm::vec2& radii)
	{
		return [radii](const glm::vec3& p)
		{
			glm::vec2 q = { glm::length(glm::vec2(p.x,p.z)) - radii.x,p.y };
			return glm::length(q) - radii.y;
		};
	}
	sdf make_capsule(const glm::vec3& first_end, const glm::vec3& second_end, float radius)
	{
		return [first_end, second_end, radius](const glm::vec3& lookup_pos)
		{
			glm::vec3 pa = lookup_pos - first_end;
			glm::vec3 ba = second_end - first_end;
			float h = glm::clamp(glm::dot(pa, ba) / dot(ba, ba), 0.f, 1.f);
			return glm::length(pa - ba * h) - radius;
		};
	}

	// ----------------------------------------------------------------------------
	sdf make_rectangle(const glm::vec3& half_dims)
	{
		return [half_dims](const glm::vec3& input_pos)
		{
			glm::vec3 local_pos = input_pos;
			glm::vec3 d = abs(local_pos) - half_dims;
			float m = glm::max(d.x, glm::max(d.y, d.z));
			return glm::min(m, glm::length(glm::max(d, glm::vec3{})));
		};
	}

	sdf make_capped_cylinder(const glm::vec2& h)
	{
		return [h](const glm::vec3& input_pos)
		{
			glm::vec3 local_pos = input_pos;
			glm::vec2 d = glm::abs(glm::vec2(glm::length(glm::vec2{ local_pos.x,local_pos.z }), local_pos.y)) - h;
			return glm::min(glm::max(d.x, d.y), 0.f) + glm::length(glm::max(d, 0.f));
		};
	}
	// ----------------------------------------------------------------------------


	sdf make_ring_world(float exterior_radius, float height, float exterior_thickness, float wall_thickness, float wall_height)
	{
		glm::vec2 exterior{ exterior_radius, height };
		glm::vec2 interior_disc{ exterior_radius - exterior_thickness - wall_height, height };
		glm::vec2 chunked{ exterior_radius - exterior_thickness, height - wall_thickness };

		return (make_capped_cylinder(exterior) - make_capped_cylinder(interior_disc)) - make_capped_cylinder(chunked);

	}

	template<typename vec_type>
	float FractalNoise(
		int octaves,
		float frequency,
		float lacunarity,
		float persistence,
		const vec_type& position)
	{
		const float SCALE = 1.f / 900.f;
		vec_type p = position * SCALE;
		float noise = 0.f;

		float amplitude = 1.f;
		p *= frequency;

		for (int i = 0; i < octaves; i++)
		{
			noise += simplex(p) * amplitude;
			p *= lacunarity;
			amplitude *= persistence;
		}
		//return 1.4f + (0.5f * noise);
		return pow(1.4f + (0.5f * noise), 4.7f);
	}
	sdf create_shitty_simplex(float scale)
	{
		return [=](const glm::vec3& world_pos)
		{
			//range -1,1
			//internally all these noise functions are a vector pointing in an arbitrary direction
			//so there should be some euclideun-ness to it
			//but we need on the range of 1unit away = 1 value for an sdf.
			return (simplex(world_pos / scale)-1.f)*scale/15.f;
		};
	}
	auto create_fractal_height_map_manager()
	{
		auto generator_lam = [&](glm::vec2 pos)
		{
			float noise = FractalNoise(4, 0.5343f, 2.2324f, 0.68324f, pos);
			return 20.f*noise;
		};
		return generator_lam;
	}
	auto create_fractal_height_map(const glm::vec2& world_pos)
	{
		auto generator_lam = [](const glm::vec2& pos)
		{
			return 12.f*FractalNoise(4, 0.5343f, 2.2324f, 0.68324f, pos);
		};
		return height_map_manager::height_map_type::create_map(1, world_pos, generator_lam);
	}
	sdf Density_Func(const glm::vec3& section_root, float section_size, std::shared_ptr<height_map_manager::height_map_type> hmap)
	{
		sdf terrain = {
			[&](const glm::vec3& input_pos)
			{
				return input_pos.y - hmap->get_world_density(input_pos);
			},
			[&](const glm::vec3& root_pos, float node_size)
			{
				return hmap->contains_crossing_point(root_pos, (int)node_size);
			} };

		constexpr glm::vec3 cube_pos = glm::vec3(-4., 190.f, -4.f);
		constexpr glm::vec3 torus_pos = glm::vec3(16.f, 27.f, 16.f);
		auto torus_rot = glm::rotate(glm::mat4{}, 1.7f, glm::normalize(glm::vec3{ 1,0,1 }));


		sdf cube = translate(make_rectangle(glm::vec3(145.1f)), cube_pos);
		sdf torus_ = translate(rotate(make_torus( { 900.f,80.f }),torus_rot), torus_pos);

		sdf right_half = cube % translate(make_sphere(155.1f), cube_pos + glm::vec3{ 10, 10, 10 });

		sdf massive_sphere = make_sphere( 1500.f);

		sdf onioned = [massive_sphere = std::move(massive_sphere)](const glm::vec3& input)
		{
			const float thickness = 100.f;
			return glm::abs(massive_sphere(input)) - thickness;
		};

		auto tmp = terrain + ((onioned + torus_ - right_half) - translate(make_sphere(500.f), glm::vec3{ 1500,0,0 }));
		tmp.collapse_field_in_region(section_root, section_size);

		return tmp;
	}
}